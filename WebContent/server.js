var mysql     =    require('mysql'),
 app                 = require('http').createServer(handler),
 io                  = require('socket.io').listen(app),
 fs                  = require('fs'),
 url = require('url'),
 favicon = require('favicon'), 
$ = require('jquery'),
 path = require('path'),
 contentTypes = {
	    '.html': 'text/html',
	    '.css': "text/css",
	    '.js': 'application/javascript'
	},
 connectionsArray    = [],
 connection      =    mysql.createConnection({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'db_prolution',
    port    :  3306
}),
 POLLING_INTERVAL = 3000,
 pollingTimer;

connection.connect(function(err) {
	  console.log( err );
	});
//var idIdea;
var userid;
var uid;

app.listen(8000);
function handler ( req, res ) {

	
}
var counts;

var pollingLoop2 = function () {
	
    // Make the database query
    var query = connection.query('SELECT COUNT(*) AS cnt FROM tb_users');

    // set up the query listeners
    query
    .on('error', function(err) {
        // Handle error, and 'end' event will be emitted after this as well
        console.log( err );
        updateSockets( err );        
    })
    .on('result', function( count ) {
        // it fills our array looping on each user row inside the db
    	counts=count;
    
    })
    .on('end',function(){
        // loop on itself only if there are sockets still connected
        if(connectionsArray.length) {
            pollingTimer = setTimeout( pollingLoop2, POLLING_INTERVAL );

        	//console.log(counts["cnt"]);
        	
            updateSockets({counts:counts});
        }
        else {

            console.log('The server timer was stopped because there are no more socket connections on the app');

          }
    });

    
};

var pollingLoop = function () {		
    var query = connection.query('SELECT * FROM tb_users'),
//	var query = connection.query('SELECT * FROM tb_users WHERE userid="'+idIdea+'"'),
        users = []; // this array will contain the result of our db query
    query
    .on('error', function(err) {
        // Handle error, and 'end' event will be emitted after this as well
        console.log( err );
        updateSockets( err );        
    })
    .on('result', function( user ) {
        // it fills our array looping on each user row inside the db
        users.push( user );
    })
    .on('end',function(){
        // loop on itself only if there are sockets still connected
        if(connectionsArray.length) {
            pollingTimer = setTimeout( pollingLoop, POLLING_INTERVAL );
            
            updateSockets({users:users});
        }
        else {

            console.log('The server timer was stopped because there are no more socket connections on the app');

          }
    });

    
};
var pollingLoop4 = function () {		
	var query = connection.query('SELECT * FROM tb_bids WHERE bidsTask IN (SELECT taskID FROM tb_tasks WHERE taskIdeaId IN (SELECT ideaId FROM tb_idea WHERE ideaPM="'+uid+'") OR taskProId IN (SELECT proID FROM tb_project WHERE proOwner="'+uid+'"))'),
        pbids = []; // this array will contain the result of our db query
    query
    .on('error', function(err) {
        // Handle error, and 'end' event will be emitted after this as well
        console.log( err );
        updateSockets( err );        
    })
    .on('result', function( bid ) {
        // it fills our array looping on each user row inside the db
        pbids.push( bid );
    })
    .on('end',function(){
        // loop on itself only if there are sockets still connected
        if(connectionsArray.length) {
            pollingTimer = setTimeout( pollingLoop4, POLLING_INTERVAL );
            
            updateSockets({pbids:pbids});
        }
        else {

            console.log('The server timer was stopped because there are no more socket connections on the app');

          }
    });

    
};

var pollingLoop3 = function () {		
	var query = connection.query('SELECT * FROM tb_bids WHERE bidsIdea IN (SELECT ideaId FROM tb_idea WHERE ideaOwner="'+userid+'")'),
        bids = []; // this array will contain the result of our db query
    query
    .on('error', function(err) {
        // Handle error, and 'end' event will be emitted after this as well
        console.log( err );
        updateSockets( err );        
    })
    .on('result', function( bid ) {
        // it fills our array looping on each user row inside the db
        bids.push( bid );
    })
    .on('end',function(){
        // loop on itself only if there are sockets still connected
        if(connectionsArray.length) {
            pollingTimer = setTimeout( pollingLoop3, POLLING_INTERVAL );
            
            updateSockets({bids:bids});
        }
        else {

            console.log('The server timer was stopped because there are no more socket connections on the app');

          }
    });

    
};


// create a new websocket connection to keep the content updated without any AJAX request
io.sockets.on( 'connection', function ( socket ) {
	
	  console.log("url: " + socket.handshake.url);
	  
	  
	  socket.on('getId', function(data, cb) {
		  userid=data["userid"];
		  uid=data["uid"];

		  //idIdea=data["ideaId"];
		  console.log(data["userid"]);
			//console.log(data);        //cb({some: 'response'});
	});    
    
    
    console.log('Number of connections:' + connectionsArray.length);
    // start the polling loop only if at least there is one user connected
    if (!connectionsArray.length) {
        pollingLoop();
        pollingLoop2();
        pollingLoop3();
        pollingLoop4();


    }
    
    socket.on('disconnect', function () {
        var socketIndex = connectionsArray.indexOf( socket );
        console.log('socket = ' + socketIndex + ' disconnected');
        if (socketIndex >= 0) {
            connectionsArray.splice( socketIndex, 1 );
        }
    });

    console.log( 'A new socket is connected!' );
    connectionsArray.push( socket );
    
});



var updateSockets = function ( data ) {
    // store the time of the latest update
    data.time = new Date();
    console.log('Pushing new data to the clients connected ( connections amount = %s ) - %s', connectionsArray.length , data.time);
    // send new data to all the sockets connected
    connectionsArray.forEach(function( tmpSocket ){
        tmpSocket.volatile.emit( 'notification' , data );
        
    });
};