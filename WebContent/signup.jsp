<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html>
<html lang="en"> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pro-Lution - Signup</title>        
      <!--  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> -->
	   <!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/signup.css" /> -->	  
	   
	   <!--  new css files attached for new signup page -->
	   
	   <!-- <link rel="stylesheet" href="bootstrap/css/signin.css"> -->
  
		 <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
		 <!-- <link rel="stylesheet" href="bootstrap/css/prolution-style.css">  -->
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">

        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
		<!-- <link rel="stylesheet" href="bootstrap/css/dropdownstyle.css" type="text/css" media="screen"/>
		
		<link rel="stylesheet" href="bootstrap/css/slidercss.css" type="text/css" media="screen"/> -->
   			<link rel="stylesheet" href="bootstrap/css/login.css">
   			
   				<script type="text/javascript" src="bootstrap/js/countries_dropdown.js">
   				
   				</script>
   				
			<%--  <script type="text/javascript">
				
				</script>
   			 
	    	    --%>
	    	   
	    	   
	    	 
</head>
<body>

			<nav class="navbar navbar-default">
				
					<div>
						<a href="goHome"> <!-- Pro<em>- Lution</em>-->
							<img class="projectlogodesign" src="bootstrap/img/ProLogo.png">
						</a>
					</div>

					
				
			</nav>
			<div  class="toplabel">
			<h1 class="headertext" id="topmsg"> Create An Account </h1>
			
			
			<s:form action="register" onsubmit="return check1();" >  
			<s:actionerror />
						<label> First Name</label>
						
						<label class="topleftlabel"> Last Name</label>
						
						<br>
						<input class="inputsize" type="text" name="firstname" required="required" placeholder="Firstname"/>          	
                        <input class="inputsize" type="text" name="lastname" required="required" placeholder="Last Name"/>
                        <br>
                        <br>
                        <label> Email Address</label>
                        <br>
                        <input class="emailinput" type="email" name="email" required="required" placeholder="Email"/>
                        <br><br>
                        <label> Username </label>
                        <br>
                        <input class="emailinput" type="text" name="username" required="required" placeholder="Username"/>
                        <br><br>
                        <label> Password </label>
                        <label class="topleftlabel"> Re-Type Password </label>
                        <br>
                        <input title="Password must contain at least 6 characters, including UPPER/lowercase and numbers" class="inputsize" type="password" name="upassword" required="required" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="upassword1" onchange="
  						this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
  						if(this.checkValidity()) form.upassword1.pattern = this.value;
						"/>
                        <input title="Please enter the same Password as above" class="inputsize" type="password" name="upassword1" required="required" placeholder="Retype Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" onchange="
  						this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
						"/>
                        <p>*Password Must Contain A Capital letter, special character and a numeric value</p>
                        
                        <label> Select your Country </label>
                        
                        <%-- <div id="countries">
                        
                        <script type="text/javascript">
                         countriesDropdown("countries");
                         
                         
                        </script>
						 
						  
						</div> --%>
                        <br>
						<!-- <input id="country" class="inputsize" type="text" name="country" required="required"  /> -->
                        
                        
                        
                        
                        
                        
                        
<select id="country" name="country" required="required">
<option value="">Select Your Country</option>
<optgroup label="Select Country"></optgroup>
<option value="Afghanistan">Afghanistan</option>
<option value="�land Islands">�land Islands</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="American Samoa">American Samoa</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antarctica">Antarctica</option>
<option value="Antigua and Barbuda">Antigua and Barbuda</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bhutan">Bhutan</option>
<option value="Bolivia">Bolivia</option>
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Bouvet Island">Bouvet Island</option>
<option value="Brazil">Brazil</option>
<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
<option value="Brunei Darussalam">Brunei Darussalam</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burundi">Burundi</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Cape Verde">Cape Verde</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Central African Republic">Central African Republic</option>
<option value="Chad">Chad</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Christmas Island">Christmas Island</option>
<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option value="Colombia">Colombia</option>
<option value="Comoros">Comoros</option>
<option value="Congo">Congo</option>
<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
<option value="Cook Islands">Cook Islands</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Cote D'ivoire">Cote D'ivoire</option>
<option value="Croatia">Croatia</option>
<option value="Cuba">Cuba</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="French Guiana">French Guiana</option>
<option value="French Polynesia">French Polynesia</option>
<option value="French Southern Territories">French Southern Territories</option>
<option value="Gabon">Gabon</option>
<option value="Gambia">Gambia</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Greece">Greece</option>
<option value="Greenland">Greenland</option>
<option value="Grenada">Grenada</option>
<option value="Guadeloupe">Guadeloupe</option>
<option value="Guam">Guam</option>
<option value="Guatemala">Guatemala</option>
<option value="Guernsey">Guernsey</option>
<option value="Guinea">Guinea</option>
<option value="Guinea-bissau">Guinea-bissau</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Isle of Man">Isle of Man</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jersey">Jersey</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
<option value="Korea, Republic of">Korea, Republic of</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macao">Macao</option>
<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
<option value="Madagascar">Madagascar</option>
<option value="Malawi">Malawi</option>
<option value="Malaysia">Malaysia</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall Islands">Marshall Islands</option>
<option value="Martinique">Martinique</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mayotte">Mayotte</option>
<option value="Mexico">Mexico</option>
<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
<option value="Moldova, Republic of">Moldova, Republic of</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montenegro">Montenegro</option>
<option value="Montserrat">Montserrat</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Myanmar">Myanmar</option>
<option value="Namibia">Namibia</option>
<option value="Nauru">Nauru</option>
<option value="Nepal">Nepal</option>
<option value="Netherlands">Netherlands</option>
<option value="Netherlands Antilles">Netherlands Antilles</option>
<option value="New Caledonia">New Caledonia</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>
<option value="Niue">Niue</option>
<option value="Norfolk Island">Norfolk Island</option>
<option value="Northern Mariana Islands">Northern Mariana Islands</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palau">Palau</option>
<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Pitcairn">Pitcairn</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Reunion">Reunion</option>
<option value="Romania">Romania</option>
<option value="Russian Federation">Russian Federation</option>
<option value="Rwanda">Rwanda</option>
<option value="Saint Helena">Saint Helena</option>
<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option value="Saint Lucia">Saint Lucia</option>
<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
<option value="Samoa">Samoa</option>
<option value="San Marino">San Marino</option>
<option value="Sao Tome and Principe">Sao Tome and Principe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Somalia">Somalia</option>
<option value="South Africa">South Africa</option>
<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Syrian Arab Republic">Syrian Arab Republic</option>
<option value="Taiwan, Province of China">Taiwan, Province of China</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
<option value="Thailand">Thailand</option>
<option value="Timor-leste">Timor-leste</option>
<option value="Togo">Togo</option>
<option value="Tokelau">Tokelau</option>
<option value="Tonga">Tonga</option>
<option value="Trinidad and Tobago">Trinidad and Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
<option value="Tuvalu">Tuvalu</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Emirates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="United States">United States</option>
<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
<option value="Uruguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Venezuela">Venezuela</option>
<option value="Viet Nam">Viet Nam</option>
<option value="Virgin Islands, British">Virgin Islands, British</option>
<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
<option value="Wallis and Futuna">Wallis and Futuna</option>
<option value="Western Sahara">Western Sahara</option>
<option value="Yemen">Yemen</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
</select>
                        
                        
<%--                         <p>Select a new car from the list.</p>

<select id="mySelect" onchange="myFunction()">
  <option value="Audi">Audi
  <option value="BMW">BMW
  <option value="Mercedes">Mercedes
  <option value="Volvo">Volvo
</select>

<p>When you select a new car, a function is triggered which outputs the value of the selected car.</p>

<p id="demo"></p>

<script>
function myFunction() {
    var x = document.getElementById("mySelect").value;
    document.getElementById("demo").innerHTML = "You selected: " + x;
}
</script>
                         --%>
         

<!-- 				
	 		Select Country: 
<select onchange="print_state('state', this.selectedIndex);" id="country" name ="country"></select>
		<br />
    City/District/State: <select name ="state" id ="state"></select>
<script language="javascript">print_country("country");</script>	
      -->                   
                        <!-- 
                        <form method="POST" action="..." onsubmit="return checkForm(this);">
						<fieldset>
						<legend>Change Password</legend>
						<p>Username: <input title="Enter your username" type="text" required pattern="\w+" name="username"></p>
						<p>Password: <input title="Password must contain at least 6 characters, including UPPER/lowercase and numbers" type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="pwd1" onchange="
  						this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
  						if(this.checkValidity()) form.pwd2.pattern = this.value;
						"></p>
						<p>Confirm Password: <input title="Please enter the same Password as above" type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="pwd2" onchange="
  						this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
						"></p>
						<p><input type="submit" value="Save Changes"></p>
						</fieldset>
						</form>
						 -->
                        <!-- 
						<p>Username: <input type="text" required pattern="\w+" name="username"></p>
						<p>Password: <input type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="pwd1" onchange="form.pwd2.pattern = this.value;"></p>
						<p>Confirm Password: <input type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="pwd2"></p>
                         -->
                        <!-- 
                        <p><s:textfield name="firstname" required="true" label="First Name" placeholder="First Name"></s:textfield></p>                
                        <p><s:textfield name="lastname" required="true" label="Last Name" placeholder="Last Name"></s:textfield></p>                               
                        <p><s:textfield name="email" required="true" label="Email" placeholder="Email"></s:textfield></p>                               
                        <p><s:textfield name="username" required="true" label="Username" placeholder="Username"></s:textfield></p>                                              
        	        	<p><s:password name="upassword" required="true" label="Password" placeholder="Password"></s:password></p>
        	            <p><s:textfield name="country" required="true" label="Country" placeholder="Country"></s:textfield></p> -->                                              
<%--                    <p><s:textfield name="country" required="true" label="Country" placeholder="Country"></s:textfield></p>                                               --%>
                
                <div>
                
                <br>
                <label>Choose how you want to Use </label>
                 <%-- <s:radio id="radiobutton" name="usertype" title="label1" label="Signing up as" 
    						list="#{'pm':'Project Manager','cn':'Contributor','en':'Enterpreneur'}"/> --%> 
    			 
	            
	            
	            <br>
	            <label for="r1">projectmanager</label>
	            <input id="r1" type="radio" required="required" name="usertype" value="pm" title="Project Manager"/>
	            
	            <label for="r2">Contributor</label>
	            <input id="r2" type="radio"  name="usertype" value="cn" title="Contributor"/>
	            
	            <label for="r3">Enterpreneur</label>
	            <input id="r3" type="radio"  name="usertype" value="en" title="Enterpreneur"/>
	            
	 			<p><input type="checkbox" required name="terms"> I accept the <u>Terms and Conditions</u></p>
                 
 				<p>
 					<s:submit value="Signup"  cssClass="btn btn-success" ></s:submit> 
                </p>
	           </div>            
				</s:form>
			
			<div id="required">
		<p>* Required Fields<br/>
		NOTE: You must activate your account after sign up</p>
		
		</div>
			
			</div>
			
			<div class="warningmsg">
			  
			  <div class="warningmsg1">
			   <p> Why Prolutions </p>
			 	<!-- <p> your information is not shared with anyone </p> -->
			 	
			 	<ul>
			 	<li> Free to post jobs </li>
			 	<li> Find work easily </li>
			 	<li> Work on micro tasks </li>
			 	</ul>
			 	<br><br>
			 	<img class="badgesize" src="bootstrap/img/badgetrusted.jpg"/>
			 	</div>
			</div>
			
	<%-- 	<div class="alert alert-success">
   	<a href="#" class="close" data-dismiss="alert">
      &times;
   	</a>
   			<strong>Warning!</strong> There was a problem with your 
   			network connection.
		</div> --%>
<%--     <!--BEGIN #signup-form -->
     <div id="signup-form">        
        <!--BEGIN #subscribe-inner -->
            <div>
            <h1 class="headingone">Sign up  </h1><br>
            </div>            
			<s:form action="register"  >            	
                        <p><s:textfield name="firstname" required="true" label="First Name" placeholder="First Name"></s:textfield></p>                
                        <p><s:textfield name="lastname" required="true" label="Last Name" placeholder="Last Name"></s:textfield></p>                               
                        <p><s:textfield name="email" required="true" label="Email" placeholder="Email"></s:textfield></p>                               
                        <p><s:textfield name="username" required="true" label="Username" placeholder="Username"></s:textfield></p>                                              
        	        	<p><s:password name="upassword" required="true" label="Password" placeholder="Password"></s:password></p>
        	            <p><s:textfield name="country" required="true" label="Country" placeholder="Country"></s:textfield></p>                                              
                        	            <p><s:textfield name="country" required="true" label="Country" placeholder="Country"></s:textfield></p>                                              
                
                <div>
                <label>Choose how you want to Use </label>
                 <s:radio id="radiobutton" name="usertype" title="label1" label="Signing up as" 
    						list="#{'pm':'Project Manager','cn':'Contributor','en':'Enterpreneur'}"/> 
	            <p>
                <!-- <button id="submit" type="submit">Submit</button> -->
                
                <!-- <button class="myButton" type="submit" > Signup</button> -->
                 <!--<s:form action="register"><s:submit  value="Signup" cssClass="myButton" /></s:form>-->
                 <s:submit value="Signup" cssClass="myButton" ></s:submit>
                
 <s:submit value="Signup"  cssClass="myButton" ></s:submit> 
                </p>
	           </div>            
</s:form> --%>



<!-- <form onsubmit="return checkForm(this);">

<p><input type="checkbox" required name="terms"> I accept the <u>Terms and Conditions</u></p>
<p><input type="submit"></p>
</form>
 -->
<script type="text/javascript">

    function check1() {

        var radio_check_val = "";
        for (i = 0; i < document.getElementsByName('usertype').length; i++) {
            if (document.getElementsByName('usertype')[i].checked) {
                /*alert("this radio button was clicked: " + document.getElementsByName('usertype')[i].value);*/
                radio_check_val = document.getElementsByName('usertype')[i].value;
                return true;

            }

        }
        if (radio_check_val === "")
        {
            alert("please select radio button");
            /*returnToPreviousPage();*/
            
            return false;
        }

       


    }
    
    
    
</script>

<script type="text/javascript">


  function checkForm(form)
  {
    
    if(!form.terms.checked) {
      alert("Please indicate that you accept the Terms and Conditions");
      form.terms.focus();
      return false;
    }
    return true;
  }

</script>
<script src="./jquery/jquery-1.11.1.min.js"></script>
<script src="./jquery/jquery.form-validator.min.js"></script>
<script>
  $.validate();
  $('#my-textarea').restrictLength( $('#max-length-element') );  
  



  
</script>            
		
        <!--END #signup-inner -->
     <!--END #signup-form -->       
</body>
</html>