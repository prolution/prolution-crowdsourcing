<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
				
				<link rel="stylesheet" href="pm/css/bidsbyme.css">
	
    			<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
				<link href="en/css/bootstrap.min.css" rel="stylesheet"> 
					
	<!-- 			<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
				<link href="en/css/bootstrap.min.css" rel="stylesheet">
	        	<link href="en/css/bootstrap-theme.css" rel="stylesheet">  
   				<link href="en/css/elegant-icons-style.css" rel="stylesheet" /> 
   				<link rel="stylesheet" href="en/css/layout.css" type="text/css"/>
   			    <link rel="stylesheet" href="en/css/pagefooter.css">
				<link href="en/css/style-responsive.css" rel="stylesheet" />
    			<link href="en/css/bootstrap.min.css" rel="stylesheet">
     -->
  
</head>
<body>



		<div class="mybidform">
		
			<div class="headertext">
			<h2> Idea Bid Request Proposals </h2>
			</div>
			
			<div class="mybidleftpan">
					<div class="tabbar">
							<div class="sortdesc">
								<label> Sort By </label> &nbsp; 
								<span>
									<select>
										<option>By name</option>
										<option>By Amount</option>
									</select>
								</span>
							</div>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
										
											<th align="left">Name</th>
											
											<th>country</th>
											<th>Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    	<s:if test="%{allBids.isEmpty()}">No bids!</s:if>
						<s:else>
								<s:iterator value="allBids">
			
								<tr>
									
									<td>
										<s:property  value="firstname"/> &nbsp;<s:property  value="lastname"/>		
									</td>			
											
									<td>		
										<s:property  value="country"/>	
									</td>		
									<td>		    			
	    							<s:form action="ideaBidder">
	    									<s:hidden name="bidsAmount" value="%{bidsAmount}" ></s:hidden>
	    									<s:hidden name="ideaId" value="%{ideaId}" ></s:hidden>
						 					<s:hidden name="userid" value="%{userid}" ></s:hidden>    				
      										<s:submit cssClass="btn btn-success" value="Open"></s:submit>
		                   			</s:form>
                   					</td>
                   				</tr>	 
                   
      							</s:iterator>
         				</s:else>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Idea Title (unasigned)</h4>
		 				 <h4><s:property value="%{idea.ideaTitle}"/></h4>  
		 				<ul>
		 					<li>Description<p><s:property value="%{idea.ideaDescrption}"/></p></li>
		 					<li>Duration<p><s:property value="%{idea.ideaDuration}"/></p></li>
		 					<li>Budget <p><s:property value="%{idea.ideaBudget}"/></p></li>
		 					
		 					
		 						<%-- <s:textfield label="Idea Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
									
									<s:textfield label="idea Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
									
									<s:textfield label="idea duration" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
									
									<s:textfield label="Idea description" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield> --%>
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the task </p>
		 			
		 			</div>
			
			</div>
			
			
			
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>









	
	
	
	                       
                        
                        
                        
                        
                        
                        
                  <%--       

                
                                <div class="row">
                    <div class="col-lg-8 text-center">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                
                                
                                 <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">List Of Bids </h3>
                            </div>
                            <div class="panel-body">
                                
									
									<table class="tablesorter">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Detail</th>
							</tr>
						</thead>

						<s:if test="%{allBids.isEmpty()}">No bids!</s:if>
						<s:else>
								<s:iterator value="allBids">
			
								<tr>
									<td>
										<s:property  value="username"/>
									</td>
									<td>
										<s:property  value="firstname"/>		
									</td>			
									<td>		
										<s:property  value="lastname"/>
									</td>		
									<td>		
										<s:property  value="country"/>	
									</td>		
									<td>		    			
	    								<s:form action="ideaBidder">
	    									<s:hidden name="bidsAmount" value="%{bidsAmount}" ></s:hidden>
	    									<s:hidden name="ideaId" value="%{ideaId}" ></s:hidden>
						 					<s:hidden name="userid" value="%{userid}" ></s:hidden>    				
      										<s:submit value="Open"></s:submit>
		                   				</s:form>
                   					</td>
                   				</tr>	 
                   
      							</s:iterator>
         				</s:else>
                        </table>
                            </div>
                        </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 text-center">
                        <div class="panel panel-default">
                            <div class="panel-body">
                               
                               
                               <h3> Unassigned Task </h3>
									<s:form action="updateProj">
									 
										
										<s:textfield label="Idea Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
									
									<s:textfield label="idea Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
									
									<s:textfield label="idea duration" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
									
									<s:textfield label="Idea description" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield>
				
										<s:submit value="Update" ></s:submit>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden>
				
			</s:form>
				

                					
 --%>
 </body>
</html>