<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link href="pm/css/ideaTasks.css" rel="stylesheet" type="text/css">
		  	<link href="en/css/bootstrap.min.css" rel="stylesheet">
	    	<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">

	<script src="pm/js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
	$(function() {
		
		$("#jRate").jRate({
			rating: 3,
			strokeColor: 'black',
			width: 20,
			height: 20,						
		 	onChange: function(rating) {
				console.log("OnChange: Rating: "+rating);
			}, 
			onSet: function(rating) {
				$('#rating').val( Math.round(rating));
				//alert("hello baba"+rating);
			}
		});
	});
	</script>
	
</head>
<body>


<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
	

<div class="maincontainer">
		<div class="leftpan">
					
							<div class="ideadetails">			
								<h3> Idea </h3>
								<h4><strong><s:property value="%{idea.ideaTitle}" /> </strong></h4>
								<ul>
									<li> <s:property value="%{idea.ideaDescrption}" /></li> 
									<li> Budget :<s:property value="%{idea.ideaBudget}" /></li>
									<li> Posted On: <s:property value="%{idea.ideaPostedOn}" /></li>
									<li> Completion Date:<s:property value="%{idea.ideaDuration}" /></li>
									<li> Idea Domain:<s:property value="%{idea.ideaDomain}" /></li>
									<li> Status: In Process</li>
									<!-- <li><a href="#">Edit</a></li> -->
										<%-- <s:textfield label="Idea Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
										<s:textfield label="idea Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
										<s:textfield label="idea duration" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
										<s:textfield label="Idea description" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield> --%>
								</ul>
							</div>
							
							<div class="linksdetails">
								<ul>
									<li>
										<s:a action="ideaTasks">Idea Details
										<s:param name="userid" value="#session.userid" />
										<s:param name="ideaId" value="%{ideaId}" />
										</s:a>
									</li>
									<li><a href="#">Idea Manager </a></li>									
									
								</ul>
							</div>
		
		</div>
		<!-- mid pan starts here  -->
		
		<div class="middlepan">
		
				<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											
    										<th>Task</th>
											<th>Title</th>
											<th>Domain</th>
											<th>Posted on</th>
											<th>Budget</th>
											
											
											
							
                                    </tr>
                                </thead>
                                <tbody>

											<tr>
    										</tr>	
												
										  
                                    
                                    
                                </tbody>
                            </table>
                       </div>
                        
			
				
		</div>
		<!-- mid pan ends here  -->
		<div class="rightpan">
					<div class="ideaowner">
						<h4> Manager of Idea </h4>
						<%-- <h3>
						<strong>
						<a href="#">
						<s:property value="%{user.firstname}" /> 
						<s:property value="%{user.lastname}" /></a></strong></h3> --%>
						
						<h3>
												<strong>
													<s:url id="viewPmpublic" action="viewPmpublic">
			             							<s:param name="userid" value="%{user.userid}"></s:param>
			             							<s:param name="username" value="%{user.username}"></s:param>
			            			 				</s:url>			
													<s:a href="%{viewPmpublic}"><s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/></s:a>
												</strong>	
						</h3>
						<p><s:property value="%{user.country}" /></p>
						<div id="jRate" style="height: 10px; width: 40px;">
						</div>
						<div Class="ratebtn">
						
						<s:form action="ratePm">
								<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="rater" />
								<s:hidden id="rating"  name="rating"></s:hidden>									
								<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>									
								<s:submit cssClass="btn btn-success" value="Rate"></s:submit>
      					</s:form>
						
						</div>
						
 						<s:form action="ideaCompleted">
						<s:hidden name="ideaFlag" value="4"></s:hidden>									
						<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>									
						<s:submit cssClass="btn btn-success" value="Completed"></s:submit>
      					</s:form>
						<hr>
						<%-- 
						<div  title="Critical Path" id="crtical" align="left" style="background-color:white; border-color:red; border-style:double; width: auto;height:auto; overflow: hidden;   "> 
							<b style="font-style: italic; ">Critical Path:</b> &nbsp; <s:property value="alist"  />

						</div>
 --%>						
						
						<%-- <div>
								<!-- <h3>Send to Entrepreneaur</h3> -->
								<s:form action="p2eMsg">
								
									<s:textarea name="ideaCommit"></s:textarea>
									<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>
									<s:submit value="Send message"></s:submit>
								</s:form>
							
								<s:label cssClass="commitlabel" label="enpm" value="%{idea.ideaCommit}"></s:label>						
						</div>
						 --%>	
					
					</div>
		
		</div>
</div>



<!-- new code starts here -->
































<%-- 

	<div class="module_content">
		<article class="stats_graph">
		<header>
	<h3>Assigned Idea</h3>
	</header>
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Idea Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
				<s:textfield label="idea Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
				<s:textfield label="idea duration" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
				<s:textfield label="Idea description" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield>				

			</s:form>
 --%>
	<!-- 		      		<div  id="jRate" style="height:10px;width: 40px;"></div> -->
          <%-- <br>
          	<br>
          
		 	<s:form action="ratePm">
				<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="rater" />
				<s:hidden id="rating"  name="rating"></s:hidden>									
				<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>									
				<s:submit value="Send this rating"></s:submit>
      		</s:form>
  <br> --%>
          	<!-- <br> -->
          	<!-- 	<header> -->
	<%-- <h3>Manager of Idea</h3>
	</header>
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
							</tr>
						</thead>
<tr>
	<td>
		<s:property value="%{user.username}"/>
		</td>
			<td>
		<s:property value="%{user.firstname}"/>		
				</td>			
			<td>		
		<s:property value="%{user.lastname}"/>
				</td>		
			<td>		
		<s:property value="%{user.country}"/>	
				</td>		
                   			</tr>	 
                   </table>
                   </div> --%>                      					
    <!-- 		<div class="form-group"> -->
   <%--                 
      <s:form action="e2pMsg">
<s:textarea label="type your message" name="ideaCommit" ></s:textarea>
<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>									
<s:submit value="send message to Manager"></s:submit>
      </s:form>
      
      <s:label label="communication b/w En and Pm" value="%{idea.ideaCommit}"></s:label>
 		<div class="module-body">
 
      </div> --%>
     <!--  </div> -->
</body>
</html>