<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
 


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Ideas</title>
<link href="pm/css/myProject.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">

 
	<link href="pm/css/style.css" type="text/css" rel="stylesheet">
	

	

	 
	<link rel="stylesheet" href="en/css/layout.css" type="text/css"/>  
	


	 
    

    
    
 
 


    
	<link href="en/css/bootstrap.min.css" rel="stylesheet">

    

    
<!--         <link href="en/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    
    
    <!-- new code added for upload idea with javascript -->
 
<!-- <link href="pm/css/style.css" type="text/css" rel="stylesheet"> -->
<link rel="stylesheet" href="pm/css/jquery-ui.css">
    
    <script src="pm/js/jquery.min.js"></script>
<script src="pm/js/jquery-1.11.2.min.js"></script>
<script src="pm/js/jquery-ui.js"></script>

<script>
	$(function() {

		var dialog, form, ideaTitle = $("#ideaTitle"), ideaPostedOn = $("#ideaPostedOn"), ideaDomain = $("#ideaDomain"), dinid = $("#dinid"),ideaBudget = $("#ideaBudget"), ideaDuration = $("#ideaDuration"), ideaDescrption = $("#ideaDescrption"), allFields = $(
				[]).add(ideaTitle).add(ideaPostedOn).add(ideaDomain).add(dinid).add(ideaBudget)
				.add(ideaDuration).add(ideaDescrption),
				tips = $( ".validateTips" );

		function updateTips( t ) {
		      tips
		        .text( t )
		        .addClass( "ui-state-highlight" );
		      setTimeout(function() {
		        tips.removeClass( "ui-state-highlight", 1500 );
		      }, 500 );
		    }

		 function checkLength( o, n, min, max ) {
		      if ( o.val().length > max || o.val().length < min ) {
		        o.addClass( "ui-state-error" );
		        updateTips( "Length of " + n + " must be between " +
		          min + " and " + max + "." );
		        return false;
		      } else {
		        return true;
		      }
		    }

		function checkRegexp( o, regexp, n ) {
		      if ( !( regexp.test( o.val() ) ) ) {
		        o.addClass( "ui-state-error" );
		        updateTips( n );
		        return false;
		      } else {
		        return true;
		      }
		    }
	    
		function addTask() {
			var valid = true;
		      allFields.removeClass( "ui-state-error" );
		 
		      valid = valid && checkLength( ideaTitle, "ideaTitle", 3, 80 );
		      valid = valid && checkLength( ideaDomain, "ideaDomain", 3, 80 );
		      valid = valid && checkLength( dinid, "dinid", 1, 8 );
		      valid = valid && checkLength( ideaBudget, "ideaBudget", 1, 8 );

		      valid = valid && checkLength( ideaDescrption, "ideaDescrption", 3, 100 );
		 
		      valid = valid && checkRegexp( ideaTitle, /^[a-z]([0-9a-z_\s])+$/i, "ideaTitle may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
		      valid = valid && checkRegexp( ideaDomain, /^[a-z]([0-9a-z_\s])+$/i, "ideaDomain may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
		      valid = valid && checkRegexp( ideaDescrption, /^[a-z]([0-9a-z_\s])+$/i, "ideaDescrption may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
		      valid = valid && checkRegexp( ideaBudget, /^([0-9a-zA-Z])+$/, "ideaBudget field only allow : 0-9" );

		      valid = valid && checkRegexp( dinid, /^([0-9a-zA-Z])+$/, "dinid field only allow : 0-9" );

		      if ( valid ) {
				form[0].submit();
			//	allFields.removeClass("ui-state-error");
			dialog.dialog("close");
		      }
		      return valid;		
		      }

		dialog = $("#dialog-form").dialog({
			autoOpen : false,
			height : 500,
			width : 550,
			modal : true,
			buttons : {
				"Create an Idea" : addTask,
				Cancel : function() {
					dialog.dialog("close");
				}
			},
			close : function() {
				form[0].reset();
				allFields.removeClass("ui-state-error");
			}
		});

		form = dialog.find("#frmToSubmit").on("submit", function(event) {
			event.preventDefault();
			addTask();
		});

		$("#create-task").click(function() {
			dialog.dialog("open");

		});

		$("#btnSubmit").click(function() {
			$("#frmToSubmit").submit();
			//alert("testing");
		});

		$("#dinid")
				.on(
						"change",
						function() {
							var date = new Date($("#ideaPostedOn").val()), days = parseInt(
									$("#dinid").val(), 10);
							if (!isNaN(date.getTime())) {

								date.setDate(date.getDate() + days);
								$("#ideaDuration").val(date.toInputFormat());
							} else {
								alert("Invalid Date");
							}
						});
		Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
			var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = this.getDate().toString();
			return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
					+ (dd[1] ? dd : "0" + dd[0]); // padding
		};

		//$( "#format" ).change(function() {
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});
		//});
		// $( "#datepicker" ).datepicker();

		$("#ideaDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
	
</script>
<style>
	.ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
<!-- new code for javascript input ends here  -->
    
    
</head>
<body>



	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%> 
	
	<div class="leftpan">
			<div class="sidesublinks">
				<ul>
					<li><strong>		<s:a action="myIdeas">All Ideas
								<s:param name="ideaOwner" value="#session.userid" />
								</s:a></strong> </li>
					<li><a href="#"> Pending Ideas</a></li>
					<li><a href="#"> Completed Ideas </a></li>
					
						<li>	<s:a action="myIdeasganttchart"> Project Gantt Chart 
								<s:param name="ideaOwner" value="#session.userid" />
								</s:a></li>
				</ul>
			</div>
	</div>
	<div class="rightpan">
			
										<div class="contain">
											<div class="gantt">
											</div>
										</div>
	
	</div>








































			
			
			<!-- new button code for javascript IDEA UPLOAD starts here  -->
			
			
			
			<!-- 			<button id="create-task"  class="btn btn-primary" type="button">Create new Idea</button> -->

		<div class="clear"></div>
		<div id="dialog-form">

			<s:form id="frmToSubmit" action="postIdea">

				<label> Idea Title </label>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" id="ideaOwner" name="ideaOwner" />
				<input type="text" class="form-control" id="ideaTitle" name="ideaTitle"
					placeholder="Enter Project Title">


				<label> Idea Description </label>
				<textarea name="ideaDescrption" id="ideaDescrption" class="form-control" rows="3"></textarea>

				<label> Idea Category </label>

				<fieldset>
					<select name="ideaDomain" id="ideaDomain">
						<option selected="selected">web</option>
						<option>Android</option>
						<option>IOS</option>
						<option>Desktop</option>
						<option>other</option>

					</select>
				</fieldset>

				<label> Idea Budget </label>
				<input type="text" class="form-control" id="ideaBudget" name="ideaBudget"
					placeholder="Enter Project Budget">

<!-- 				 <label> Idea PostedOn </label> -->
				<input type="hidden" class="form-control" id="ideaPostedOn"
					name="ideaPostedOn" value="${currentDate}" />

				<label> Idea Duration in days </label>

				<input id="dinid" type="text" class="form-control" name="ideaDays" placeholder="Enter Idea Duration" />
	
<!-- 				<label> Idea Submission </label> -->

				<input id="ideaDuration" type="hidden" class="form-control" name="ideaDuration" readonly />
			</s:form>
		</div>
			
			
			<!-- new button code for javascript IDEA UPLOAD ends here  -->
			
			
		

    								




	



	<!--<script src="en/js/jquery.js"></script>-->
    <!--<script src="en/js/bootstrap.min.js"></script>-->
    <!-- nicescroll -->
    <script src="en/js/jquery.scrollTo.min.js"></script>
    <script src="en/js/jquery.nicescroll.js" type="text/javascript"></script>
    <!--custome script for all page-->
    <!--<script src="en/js/scripts.js"></script>-->
	
	<!-- 		<script src="pm/js/jquery.min.js"></script> -->

	<script src="pm/js/jquery.fn.gantt.js"></script>
	<script src="pm/js/jquery-te-1.4.0.min.js"></script>
	<script type="text/javascript">
	var src = [];
</script>
	<s:iterator value="ideas">
		<script>
			$(function() {

				var sdate = new Date("${ideaPostedOn}");
				var smilli = sdate.getTime();

				var ldate = new Date("${ideaDuration}");
				var lmilli = ldate.getTime();

				src.push({
					name : "${ideaTitle}",
					desc : "${ideaDescrption}",
					values : [ {
						from : "/Date(" + smilli + ")/",
						to : "/Date(" + lmilli + ")/",
						label : "${ideaDomain}",
						customClass : "ganttGreen"
					} ]

				});
			});
		</script>
	</s:iterator>
	<script>
		$(function() {
			"use strict";
			$(".gantt").gantt({
				source : src,
				navigate : "scroll",
				maxScale : "hours",
				itemsPerPage : 10
			});
			prettyPrint();

		});
	</script>
</body>
</html>