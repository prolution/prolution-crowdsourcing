<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

		<link rel="stylesheet" href="pm/css/bidsbyme.css">
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
		
   	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});
	</script>
</head>
<body>
<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
	
	
	<!-- new code Starts here  -->
	
	
<div class="mybidform">
		
			<div class="headertext">
			<h2> Completed Idea </h2>
			</div>
			
			<%-- <p><strong><span>Entrepreneur</span></strong> :<s:property value="%{user.firstname}"/>&nbsp;<s:property value="%{user.lastname}"/></p>		
			<p><strong><span>Country</span></strong><s:property value="%{user.country}"/></p>	
			<strong><span>User Rating:</span></strong><div  id="kRate" style="height:10px;width: 40px;"></div> --%>
			<br>							
			<div class="mybidleftpan">
					<div class="tabbar">
							<%-- <div class="sortdesc">
								<label> Sort By </label> &nbsp; 
								<span>
									<select>
										<option>By name</option>
										<option>By Amount</option>
									</select>
								</span>
							</div> --%>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
											<th>Project Manager</th>
											<!-- <th>Description</th> -->
											<th>Country </th>
											<th>Ratings</th>
											
											
                                    </tr>
                                 </thead>
                                		
									<tr>
										<%-- <td>
											<s:property value="%{user.username}"/>
										</td> --%>
										<td>
											<s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/>		
										</td>			
												
										<td>		
											<s:property value="%{user.country}"/>	
										</td>		
                   						<td>
                   								<div  id="kRate" style="height:10px;width: 40px;"></div>
                   						</td>
										
									</tr>
										
                                </tbody>
                            </table>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Idea Title</h4>
		 				 <h4><s:property value="%{idea.ideaTitle}"/></h4>  
		 				<ul>
		 					<li>Description:<p style="color: green;"><s:property value="%{idea.ideaDescrption}"/></p></li>
		 					<li>Duration:<p style="color: green;"><s:property value="%{idea.ideaDuration}"/></p></li>
		 					<li>Budget: <p style="color: green;"><s:property value="%{idea.ideaBudget}"/></p></li>
		 					
		 					<%-- <s:textfield label="Project Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
							<s:textfield label="Project Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
							<s:textfield label="Project" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
							<s:textfield label="Project" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield>
		 					 --%>
		 					
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the Idea </p>
		 			
		 			</div>
			
			</div>
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>











<!-- new code finished here  -->




<%-- 


	<div class="module_content">
		<article class="stats_graph">
		<header>
	<h3>Completed Idea</h3>
	</header>
		<div class="form-group">
			<s:form action="updateProj"> 
				<s:textfield label="Idea Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
				<s:textfield label="idea Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
				<s:textfield label="idea duration" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
				<s:textfield label="Idea description" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield>				
			</s:form>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
		<div class="module-body">
			<div class="tab_container">
          <br>

          	<br>
          		<header>
	<h3>Manager of Idea</h3>
	</header>
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Rating</th>

							</tr>
						</thead>
<tr>
	<td>
		<s:property value="%{user.username}"/>
		</td>
			<td>
		<s:property value="%{user.firstname}"/>		
				</td>			
			<td>		
		<s:property value="%{user.lastname}"/>
				</td>		
			<td>		
		<s:property value="%{user.country}"/>	
				</td>		
                   			<td>
                   								<div  id="kRate" style="height:10px;width: 40px;"></div>
                   			</td>
                   			</tr>	 
                   </table>
             
                        					
 --%>
 </body>
</html>