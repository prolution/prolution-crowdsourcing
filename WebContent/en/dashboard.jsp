<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>



<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>Dashboard</title>
	
	 <link href="en/css/dashboard.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet"> 

	</head>


<body>

<div class="leftpan">

</div>
<div class="centerpan">
					
					<div class="Midpan">
						<%-- <div class="col-lg-12">
                        	<div class="alert alert-info alert-dismissable">
                            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            	<i class="fa fa-info-circle"></i>  <strong>You Have Some Pending Requests</strong> Click <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">Check Notfications</a>
                        	</div>
                    	</div> --%>
                    	<div class="textstyle">
                    	
                    		<h4 class="subheaderhome">  
                    			<strong> <s:a action="">My Ideas 
										<s:param name="" value="#session.userid" />
										</s:a>  
								</strong>   |  <s:a action="">Find Project Managers
										  <s:param name="" value="#session.userid" />
										  </s:a> 
							</h4>
                    	
								 
						
                    	</div>
                    	
                    	<div class="notifyticker">
                    		
                    		<i class="fa fa-info-circle"></i><strong> There are Thousands of Jobs waiting for you</strong>
                    		<ul>
                    			<li> Upload Great Ideas </li>
                    			<li> Select Bids on Ideas</li>
                    			<li> Hire your Project Managers </li>
                    		</ul>
                    	
                    	</div>
                    	
                    	<div class="tabbar">
                    	
                    				
                    	
						<%-- 		<div class="sortdesc">
										<label> Sort By </label> &nbsp; 
									<span>
										<select>
											<option>By name</option>
											<option>By Job</option>
										</select>
									</span>
								</div> --%>
							<!-- <div class="verticalline">
							</div> -->
						</div>
						
						<div>
						
									<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											<th>Person</th>
											<!-- <th>Project Description</th> -->
											<th>Bid Idea</th>
											<th>Bid Proposal</th>
											<th>Review</th>
											
    										
                                    </tr>
                                </thead>
                                <tbody>
                                    	<%-- <s:iterator value="project"> --%>
											<tr>
												<%-- <td>
													<s:a action="projectTasks"><s:property value="proTitle" />
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
												</td>
												<td><s:property value="proDescrption" /></td>
												<td><s:property value="proDomain" /></td>
												<td><s:property value="proPostedOn" /></td>
												<td><s:property value="proBudget" /></td>
												<td><s:property value="proDuration" /></td>
												<td>
													<a href="#">
														<i class="glyphicon glyphicon-remove-circle">
														</i>
													</a>
												</td> --%>
											</tr>
										<%-- </s:iterator> --%>
										  
                                    
                                    
                                </tbody>
                            </table>
                        </div>
									
						
						
						
						</div>
			

					</div>
					<div class="rightpan">
							<div class="messagesblock">
										<h3>Inbox</h3>
										<hr>
									<ul>
										
										<li>Action Required</li>
										<li>Messages</li>
										<li>Invites</li>
										<li>All</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							<div class="idea">
							<div class="messagesblock">
										<h3>Ideas</h3>
										<hr>
									<ul>
										
										<li>No.of Ideas</li>
										<li>Completed Ideas</li>
										<li>Pending Ideas</li>
										<li>Completed Tasks</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							</div>
							
					</div>
</div>


	

</body>

</html>








