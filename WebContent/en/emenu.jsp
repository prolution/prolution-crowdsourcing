<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title></title>
	
	<link rel="stylesheet" href="en/css/layout.css" type="text/css"/>
	<!-- <link href="en/css/sb-admin.css" rel="stylesheet" type="text/css" > -->
   
    <link href="en/css/simple-sidebar.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="en/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    
  
	
</head>
<body>
	<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
        <form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
        
            <ul class="sidebar-nav">
            
            <li>
					<s:a action="myEnProfile">My Profile
					<s:param name="userid" value="#session.userid" />
					</s:a>
			 </li>    
              <%-- <li>
				<s:a action="ideaUpload">Upload Ideas
				<s:param name="ideaOwner" value="#session.userid" />
				</s:a> 
			</li>
			<li>
				<s:a action="myIdeas">Current ideas
				<s:param name="ideaOwner" value="#session.userid" />
				</s:a> 
			</li> --%>
			
			
		
			<li> 
					<s:a action="findusersEn">Find Users
					<s:param name="userid" value="#session.userid" />
					</s:a>
				
			</li>
			<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"> Ideas <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
								<s:a action="ideaUpload">Upload Ideas
								<s:param name="ideaOwner" value="#session.userid" />
								</s:a> 
							</li>
							<li>
								<s:a action="myIdeas">Current ideas
								<s:param name="ideaOwner" value="#session.userid" />
								</s:a> 
							</li>
                        </ul>
            </li>
			<li>
		   			<s:url id="lockscreen" action="lockscreen">
		   			<!--<s:property value="#session.userid"/>-->
		   			<s:param name="userid" value="#session.userid"></s:param>
					</s:url> <s:a href="%{lockscreen}">Lock Screen</s:a>
			</li>
			<li>
					<s:form action="logout">
                         	<a href="logout"> Log Out</a>
                         	
                            <!-- <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a> -->
                            </s:form>
			</li>
            </ul>
        </div>
</div>

      	
		
		
		
	
</body>
</html>