<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<link rel="stylesheet" href="pm/css/bidsbyme.css">
	
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet">
	
 	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});

  </script>
  
</head>
<body>


<div class="mybidform">
		
			<div class="headertext">
			<h2> Bidder Information For the Idea '<s:property value="%{idea.ideaTitle}"/>'</h2>
			</div>
			
			<div class="mybidleftpan">
					<div class="tabbar">
							<div class="sortdesc">
								
							</div>
							
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
										
										<th>Name</th>
										<th>country</th>
										<th>Bids Amount</th>
										<th>Rating</th>
										<th>Accept</th>
    									
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
										<%-- <td>
											<s:property value="%{user.username}"/>
										</td> --%>
										<td>
											<s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/>		
										</td>			
										
										<td>		
											<s:property value="%{user.country}"/>	
										</td>	
										<td>		
											<s:property value="%{bidsAmount}"/>	
										</td>	
										<td>
											<div  id="kRate" style="height:10px;width: 40px;"></div>
										</td>
										<td>
												<s:form action="assignIdea">						
						 						<s:hidden name="ideaId" value="%{idea.ideaId}" ></s:hidden>
						 						<s:hidden name="ideaFlag" value="2" ></s:hidden>
						 						<s:hidden name="ideaPM" value="%{user.userid}" ></s:hidden>
    											<s:submit cssClass="btn btn-success" value="Accept"></s:submit>
                   							</s:form>
										</td>
                   					</tr>	 
      
                                </tbody>
                            </table>
                            <%-- <div class="taskacceptbutton">
                            	<s:form action="assignTask" >
									<s:hidden name="taskOwner" value="%{user.userid}" ></s:hidden>
									<s:hidden name="taskID" value="%{task.taskID}" ></s:hidden>	    			
	  								<s:hidden name="taskFlag" value="2" ></s:hidden>    
      								<s:submit cssClass="btn btn-success" value="Accept bids"></s:submit>
                   				</s:form>
                   			</div> --%>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Idea Title</h4>
		 				 <h4><s:property value="%{idea.ideaTitle}"/></h4>  
		 				<ul>
		 					<li>Description<p><s:property value="%{idea.ideaDescrption}"/></p></li>
		 					<li>Duration<p><s:property value="%{idea.ideaDuration}"/></p></li>
		 					<li>Budget <p><s:property value="%{idea.ideaBudget}"/></p></li>
		 					
		 					
		 					
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the task </p>
		 			
		 			</div>
			
			</div>
			
		
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>



















<%-- 



	<div class="module_content">
		<article class="stats_graph">
		<header><h3>My Idea</h3></header>
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Idea Name" value="%{idea.ideaTitle}"
					name="ideaTitle"></s:textfield>
				<s:textfield label="idea Cost" name="ideaBudget"
					value="%{idea.ideaBudget}"></s:textfield>
				<s:textfield label="idea duration" name="ideaDuration"
					value="%{idea.ideaDuration}"></s:textfield>
				<s:textfield label="Idea description" name="ideaDescrption"
					value="%{idea.ideaDescrption}"></s:textfield>
										<s:submit value="Update" ></s:submit>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden>

			</s:form>
	<section id="main" class="column"> <article
		class="module width_full"> <header>
	<h3>Idea Bidder</h3>
	</header>
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
		<div class="module-body">
			<div class="tab_container">
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Bids Amount</th>

								<th>Rating</th>

							</tr>
						</thead>

			
			<tr>
					<td>
							<s:property  value="user.username"/>
					</td>
					<td>
		
							<s:property  value="user.firstname"/>		
					</td>			
					<td>		
							<s:property  value="user.lastname"/>
					</td>		
					<td>		
							<s:property  value="user.country"/>	
					</td>
					<td>		
							<s:property  value="bidsAmount"/>	
					</td>		
					<td>
											<div  id="kRate" style="height:10px;width: 40px;"></div>
							    			
	    			
                   </td>
                   			</tr>	 
       </table>
      				<s:form action="assignIdea">						
						 <s:hidden name="ideaId" value="%{idea.ideaId}" ></s:hidden>
						 <s:hidden name="ideaFlag" value="2" ></s:hidden>
						 <s:hidden name="ideaPM" value="%{user.userid}" ></s:hidden>
    				
      				<s:submit value="Accept"></s:submit>
                   </s:form>            
          </div> --%>              					
</body>
</html>