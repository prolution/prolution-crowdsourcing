<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
			
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
		<link href="pm/css/uploadingproject.css" rel="stylesheet" type="text/css">

			
			    	<link rel="stylesheet" href="pm/css/jquery-ui.css"> 
          	<link rel="stylesheet" href="pm/css/style.css">  
     
	
			<script src="pm/js/jquery-1.11.2.min.js"></script>
  			<script src="pm/js/jquery-ui.js"></script>
	
<script>

	$(function() {
		window.history.forward();function noBack(){window.history.forward();}
			/* Original method*/
		 $("#btnSubmit").click(function() {
				$("#frmToSubmit").submit();
				
				
				
				/*alert("testing");*/
				
			});


		 		 
		$("#dinid")
				.on(
						"change",
						function() {
							var date = new Date($("#ideaPostedOn").val()), days = parseInt(
									$("#dinid").val(), 10);
							if (!isNaN(date.getTime())) {

								date.setDate(date.getDate() + days);
								$("#ideaDuration").val(date.toInputFormat());
							} else {
								alert("Invalid Date");
							}
						});
		Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
			var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = this.getDate().toString();
			return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
					+ (dd[1] ? dd : "0" + dd[0]); // padding
		};

		//$( "#format" ).change(function() {
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});
		//});
		// $( "#datepicker" ).datepicker();

	});
	$(function() {
		$("#ideaDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
	
	
	
	
</script>
<script type='text/javascript'>
CharacterCount = function(TextArea,FieldToCount){
	var proDescrption = document.getElementById(TextArea);
	var myLabel = document.getElementById(FieldToCount); 
	if(!proDescrption || !proDescrption){return false}; // catches errors
	var MaxChars =  proDescrption.maxLengh;
	if(!MaxChars){MaxChars =  proDescrption.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};
	var remainingChars =   MaxChars - proDescrption.value.length
	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars
}

//SETUP!!
setInterval(function(){CharacterCount('proDescrption','CharCountLabel1')},55);
</script>

<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}
</style>


</head>

<body>

<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>



<div class="forminput">

				<div class="formfields">
					<%-- <s:form id="frmToSubmit" action="postIdea" validate="true" method="post"> --%>
					<s:form id="frmToSubmit" action="postIdea" method="post">					
						<s:actionerror />
   								<h1> Post Your Idea </h1>
   								<p class="subheading">Describe the Idea Details</p>
						    <p class="name"> 
							    <label for="ideaTitle"> Idea Title </label>
		  				 		<input type="hidden" value="<%=session.getAttribute("userid")%>" name="ideaOwner"/>
								<input type="text"  name="ideaTitle" placeholder="Enter Idea Title" required="required" maxlength="25"/>
								<p class="sbscript">Max 25 words</p>
    						</p> 
    						<p class="text"> 
					
        						<label for="ideaDescrption"> Describe your Idea </label>
								<textarea name="ideaDescrption"  id="proDescrption" rows="10" required="required" cols="50" maxlength="250" minlength="80"></textarea>
								<div id='CharCountLabel1'></div>
        							
    						</p>
   							<p class="procategory">
           						<label for="ideaDomain">Choose the Idea Category</label>
           						<!-- <fieldset> -->
								<select name="ideaDomain" id="ideaDomain" required="required">
									<option selected="selected">Select Category</option>
									<option>web</option>
									<option>Android</option>
									<option>IOS</option>
									<option>Desktop</option>
									<option>other</option>
								</select>
								<!-- </fieldset> -->
						   </p>
    							<p class="email">
    							<label for="ideaBudget">Total Budget of the Idea</label> 
								<input type="number" name="ideaBudget" placeholder="$" required="required" min="100" max="1000"> 
        						<!-- <label for="email">E-mail</label> 
        						<input type="text" name="email" id="email" /> --> 
        						<input type="hidden" id="ideaPostedOn" name="ideaPostedOn" value="${currentDate}" />
				
    						</p> 
   
						    <p class="web"> 
        	
        						<label for="ideaDays"> Total Duration of Idea in [DAYS] </label>
								<input id="dinid" type="number" name="ideaDays" placeholder="days" required="required" min="1" max="100"/>
								 
    						</p> 
   
    						 
    						<p>
    							<label for="ideaDuration"> Idea Deadline </label> 
								<input id="ideaDuration" type="text" name="ideaDuration" readonly />
								
								
       							
   							</p>
   							
    						<p class="submitnew">
        							<s:submit value="Continue"  cssClass="btn btn-success" ></s:submit> 
    						</p> 
    						
   
   							
   
				</s:form>

			</div>
			<%-- 
			<div class="formfields">

					<s:form id="frmToSubmit" action="projectposted" class="form">
						<s:actionerror />
							<h1> Post Your Idea </h1>
   								<p class="subheading">Describe the Idea Details</p>
						    <p class="name"> 
							    <label for="ideaTitle"> Idea Title </label>
		  				 		<input type="hidden" value="<%=session.getAttribute("userid")%>" name="ideaOwner"/>
								<input type="text"  name="ideaTitle" placeholder="Enter Idea Title" required="required" maxlength="25"/>
								<p class="sbscript">Max 25 words</p>
    						</p>
    						
    						<p class="text"> 
        						<!-- <textarea name="text"></textarea> --> 
					
        						<label for="proDescrption"> Describe your Project </label>
								<textarea name="proDescrption"  id="proDescrption" rows="10" required="required" cols="50" maxlength="250" minlength="80"></textarea>
								<div id='CharCountLabel1'></div>
        
    						</p>
   							<p class="procategory">
           						<label for="proDomain">Choose the Project Category</label>
           						<!-- <fieldset> -->
								<select name="proDomain" id="proDomain" required="required">
									<option selected="selected">Select Category</option>
									<option>web</option>
									<option>Android</option>
									<option>IOS</option>
									<option>Desktop</option>
									<option>other</option>
								</select>
								<!-- </fieldset> -->
   
						   </p>
    							<p class="email">
    							<label for="proBudget">Total Budget of the Project</label> 
								<input type="number" name="proBudget" placeholder="$" required="required" min="100" max="1000"> 
        						<!-- <label for="email">E-mail</label> 
        						<input type="text" name="email" id="email" /> --> 
        
    						</p> 
   
						    <p class="web"> 
        	
        						<label for="proDays"> Total Duration of Project in [DAYS] </label>
								<input id="dinid" type="number" name="proDays" placeholder="days" required="required" min="1" max="100"/> 
    						</p> 
   
    						 
    						<p>
    							<label for="proDuration"> Project Deadline </label> 
								<input id="proDuration" type="text" name="proDuration" readonly />
   							</p>
   							
    						<p class="submitnew"> 
        							<s:submit value="Continue"  cssClass="btn btn-success" ></s:submit>
    						</p> 
    						
   
   							
   
				</s:form>

			</div>
 --%>			
 	
			<div class="rightinfor">
						<h4>Need Help With Your Post ? </h4>
						<p>We can help you describe what you need. </p>
						<p><a href="#">Contact Us </a> ?</p>
						
			</div>
			<div class="rightinformto">
						<div class="infortext">		
						<p>*Note</p>
						<p>This Project Will Only Be Visible to You,
						Proceed Further to add tasks and
						Hire Contributors</p>
						</div>
			</div>
</div>



<!-- new code finished here  -->


				<%-- <s:form id="frmToSubmit" action="postIdea" validate="true" method="post"> --%>
			
		<%-- 		<label> Idea Title </label>
		  
		 		<input type="hidden" value="${sessionScope.userid}" name="ideaOwner"/>
				<input type="text" class="form-control" name="ideaTitle" placeholder="Enter Project Title" required="required" />
		 --%>
<!-- 				<label> Idea Description </label>
				<textarea name="ideaDescrption" class="form-control" rows="3" ></textarea>
 -->
<%-- 				<label> Idea Category </label>

				<fieldset>
				<select name="ideaDomain" id="ideaDomain">
					<option selected="selected">web</option>
					<option>Android</option>
					<option>IOS</option>
					<option>Desktop</option>
					<option>other</option>

					</select>
		
				</fieldset> --%>
				<%-- <label> Idea Budget </label>
				<input type="text" class="form-control" name="ideaBudget" placeholder="Enter Project Budget" />
				<input type="hidden"  class="form-control" id="ideaPostedOn" name="ideaPostedOn" value="${currentDate}" /> --%>
				
				<!-- <label> Idea Duration in days </label>
				<input id="dinid" type="text" class="form-control" name="ideaDays" placeholder="Enter Idea Duration" /> -->
				<%-- <label> Idea Submission </label>
				<input id="ideaDuration" type="text" class="form-control" name="ideaDuration" readonly />
       			<input type="button" class="btn btn-primary" id="btnSubmit" value="Submit Idea"/>

	</s:form>
                          
                          </div>
                           
                      
                     
                  </div>
              </div>
    

 --%>


		 
</body>
</html>