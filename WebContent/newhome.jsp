<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pro-Lutions :: Official Website</title>

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="bootstrap/css/prolution-style.css">
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">



<script src="bootstrap/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
 <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="bootstrap/css/prolution-style.css">
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap/css/dropdownstyle.css" type="text/css" media="screen"/>
		
		<link rel="stylesheet" href="bootstrap/css/slidercss.css" type="text/css" media="screen"/>
		
        <script src="bootstrap/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>



</head>
<body>
        <time></time>

<script src="http://localhost:8000/socket.io/socket.io.js" ></script>
<script src="js/jquery-1.11.2.min.js" ></script>
<script src="js/notify.min.js"></script>
<script type="text/javascript">
	 var socket = io.connect('http://localhost:8000');

	 $(document).ready(function(){

	     socket.on('notification', function(data) {        	        	 

     	$("#nuser").html(data.counts["cnt"]+" People are on Pro-Lutions");

		     });
		 
		 setInterval(function(){
			 socket.emit('getId', {ideaId: 'helloooo'});
				}, 5000);
	 });
	 </script>


	
	<nav class="navbar navbar-default navbar-fixed-top">
	<div class="newcontainer">
	
				<ul class="topmenunew">
					
					<li><a href="findwork">Find Work</a></li>
					<li><a href="crowduserSearch">Find Pro-Users</a></li>
					<li><a href="#our-work">How it Works</a></li>
					<!-- <li><a href="projectSearch">Browse Projects</a></li> -->
					<li><a href="prelogin">LogIn</a> <span style="color:white;">or</span> <a href="preSignup">Signup</a></li>	
						
					<%-- <s:form  action="prelogin"><s:submit value="SignIn" cssClass="btn" />
					</s:form> --%>	
					<%-- <s:form action="preSignup"><s:submit  value="SignUp" cssClass="btn" /></s:form> --%>
					
					<!--  new code for pop window -->
					        
	        
	        		<!--  new code for pop window -->
					
				</ul>
				
				
				
	
	</div>
	
	<div class="container">
				<div class="logo-wrapper">
					<a class="navbar-brand" href="goHome"> <!-- Pro<em>- Lution</em>-->
						<img class="projectlogo" src="bootstrap/img/ProLogo.png">

					</a>
				</div>
	
		<div class="row">
				

			<!-- <div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<i class="fa fa-bars"></i>
				</button>
				
			</div>
			<div class="collapse navbar-collapse" id="main-menu">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="goHome">Home</a></li>
					<li><a href="#our-work">How it Works</a></li>
					<li><a href="userSearch">Find Freelancers</a></li>
					<li><a href="projectSearch">Browse Projects</a></li>

						
				</ul>
					
			</div> -->
			<%-- 
			  <div class="form-group input-group">
                                <span class="input-group-addon">@</span>
                                <input type="text" class="form-control" placeholder="Username">
                            </div>
                            <div class="form-group input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></span>
                            </div> --%>
                            
                        <%--     <div class="btn-group">
                                          <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"> Dropdown <span class="caret"></span> </button>
                                          <ul class="dropdown-menu">
                                              <li><a href="#">Dropdown link 1</a></li>
                                              <li><a href="#">Dropdown link 2</a></li>
                                              <li><a href="#">Dropdown link 3</a></li>
                                          </ul>
                                      </div> --%>
                                      
                                      
				<form id="ui_element" class="sb_wrapper">
                    <p>
						<span class="sb_down"></span>
						<input class="sb_input" type="text" placeholder="Search Crowd Users"/>
						<input class="sb_search" type="submit" value="Go"/>
					</p>
					<ul class="sb_dropdown" style="display:none;">
						<li class="sb_filter">Filter your search</li>
						<li><input type="checkbox"/><label for="all"><strong>All Categories</strong></label></li>
						<li><input type="checkbox"/><label for="Enterpreneurs">Enterpreneurs</label></li>
						<li><input type="checkbox"/><label for="Projectmanagers">Project Managers</label></li>
						<li><input type="checkbox"/><label for="Contributors">Contributors</label></li>
						
						
					</ul>
                </form>
		</div> 
	</div>
	</nav>
	
	<!--  new code for the slider  -->
	<div class="flex-container">
	    			<div class="flexslider">
    			    <ul class="slides">
            			<li>
                			<a href="#"><img src="bootstrap/img/slide1.jpg" /></a>
            			</li>
	 
    			        <li>
			                <img src="bootstrap/img/slide2.jpg" />
			            </li>
 
            			<li>
                			<img src="bootstrap/img/slide3.jpg" />
                			
            			</li>
            			<li>
                			<img src="bootstrap/img/slide1.jpg" />
                			
            			</li>
            			<li>
			                <img src="bootstrap/img/slide2.jpg" />
			            </li>
        			</ul>
    				</div>
	</div>
	<!--  new code for the slider  -->
	<div class="sliderupper">
		
		<img src="bootstrap/img/sliderafter.png"/>  
		
	</div>
	
	<!--  div settings for register button  -->
	<div>
	
		
	<!-- 	 <button type="button" class="btn btn-success">Register Now</button> --> 
	
		<a href="preSignup" class="btn btn-success">Register Now</a>
	        
	        
	        
		 
	</div>
	<!--  div settings for register button  ends here -->
	
	<div class="cta">
         
          <div class="col-md-12">
            <!-- <h3 class="htxt">Where Ideas Meet Solutions</h3> -->
            <!--<span>Join us</span>-->
	    		<%-- <span> have an account? </span> <s:form  action="prelogin"><s:submit value="SignIn" cssClass="btn" /></s:form><span> or </span><s:form action="preSignup"><s:submit  value="SignUp" cssClass="btn" /></s:form> --%>
    <!-- new images placed here  -->       			
           		<div class="col-md-4 col-sm-12" id="our-work">
	      			<h3>Be an <br><em>enterpreneur</em></h3>
              		<img class="first-icon" src="bootstrap/img/enterpreneur.png" alt="">
        	    </div>
        	    <div class="col-md-4 col-sm-12">
              		<h3>Become A<br><em>Project Manager</em></h3>
              		<img class="second-icon" src="bootstrap/img/projectmanager.png" alt="">
            	</div>		
	           	<div  class="col-md-4 col-sm-12">
              		<h3>Contribute<br><em>Your Skills</em></h3>
              		<img class="second-icon" src="bootstrap/img/contributor.png" alt="">
            	</div>	
    <!-- new images placed here  -->
    
          </div>
        
    </div>


<%-- <footer>
           <table>
           
           	<tr>
           		<td>
           			Home 
           		</td>
           		</br>
           		<td>
           			How it works 
           		</td>
           	</tr>
           	
           </table>
			<p align="center"><strong>Copyrights@ prolutions.com</strong></p>
			
			
                <div class="col-md-6 col-sm-6 col-sm-12">
                    <ul class="social">
                        <li><a href="https://www.facebook.com/prolution.official" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                        <li><a href="#" class="fa fa-google-plus"></a></li>
                    </ul>
                </div>
            
</footer>
 --%>
 
 
 <!-- ---------------------------------------------------------------------------- -->
 
 
 
 
 
 	<%-- 	<footer class="footer-distributed">

			<div class="footer-left">
				<a href="goHome"> <!-- Pro<em>- Lution</em>-->
 						<img class="projectlogo" src="bootstrap/img/ProLogo.png">

					</a>
 				

				<p class="footer-links">
					<a href="#">Home</a>
					�
					<a href="#">How it Works</a>
					�
					<a href="#">Find Work</a>
					�
					<a href="#">Faq</a>
					�
					<a href="#">Contact</a>
				</p>

				<p class="footer-company-name">Copyrights@ prolutions.com.pk &copy; 2015</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>21 Revolution Street</span> Paris, France</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">support@company.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>About the company</span>
					Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

			</div>

		</footer>
		 --%>
 <!--  footer script starts here  -->
<footer>

<div class="container">
  	<div class="col-sm-8 col-sm-offset-2 text-center">
      <h2>www.prolutions.com.pk</h2>
      
      <hr>
      <h4>
        Join us Today With the largest platform
      </h4>
      <p>Follow us on <!--  <a href="http://bootply.com">Bootstrap Playground</a>, Bootply. --></p>
      <hr>
         <h2 id="nuser"></h2>
             	            
      <ul class="list-inline center-block">
      	<li class="footerlist"><a href="https://www.facebook.com/prolution.official" class="fa fa-facebook"></a></li>
        <li class="footerlist"><a href="https://twitter.com/pro_lution"><i class="fa fa-twitter"></i></a></li>
        <li class="footerlist"><a href="https://www.linkedin.com/grp/home?gid=6794518&sort=RECENT"><i class="fa fa-linkedin"></i></a></li>
        
      </ul>
      
  	</div><!--/col-->
</div><!--/container-->
  

<div>
  <div class="container">
    <p class="text-muted">Courtesy of @ <a href="#">prolutions.com.pk</a></p>
  </div>
</div>

<ul class="nav pull-right scroll-top">
  <li><a href="#" title="Scroll to top"><i class="glyphicon glyphicon-chevron-up"></i></a></li>
</ul>
</footer>


<!--  footer script ends here  -->



<script src="bootstrap/js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="bootstrap/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="bootstrap/js/vendor/bootstrap.js"></script>
        <script src="bootstrap/js/main.js"></script>



<script src="bootstrap/js/vendor/jquery-1.11.0.min.js"></script>
<script>
	window.jQuery
			|| document
					.write('<script src="bootstrap/js/vendor/jquery-1.11.0.min.js"><\/script>')
</script>
<script src="bootstrap/js/vendor/bootstrap.js"></script>
<script src="bootstrap/js/main.js"></script>


 <script src="boostrap/js/jquery.js"></script>
    <script src="bootstrap/js/vendor/bootstrap.min.js"></script>
    
    
    

<!--  new javascript for the dropdown search starts here  -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript">
            $(function() {
				/**
				* the element
				*/
				var $ui 		= $('#ui_element');
				
				/**
				* on focus and on click display the dropdown, 
				* and change the arrow image
				*/
				$ui.find('.sb_input').bind('focus click',function(){
					$ui.find('.sb_down')
					   .addClass('sb_up')
					   .removeClass('sb_down')
					   .andSelf()
					   .find('.sb_dropdown')
					   .show();
				});
				
				/**
				* on mouse leave hide the dropdown, 
				* and change the arrow image
				*/
				$ui.bind('mouseleave',function(){
					$ui.find('.sb_up')
					   .addClass('sb_down')
					   .removeClass('sb_up')
					   .andSelf()
					   .find('.sb_dropdown')
					   .hide();
				});
				
				/**
				* selecting all checkboxes
				*/
				$ui.find('.sb_dropdown').find('label[for="all"]').prev().bind('click',function(){
					$(this).parent().siblings().find(':checkbox').attr('checked',this.checked).attr('disabled',this.checked);
				});
            });
        </script>
<!--  new javascript for the dropdown search ends here  -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="bootstrap/js/jquery.flexslider-min.js"></script>
<script>
    $(document).ready(function () {
        $('.flexslider').flexslider({
            animation: 'fade',
            controlsContainer: '.flexslider'
        });
    });
</script>

</body>




</html>