<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>



<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>Profile</title>
	<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/myProfile.css" rel="stylesheet">
    
	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});
	</script>
	
    
	</head>


<body>

	
	<div class="leftbarpanel">
		
		<div class="imgboxlarge">
		
		</div>
	  	
	  	<div class="linkdescription">
	  	
	  		<ul>
	   			<li> <a href="#"> Overview </a></li>
	   			
	   			<li> <a href="#">Job History </a></li>
	   			
	  			<li> <a href="#">Portfolio</a></li>
	  			<li> <a href="#">Skills</a></li>
	  			<hr>
	  			<li> <a href="#">Contact Info </a></li>
	  			<li> <a href="#">Privacy Settings </a> </li>
	  			<!-- <li> <a href="#">Public View of Profile</a>  </li> -->
	  			<li>	<s:url id="viewEnpublicview" action="viewEnpublicview">
			    		<s:param name="userid" value="%{userid}"></s:param>
			    			<s:param name="username" value="%{username}"></s:param>
			          			 </s:url>			
							<s:a href="%{viewEnpublicview}">Public View of Profile</s:a>
				</li>
				<%-- <li>	
						<s:url id="viewEnpublicview" action="viewEnpublicview">
			        	<s:param name="userid" value="%{userid}"></s:param>
			        		<s:param name="username" value="%{username}"></s:param>
			        	 	</s:url>			
					<s:a href="%{viewEnpublicview}"><s:property value="fullName" /></s:a>
									
				</li> --%>
	  			<hr>
	    	</ul>
		</div>
		
		
		<div>
			<div class="smallthumb">  </div>
			 <span class="logintime"> Last-Signin: May-06-2015</span> 
		</div>
		
		
	</div>
	
	<div class="Midpanel">
	
	
			<div class="username"> 
					<h1> <s:property value="%{user.firstname}"/> <s:property value="%{user.lastname}"/></h1>
					<h4> <s:property value="%{user.usertype}"/> </h4> <!--Project Manager-->
					<p> <span> <s:property value="%{user.country}"/> </span> | <span> State, Province</span></p>
					
					<hr>
			</div>
			<div class="midpanleftside">
			
			
							
							              <!-- page start-->
              <div class="row">
                 <div class="col-lg-12">
                    <section class="panel">
                          <header class="panel-heading tab-bg-info">
                              <ul class="nav nav-tabs">
                                  <li class="active">
                                      <a data-toggle="tab" href="#profile">
                                          <i class="icon-home"></i> 
                                             Overview
                                      </a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#recent-activity">
                                          <i class="icon-user"></i> 
                                          Reviews
                                      </a>
                                  </li>
                                  <li class="">
                                      <a data-toggle="tab" href="#edit-profile">
                                          <!-- <i class="icon-envelope"></i> -->
                                          Portfolio
                                      </a>
                                  </li>
                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content">
                                  <!-- profile -->
                                  <div id="profile" class="tab-pane active">
                                    <section class="panel">
                                      <div class="bio-graph-heading">
                                                <p> Minimum Hourly Rate $5 </p>
                                                Hello I am <s:property value="%{user.firstname}"/> <s:property value="%{user.lastname}"/>, project manager specialized in the following projects
                                                <br>
                                                Desktop Applications , Mobile Applications, Web Applications
                                      </div>
                                      <hr>
                                      <div class="panel-body bio-graph-info">
                                          <h4 class="sblinks">Contact Info</h4>
                                          <div class="row">
                                              
                                              
                                                  <p><span>Email </span>:<s:property value="%{user.email}"/></p>
                                                  <p><span>Mobile </span>: (+6283) 456 789</p>
                                                  <p><span>Phone </span>:  (+021) 956 789123</p>
                                              
                                              
                                              
                                          </div>
                                          <hr>
                                          <h4 class="sblinks">Skills</h4>
                                          <div class="row">
                                              
                                              
                                                  <p><span>Mobile Development </span>:-------------</p>
                                                  <p><span>Desktop Development </span>:------------</p>
                                                  <p><span>Web Development </span>:----------------</p>
                                              
                                              
                                              
                                          </div>
                                          <hr>
                                          <h4 class="sblinks">Education</h4>
                                          <div class="row">
                                              
                                              
                                              
                                              
                                          </div>
                                      </div>
                                    </section>
                                      <section>
                                          <div class="row">                                              
                                          </div>
                                      </section>
                                  </div>
                                  
                                  <div id="recent-activity" class="tab-pane">
                                      <div class="profile-activity">                                          
                                          <div class="act-time">                                      
                                              <div class="activity-body act-in">
                                                  <span class="arrow"></span>
                                                  <div class="text">
                                                      <p class="attribution"><a href="#">Muhammad Jaffar </a> at 4:25pm, 30th March 2015</p>
                                                      <p>It was a great experience working with <s:property value="%{user.firstname}"/> <s:property value="%{user.lastname}"/>, he completed the project within the deadline </p>
                                                  </div>
                                                  <div class="text">
                                                      <p class="attribution"><a href="#">Mirza Safwan </a> at 5:00pm, 5th May 2015</p>
                                                      <p>It was a great experience working with <s:property value="%{user.firstname}"/> <s:property value="%{user.lastname}"/>, he completed the project within the deadline </p>
                                                  </div>
                                              </div>
                                          </div>
                                          
                                          
                                          										
                                      </div>
                                  </div>
                                  
                                  <!-- edit-profile -->
                                  <div id="edit-profile" class="tab-pane">
                                                                              
                                          <div class="panel-body bio-graph-info">
													<h4>Not Uploaded Yet ?</h4>
												upload your portfolio <a href="#">Now</a>
                                          </div>
                                    
                                  </div>
                              </div>
                          </div>
                      </section>
                 </div>
              </div>

              <!-- page end-->
			
			
			
			</div>
			
			<div class="midpanrightside">
				<div class="ratingbox">
				
					<h4 class="prating"> Profile Rating</h4>
					<div  id="kRate" style="height:10px;width: 40px;padding-left: 100px;"></div>
					
				</div>
				<div class="shortdetails">
				
					<ul>
						<li> Projects Posted &nbsp; &nbsp; &nbsp; 10</li>
						<hr>
						<li> User Reviews &nbsp; &nbsp; &nbsp; 2 </li>
						<hr>
						<li> Teams &nbsp; &nbsp; &nbsp; 2</li>
						<hr>
						<li> Earnings &nbsp; &nbsp; &nbsp; $ 100</li>
						
					</ul>
					
				</div>
				<div class="shortdetails2">
					<div class="pprogress">Profile Progress:
					<progress value="22" max="100">
					</progress>
					</div>
				</div>
				
				
			</div>
	</div>
	

</body>

</html>


