<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Available Ideas</title>
		
		<link href="pm/css/availableideas.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
     
		<script src="pm/js/jquery-1.11.2.min.js"></script>
  		<script src="pm/js/jquery-ui.js"></script>
 		<script src="js/jquery.min.js"></script>
		<script src="js/jRate.min.js"></script>
		<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});

  </script>
</head>
<body>
		<!-- new code for the session begins here -->




<div class="navSearch">
		
		
		
		
					<div class="submenulinks">
								<p>
							   	 			<s:a action="publicsearching">Find Users
											<s:param name="userid" value="#session.userid" />
											</s:a>
								 			 &nbsp;&nbsp; |&nbsp;&nbsp; 
								<strong>			<s:a action="ideasAvailable">Find Ideas
											<s:param name="bidsUser" value="#session.userid" />
											</s:a>
								</strong>   
								</p>
					</div>	
				
					
		</div>

		<div class="maincontainer">
		
				<div class="leftcontainer">
							
							<div class="sideheadings">
								 <h3> Idea Domain </h3>
								 <ul>
								 	<li><a href="#">Desktop Development</a></li>
								 	<li><a href="#">Android Development</a></li>
								 	<li><a href="#">Web Development</a></li>
								 	<li><a href="#">IOS Development</a></li>
								 </ul>
							</div>
				</div>
				<div class="rightcontainer">
				
							<div class="ideadescription">
								<h2>Idea Summary</h2>
								<h3 class="heading"> &nbsp;&nbsp; <s:property value="idea.ideaTitle" /></h3>
								<p>Description:<s:property value="idea.ideaDescrption" /></p>
								<p>Domain:<s:property value="idea.ideaDomain" /></p>
								<p>Posted On:<s:property value="idea.ideaPostedOn" /></p>
								<p>Budget:<s:property value="idea.ideaBudget" /></p>
								<p>Completion Date:	<s:property value="idea.ideaDuration" /></p>
								<hr>
							</div>
								
							<div class="biddingbox">	
								<s:form action="bidsIdea">
									<s:actionerror />
									<input type="hidden" value="<%=session.getAttribute("userid")%>" name="bidsUser" />
									<label id="bidlabel">Enter Bid Amount &nbsp;</label>
									<input  type="number" id="bidlabel" name="bidsAmount" min="1" required="required"/>
									<%-- <s:textfield id="bidlabel" name="bidsAmount" min="5" required="required" ></s:textfield> --%>
									<br>
									<s:hidden type="hidden" value="%{ideaId}" name="bidsIdea" />
									<input type="hidden" value="1" name="bidsFlag" />
									
									<s:submit cssClass="btn btn-success" value="bids"></s:submit>
								</s:form>	
							</div>
						<div class="rightsubcontainer">
						
										
										<div class="userdetail">
											<h3> Idea Posted By</h3>
											<div class="igbox">
											</div>					
											<p>
												<strong>
													<s:url id="viewPmpublic" action="viewPmpublic">
			             							<s:param name="userid" value="%{user.userid}"></s:param>
			             							<s:param name="username" value="%{user.username}"></s:param>
			            			 				</s:url>			
													<s:a href="%{viewPmpublic}"><s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/></s:a>
												</strong>	
											</p>
											<%-- <s:property value="%{user.username}"/> --%>
											<p><s:property value="%{user.country}"/></p>
											
											
									
											<div  id="kRate" style="height:10px;width: 40px;"></div>	
										</div>
						</div>			
						
				</div>
		</div>


















		<!-- new code for the session ends here -->
		
			  <!-- <h3>The Available Idea</h3> -->
		
					<!-- <div class="module-body"> -->
						<!-- <div class="tab_container"> -->
							<!-- <div id="tab1" class="tab_content"> -->
				<%-- 			<table class="tablesorter"> 
								<thead> 
								<tr> 
   					 
			    					<th align="left" >Idea Title </th> 
    								<th>Description</th> 
    								<th>Domain</th> 
    								<th>Posted on</th> 
    								<th>Budget</th>
    								<th>Duration</th>
								</tr> 
								</thead> 
							<tr>
								<td><s:property value="idea.ideaTitle" /></td>
								<td><s:property value="idea.ideaDescrption" /></td>				
								<td><s:property value="idea.ideaDomain" /></td>
								<td><s:property value="idea.ideaPostedOn" /></td>
								<td><s:property value="idea.ideaBudget" /></td>
								<td><s:property value="idea.ideaDuration" /></td>
				
							</tr>
							</table> --%>
			<!-- 				</div> -->
			
	<%-- 						<h3>Idea Poster</h3>
			
	
							
							<br>
							<br>
							<div id="tab1" class="tab_content">
							<table class="tablesorter">
							<thead>
								<tr>
									<th align="left"> username</th>
									<th align="left">firstname</th>
									<th>lastname</th>
									<th>country</th>
									<th>Rating</th>
								</tr>
							</thead>
								<tr>
									<td>
										<s:property value="%{user.username}"/>
									</td>
									<td>
										<s:property value="%{user.firstname}"/>		
									</td>			
									<td>		
										<s:property value="%{user.lastname}"/>
									</td>		
									<td>		
										<s:property value="%{user.country}"/>	
									</td>		
                   				</tr>	 
                   			</table> --%>
								
							<!-- </div>
					</div>
				</div> -->

</body>
</html>