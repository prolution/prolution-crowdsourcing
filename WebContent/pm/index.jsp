<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="pm/css/style.css" type="text/css" rel="stylesheet">
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">
<link href="http://taitems.github.com/UX-Lab/core/css/prettify.css"
	rel="stylesheet" type="text/css">
<style type="text/css">
body {
	font-family: Helvetica, Arial, sans-serif;
	font-size: 13px;
	padding: 0 0 50px 0;
}

.contain {
	width: 800px;
	margin: 0 auto;
}

h1 {
	margin: 40px 0 20px 0;
}

h2 {
	font-size: 1.5em;
	padding-bottom: 3px;
	border-bottom: 1px solid #DDD;
	margin-top: 50px;
	margin-bottom: 25px;
}

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}
</style>
</head>
<body>

	<div class="contain">



		<div class="gantt"></div>




	</div>

	<script src="pm/js/jquery.min.js"></script>
	<script src="pm/js/jquery.fn.gantt.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="http://taitems.github.com/UX-Lab/core/js/prettify.js"></script>
		<script src="pm/js/jquery-te-1.4.0.min.js"></script>

<script type="text/javascript">
var src=[];
</script>
<s:iterator value="project">
	<script>
	$(function() {


		var sdate=new Date("${proPostedOn}");
		var smilli=sdate.getTime();

		var ldate=new Date("${proDuration}");
		var lmilli=ldate.getTime();
		
		src.push({
			name: "${proTitle}",
			desc: "${proDescrption}",
			values: [{
				from: "/Date("+smilli+")/",
				to: "/Date("+lmilli+")/",
				label: "${proDomain}",
				customClass: "ganttRed"
			}]
				
		});
	});
		
		</script>
		</s:iterator>
		<script>
	$(function() {
		/* var ppp="${project}";
		var src=[];
			for(var key in ppp) {
				src.push({
			name: key.proTitle,
			desc: "${proDescrption}",
			values: [{
				from: "/Date(1320192000000)/",
				to: "/Date(1322401600000)/",
				label: "${proDomain}",
				customClass: "ganttRed"
			}]
				
		});
} */
			"use strict";
			$(".gantt").gantt({
				source:src,

			
				
				
				navigate: "scroll",
				maxScale: "hours",
				itemsPerPage: 10/* ,
				onItemClick: function(data) {
					alert("Item clicked - show some details");
				},
				onAddClick: function(dt, rowId) {
					alert("Empty space clicked - add an item!");
				},
				onRender: function() {
					if (window.console && typeof console.log === "function") {
						console.log("chart rendered");
					}
				} */
			});

			/* $(".gantt").popover({
				selector: ".bar",
				title: "I'm a popover",
				content: "And I'm the content of said popover.",
				trigger: "hover"
			}); */

			prettyPrint();

		});
  </script>
</body>
</html>