<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta charset="utf-8">
    	<title> Pro-Lutions :: Upload Project </title>
    
    	<!-- css files for this jsp file -->
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
		<link href="pm/css/uploadingproject.css" rel="stylesheet" type="text/css">

        	<link rel="stylesheet" href="pm/css/jquery-ui.css"> 
          	<link rel="stylesheet" href="pm/css/style.css">  
     
	
			<script src="pm/js/jquery-1.11.2.min.js"></script>
  			<script src="pm/js/jquery-ui.js"></script>
	
	<script>

	$(function() {
		window.history.forward();function noBack(){window.history.forward();}
			/* Original method*/
		 $("#btnSubmit").click(function() {
				$("#frmToSubmit").submit();
				
				
				
				/*alert("testing");*/
				
			});


		 		 
		$("#dinid")
				.on(
						"change",
						function() {
							var date = new Date($("#proPostedOn").val()), days = parseInt(
									$("#dinid").val(), 10);
							if (!isNaN(date.getTime())) {

								date.setDate(date.getDate() + days);
								$("#proDuration").val(date.toInputFormat());
							} else {
								alert("Invalid Date");
							}
						});
		Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
			var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = this.getDate().toString();
			return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
					+ (dd[1] ? dd : "0" + dd[0]); // padding
		};

		//$( "#format" ).change(function() {
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});
		//});
		// $( "#datepicker" ).datepicker();

	});
	$(function() {
		$("#proDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
	
	
	
	
</script>		
			<%-- <script>
			window.history.forward();function noBack(){window.history.forward();
  					$(function() {

						$("#dinid")		
						.on(
						"change",
						function() {
							var date = new Date($("#proPostedOn").val()), days = parseInt(
							$("#dinid").val(), 10);
							if (!isNaN(date.getTime())) {

						date.setDate(date.getDate() + days);
						$("#proDuration").val(date.toInputFormat());
					} else {
						alert("Invalid Date");
					}
				});

	  Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
			var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = this.getDate().toString();
			return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
					+ (dd[1] ? dd : "0" + dd[0]); // padding
		};

      $( "#datepicker" ).datepicker( {dateFormat: 'yy-mm-dd'} );
	  

	  $("#btnSubmit").click(function() {
			$("#frmToSubmit").submit();
			//alert("testing");
		});
	  
	    $( "#proDomain" ).selectmenu();
	 
	    $( "#files" ).selectmenu();
	 
	    $( "#number" )
	      .selectmenu()
	      .selectmenu( "menuWidget" )
	        .addClass( "overflow" );
	  });
	  
  </script> --%>
<style>
    fieldset {
      border: 0;
       width: 200px;
    }
    label {
      display: block;
      margin: 30px 0 0 0;
    }
    select {
      width: 200px;
    }
    .overflow {
      height: 200px;
    }
  </style>
 
 <script type='text/javascript'>
CharacterCount = function(TextArea,FieldToCount){
	var proDescrption = document.getElementById(TextArea);
	var myLabel = document.getElementById(FieldToCount); 
	if(!proDescrption || !proDescrption){return false}; // catches errors
	var MaxChars =  proDescrption.maxLengh;
	if(!MaxChars){MaxChars =  proDescrption.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};
	var remainingChars =   MaxChars - proDescrption.value.length
	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars
}

//SETUP!!
setInterval(function(){CharacterCount('proDescrption','CharCountLabel1')},55);
</script>
  
<%-- <script type='text/javascript'>
CharacterCount = function(TextArea,FieldToCount){
	var proDescrption = document.getElementById(TextArea);
	var myLabel = document.getElementById(FieldToCount); 
	if(!proDescrption || !proDescrption){return false}; // catches errors
	var MaxChars =  proDescrption.maxLengh;
	if(!MaxChars){MaxChars =  proDescrption.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};
	var remainingChars =   MaxChars - proDescrption.value.length
	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars
}

//SETUP!!
setInterval(function(){CharacterCount('proDescrption','CharCountLabel1')},55);
</script> --%>

</head>

<body>
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>


<div class="forminput">

				<div class="formfields">

					<s:form id="frmToSubmit" action="projectposted" method="post">
						<s:actionerror />
   								<h1> Post Your Project </h1>
   								<p class="subheading">Describe the Project Details</p>
						    <p class="name"> 
        						<!-- <input type="text" name="name" id="name" /> --> 
        						<label for="proTitle">Project Title</label> 
        						<input type="hidden" value="<%=session.getAttribute("userid")%>" name="proOwner" /> 
								<input type="text"  name="proTitle" placeholder="Enter Project Title" required="required" maxlength="25">
								<p class="sbscript">Max 25 words</p>
								<input type="hidden" name="proPostedOn" id="proPostedOn" value="${currentDate}" />
    						</p> 
    						<p class="text"> 
        						<!-- <textarea name="text"></textarea> --> 
					
        						<label for="proDescrption"> Describe your Project </label>
								<textarea name="proDescrption"  id="proDescrption" rows="10" required="required" cols="50" maxlength="250" minlength="80"></textarea>
								<div id='CharCountLabel1'></div>
        
    						</p>
   							<p class="procategory">
           						<label for="proDomain">Choose the Project Category</label>
           						<!-- <fieldset> -->
								<select name="proDomain" id="proDomain" required="required">
									<option selected="selected">Select Category</option>
									<option>web</option>
									<option>Android</option>
									<option>IOS</option>
									<option>Desktop</option>
									<option>other</option>
								</select>
								<!-- </fieldset> -->
   
						   </p>
    							<p class="email">
    							<label for="proBudget">Total Budget of the Project</label> 
								<input type="number" name="proBudget" placeholder="$" required="required" min="100" max="1000"> 
        						<!-- <label for="email">E-mail</label> 
        						<input type="text" name="email" id="email" /> --> 
        
    						</p> 
   
						    <p class="web"> 
        	
        						<label for="proDays"> Total Duration of Project in [DAYS] </label>
								<input id="dinid" type="number" name="proDays" placeholder="days" required="required" min="1" max="100"/> 
    						</p> 
   
    						 
    						<p>
    							<label for="proDuration"> Project Deadline </label> 
								<input id="proDuration" type="text" name="proDuration" readonly />
   							</p>
   							
    						<p class="submitnew"> 
        							<s:submit value="Continue"  cssClass="btn btn-success" ></s:submit>
    						</p> 
    						
   
   							
   
				</s:form>

			</div>
			
			<div class="rightinfor">
						<h4>Need Help With Your Post ? </h4>
						<p>We can help you describe what you need. </p>
						<p><a href="#">Contact Us </a> ?</p>
						
			</div>
			<div class="rightinformto">
						<div class="infortext">		
						<p>*Note</p>
						<p>This Project Will Only Be Visible to You,
						Proceed Further to add tasks and
						Hire Contributors</p>
						</div>
			</div>
</div>
		

<%-- 
			  	 <h3>Upload New Project</h3>

                            
                <s:form id="frmToSubmit" action="projectposted">

						<label> Project Title </label>
						<s:hidden name="proOwner" value="%{sessionScope.userid}"></s:hidden>

						<input type="hidden" value="<%=session.getAttribute("userid")%>" name="proOwner" /> 
						<input type="text" class="form-control" name="proTitle" placeholder="Enter Project Title">
						 
						<input type="hidden" name="proPostedOn" id="proPostedOn" value="${currentDate}" /> 
						<label> Project Category </label>
						<fieldset>
							<select name="proDomain" id="proDomain">
								<option>web</option>
								<option>Android</option>
								<option selected="selected">IOS</option>
								<option>Desktop</option>
								<option>other</option>
							</select>
						</fieldset>
						<label> Project Budget </label> 
						<input type="text" name="proBudget" placeholder="Enter Project Budget"> 
						
						<label> Project Duration in days </label>
						<input id="dinid" type="text" name="proDays" placeholder="Enter Project Duration" />
						<label> Project Submission </label> 
						<input id="proDuration" type="text" name="proDuration" readonly />
						<!--<input type="text" id="datepicker" class="form-control" placeholder="Enter Project Duration" -->
<!-- 							name="proDuration" size="30">  -->
						<label> Project	Description </label>
						<textarea name="proDescrption" class="form-control" rows="3"></textarea>
				
 						<input type="button" id="btnSubmit" value="Submit Project" />
					</s:form>
    
 --%>		
	
</body>

</html>