<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Available Ideas</title>

		<link href="pm/css/availableideas.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
		
          <link rel="stylesheet" href="pm/css/jquery-ui.css">
           <link rel="stylesheet" href="css/demo_page.css">
          <link rel="stylesheet" href="css/demo_table_jui.css">
          <link rel="stylesheet" href="css/dataTables.css">
     
		<script src="pm/js/jquery-1.11.2.min.js"></script>
 		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dataTables.js"></script>
  		<script src="pm/js/jquery-ui.js"></script>

	<script type="text/javascript">
		
	$(document).ready(function(){
		
	 $("#jqueryDataTable").dataTable({
         "columnDefs": [
{ className: "hidden", "targets": [ 0 ] }
                        
                    ],
         "sPaginationType" : "full_numbers",
             "bProcessing" : false,
             "bServerSide" : false,
             "sAjaxSource" : "dataTablesAction",
             "bJQueryUI" : true,
             "aoColumns" : [

                            { "mData": "ideaId" },
                            
         { "mData": "ideaTitle" },

         { "mData": "ideaDescrption" },
         { "mData": "ideaDomain" },
         { "mData": "ideaPostedOn" },
         { "mData": "ideaBudget" },
         { "mData": "ideaDuration" }         
     ]
 } );
	
	$('#jqueryDataTable tbody').on( 'click', 'tr', function () {
        //$(this).toggleClass('selected');
              // var firstcell = $(this).children('td:first');
                          $(this).toggleClass('selected');
              
               var name=$(this).find("td.hidden");
               $("#ideaId").val(parseInt(name.text()));
               $("#frmIdeaPoster").submit();
               //var name = $('td', this).eq().prev().text();
	 });
	});

  </script>
</head>
<body>
		<div class="navSearch">
		
		
		
		
					<div class="submenulinks">
								<p>
							   	 			<s:a action="publicsearching">Find Users
											<s:param name="userid" value="#session.userid" />
											</s:a>
								 			 &nbsp;&nbsp; |&nbsp;&nbsp; 
								<strong>			<s:a action="ideasAvailable">Find Ideas
											<s:param name="bidsUser" value="#session.userid" />
											</s:a>
								</strong>   
								</p>
					</div>	
				
					
				 <!-- <form class="navbar-form" > -->
				<%--  <form>
	    			<div class="input-group stylish-input-group">
        				<span class="input-group-addon">
            				<button type="submit"><span class="glyphicon glyphicon-search"></span></button>  
        				</span>
        					<input type="text" class="form-control input-sm searchbox" placeholder="Search">
    				</div>
     			</form> --%>
    			<!-- /.search -->
				
				<!-- <div class="submenubutton">
				<input class="btnpostproject" type="button" value="Post a Project "/>
				</div> -->
		</div>

		<div class="maincontainer">
		
				<div class="leftcontainer">
							
							<div class="sideheadings">
								 <h3> Idea Domain </h3>
								 <ul>
								 	<li><a href="#">Desktop Development</a></li>
								 	<li><a href="#">Android Development</a></li>
								 	<li><a href="#">Web Development</a></li>
								 	<li><a href="#">IOS Development</a></li>
								 </ul>
							</div>
				</div>
				<div class="rightcontainer">
				
				
							<h3 class="heading"> &nbsp;&nbsp; ' Search New Ideas '</h3>
		
		
	
							<div id="container">		
								<div id="demo_jui">
        							<table id="jqueryDataTable" class="display jqueryDataTable">
        							<thead>
        							<tr>
    									<th  align="left">ideaId</th> 
                						<th>Idea Title </th> 

    									<th>Description</th> 
    									<th>Domain</th> 
    									<th>Posted on</th> 
    									<th>Budget</th>
    									<th>Duration</th>
        							</tr>
        							</thead>
        							<tbody>
        							</tbody>
        							</table>
								</div>
							</div>
			
			
									<s:form id="frmIdeaPoster" action="ideaPoster">
									<s:hidden type="hidden" id="ideaId" name="ideaId" />					
									</s:form>
	
				
				</div>
		</div>


		
	
	
	
</body>
</html>