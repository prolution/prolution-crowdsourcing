<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
<title>upload tasks</title>

<!-- <link href="pm/css/style.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" /> -->
<!-- <link rel="stylesheet" href="pm/css/jquery-ui.css"> -->
		<link rel="stylesheet" href="pm/css/bidsbyme.css">
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">

	<script src="pm/js/jquery-1.11.2.min.js"></script>
	<script src="pm/js/jquery-ui.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
<script>
$(function () {
	var that = this;
	//alert("${rating}");
	$("#kRate").jRate({
		rating: "${rating}",
		strokeColor: 'black',
		width: 20,
		height: 20,						
		readOnly:"true" 
		
	});
	
});
		$(function() {
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});

		$("#taskDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
	$('body').on('click', '#create-task', function() {
		dialog.dialog("open");
	});
</script>
<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}
</style>
</head>
<body>
	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
	
	
	<!-- new code Starts here  -->
	
	
<div class="mybidform">
		
			<div class="headertext">
			<h2> Completed Idea </h2>
			</div>
			
			<p><strong><span>Entrepreneur</span></strong> :<s:property value="%{user.firstname}"/>&nbsp;<s:property value="%{user.lastname}"/></p>		
			<p><strong><span>Country</span></strong><s:property value="%{user.country}"/></p>	
			<strong><span>User Rating:</span></strong><div  id="kRate" style="height:10px;width: 40px;"></div>
			<br>							
			<div class="mybidleftpan">
					<div class="tabbar">
							<%-- <div class="sortdesc">
								<label> Sort By </label> &nbsp; 
								<span>
									<select>
										<option>By name</option>
										<option>By Amount</option>
									</select>
								</span>
							</div> --%>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
											<th>Title</th>
											<!-- <th>Description</th> -->
											<th>Domain</th>
											<th>Posted on</th>
											<th>Budget</th>
											<th>Duration</th>
                                    </tr>
                                 </thead>
                                			<s:iterator value="tasks">
									<tr>
											<td><s:property value="taskTitle" /></td>
											<%-- <td><s:property value="taskDescrption" /></td> --%>
											<td><s:property value="taskDomain" /></td>
											<td><s:property value="taskPostedOn" /></td>
											<td><s:property value="taskBudget" /></td>
											<td><s:property value="taskDuration" /></td>
											<td>
												<s:form action="projectTask">
												<s:hidden name="taskID" value="%{taskID}"></s:hidden>									
												<s:submit cssClass="btn btn-success" value="Open"></s:submit>
												</s:form>
											</td>
									</tr>
											</s:iterator>
                                </tbody>
                            </table>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Idea Title</h4>
		 				 <h4><s:property value="%{idea.ideaTitle}"/></h4>  
		 				<ul>
		 					<li>Description:<p style="color: green;"><s:property value="%{idea.ideaDescrption}"/></p></li>
		 					<li>Duration:<p style="color: green;"><s:property value="%{idea.ideaDuration}"/></p></li>
		 					<li>Budget: <p style="color: green;"><s:property value="%{idea.ideaBudget}"/></p></li>
		 					
		 					<%-- <s:textfield label="Project Name" value="%{idea.ideaTitle}" name="ideaTitle"></s:textfield>
							<s:textfield label="Project Cost" name="ideaBudget" value="%{idea.ideaBudget}"></s:textfield>
							<s:textfield label="Project" name="ideaDuration" value="%{idea.ideaDuration}"></s:textfield>
							<s:textfield label="Project" name="ideaDescrption" value="%{idea.ideaDescrption}"></s:textfield>
		 					 --%>
		 					
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the Idea </p>
		 			
		 			</div>
			
			</div>
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>











<!-- new code finished here  -->


</body>
</html>