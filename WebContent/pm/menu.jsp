<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dash board(Project Manager)</title>
	<link href="bootstrap/css/simple-sidebar.css" rel="stylesheet">
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css"/> 
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- <link rel="stylesheet" href="en/css/layout.css" type="text/css"/> --> 
	
    
    
<!--     <!-- new css files 	
	<link rel="stylesheet" href="en/css/layout.css" type="text/css"/>
	<link href="en/css/sb-admin.css" rel="stylesheet" type="text/css" >
   
    <link href="en/css/simple-sidebar.css" rel="stylesheet">
    
    <link href="en/css/bootstrap.min.css" rel="stylesheet">
 -->
     
	
	
</head>
<body >
	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
	
	

<!-- new code for the menu  -->

<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
        <form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
        
            <ul class="sidebar-nav">
            
            
            <li>
					<s:a action="myPmProfile">My Profile
					<s:param name="userid" value="#session.userid" />
					</s:a>
			 </li>    
			 
			 <li class="icn_new_article">
			 	<%-- <s:form action="ideasAvailable">
					<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="bidsUser" />
				<s:submit value="Projects Available"></s:submit>
				</s:form> --%>
				<s:a action="ideasAvailable">Projects Available
					<s:param name="bidsUser" value="#session.userid" />
				</s:a>
			</li>
        	
        
			<li> 
					<s:a action="findusersPm">Find Users
					<s:param name="userid" value="#session.userid" />
					</s:a>
				
			</li>
			<li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"> Projects <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                        	
                            <li>
								<s:a action="projectpost">Upload Projects
								<s:param name="proOwner" value="#session.userid" />
								</s:a> 
							</li>
							
							<li>
								<s:a action="myProjects">Current Projects
								<s:param name="proOwner" value="#session.userid" />
								</s:a> 
							</li>
                        </ul>
            </li> 
            <!-- for a time being purpose  -->
            <%-- <li>
								<s:a action="projectpost">Upload Projects
								<s:param name="proOwner" value="#session.userid" />
								</s:a> 
			</li>
			<li>
								<s:a action="myProjects">Current Projects
								<s:param name="proOwner" value="#session.userid" />
								</s:a> 
			</li> --%>
			
			<!-- for a time being purpose  -->
            <li>
            		<s:a action="myBids">My Bids
					<s:param name="bidsUser" value="#session.userid" />
					</s:a> 
			</li>
            
			<li>
		   			<s:url id="lockscreen" action="lockscreen">
		   			<!--<s:property value="#session.userid"/>-->
		   			<s:param name="userid" value="#session.userid"></s:param>
					</s:url> <s:a href="%{lockscreen}">Lock Screen</s:a>
			</li>
			<li>
					<s:form action="logout">
                         	<a href="logout"> Log Out</a>
					 </s:form>
			</li>
            </ul>
        </div>
</div>




</body>
</html>