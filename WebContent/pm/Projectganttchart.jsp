<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>upload tasks</title>
	
	
		 <link href="pm/css/projectTasks.css" rel="stylesheet" type="text/css"> 
	    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">

	
	<!-- this one is for the ganttchart -->
	<link href="pm/css/style.css" type="text/css" rel="stylesheet">


<!-- <link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" /> -->


		<!-- important for popup windows -->
		<link rel="stylesheet" href="pm/css/jquery-ui.css"> 
		<script src="pm/js/jquery.min.js"></script>
		<script src="pm/js/jquery-1.11.2.min.js"></script>
		<script src="pm/js/jquery-ui.js"></script>

<script>
	//function pfunc() {
	//$(this).parent('s:form').trigger('submit');
	//$(this).closest('#pform').trigger('submit');
	//this.form.submit();
	//}

	$(function() {
				var dialog, form, taskTitle = $("#taskTitle"),taskBudget = $("#taskBudget"),  taskPostedOn = $("#taskPostedOn"), taskDomain = $("#taskDomain"), dinid = $("#dinid"), precedor = $("#precedor"), taskDuration = $("#taskDuration"), taskDescrption = $("#taskDescrption"), taskProId = $("#taskProId"), allFields = $(
						[]).add(taskTitle).add(taskPostedOn).add(taskDomain)
						.add(dinid).add(precedor).add(taskDuration).add(
								taskDescrption).add(taskProId).add(taskBudget),
				tips = $( ".validateTips" );

				function updateTips( t ) {
				      tips
				        .text( t )
				        .addClass( "ui-state-highlight" );
				      setTimeout(function() {
				        tips.removeClass( "ui-state-highlight", 1500 );
				      }, 500 );
				    }

				 function checkLength( o, n, min, max ) {
				      if ( o.val().length > max || o.val().length < min ) {
				        o.addClass( "ui-state-error" );
				        updateTips( "Length of " + n + " must be between " +
				          min + " and " + max + "." );
				        return false;
				      } else {
				        return true;
				      }
				    }

				function checkRegexp( o, regexp, n ) {
				      if ( !( regexp.test( o.val() ) ) ) {
				        o.addClass( "ui-state-error" );
				        updateTips( n );
				        return false;
				      } else {
				        return true;
				      }
				    }
								

				function addTask() {
					var valid = true;
				      allFields.removeClass( "ui-state-error" );
				 
				      valid = valid && checkLength( taskTitle, "taskTitle", 3, 80 );
				      valid = valid && checkLength( taskDomain, "taskDomain", 3, 80 );
				      valid = valid && checkLength( dinid, "dinid", 1, 8 );
				      valid = valid && checkLength( taskBudget, "taskBudget", 1, 8 );

				      valid = valid && checkLength( taskDescrption, "ideaDescrption", 3, 100 );
				 
				      valid = valid && checkRegexp( taskTitle, /^[a-z]([0-9a-z_\s])+$/i, "ideaTitle may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
				      valid = valid && checkRegexp( taskDomain, /^[a-z]([0-9a-z_\s])+$/i, "taskDomain may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
				      valid = valid && checkRegexp( taskDescrption, /^[a-z]([0-9a-z_\s])+$/i, "taskDescrption may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
				      valid = valid && checkRegexp( taskBudget, /^([0-9a-zA-Z])+$/, "taskBudget field only allow : 0-9" );

				      valid = valid && checkRegexp( dinid, /^([0-9a-zA-Z])+$/, "dinid field only allow : 0-9" );

				      if ( valid ) {
						form[0].submit();
					//	allFields.removeClass("ui-state-error");
					dialog.dialog("close");
				      }
				      return valid;		
				      }

				dialog = $("#dialog-form").dialog({
					autoOpen : false,
					height : 600,
					width : 550,
					modal : true,
					buttons : {
						"Create an Task" : addTask,
						Cancel : function() {
							dialog.dialog("close");
						}
					},
					close : function() {
						form[0].reset();
						allFields.removeClass("ui-state-error");
					}
				});

				form = dialog.find("#frmToSubmit").on("submit",
						function(event) {
							event.preventDefault();
							addTask();
						});

				$("#create-task").click(function() {
					dialog.dialog("open");

				});


				$("#dinid")
						.on(
								"change",
								function() {
									var date = new Date($("#taskPostedOn")
											.val()), days = parseInt(
											$("#dinid").val(), 10);
									if (!isNaN(date.getTime())) {

										date.setDate(date.getDate() + days);
										$("#taskDuration").val(
												date.toInputFormat());
									} else {
										alert("Invalid Date");
									}
								});

				Date.prototype.toInputFormat = function() {
					var yyyy = this.getFullYear().toString();
					var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
					var dd = this.getDate().toString();
					return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
							+ (dd[1] ? dd : "0" + dd[0]); // padding
				};

				$("#datepicker").datepicker({
					dateFormat : 'yy-mm-dd'
				});

				$("#taskDomain").selectmenu();

				$("#files").selectmenu();

				$("#number").selectmenu().selectmenu("menuWidget").addClass(
						"overflow");

			})(jQuery, this, document);
</script>
<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}

.ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>

</head>

<body>

	<div class="maincontainer">
			
			<div class="leftpan">
				<div class="projectdetails">
			 		<s:form action="updateProj">
						<s:iterator>
							<h3> Project </h3>
							<h4><strong><s:property value="%{proj.proTitle}"/></strong></h4>
							<ul>
								<li><s:property value="%{proj.proDescrption}"/> </li> 
								<li> Budget :<s:property value="%{proj.proBudget}"/></li>
								<li> Duration:<s:property value="%{proj.proDuration}"/></li>
								<li> Status: In Process</li>
								<li><a href="#">Edit</a></li>
							
							</ul>
							
						</s:iterator>
					</s:form> 					
					<hr>
					<div class="projectlinks">
						<ul>
							<li><a href="#">Project Details </a></li>
							
							<li><a href="#">Project Team </a></li>
							<li>
													<s:a action="projectTasksGantview">Project Gantt Chart 
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
							</li>
							<li>
													<s:a action="projectTasks">All Tasks
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
							</li>
							<li><a href="#">Assigned Tasks </a></li>
							<li><a href="#">Pending Tasks </a></li>
							<li><a href="#">Completed Tasks </a></li>
						</ul>
					</div>
					<hr>
					</div>
					
					
			</div>
			
			<!-- ------------------------------------------------------------------------------ -->
			<!-- start of centrepan -->
			<div class="centrepanfor">
			<!-- ------------------------------------------------------------------------------ -->
			<!-- start of mid pan -->
			<div class="midpan">
				<div class="contain">
					<div class="gantt"></div>

				</div>
			
			</div>
				<!-- end of mid pan -->
				
				
			<div class="rightpan" style="padding-top: 30px;">
			
				<div class="rightpaninside">
					<div class="projectstatsview">	
			
							<h4> Project Stats </h4>
							<ul>
								<li> Project Posted On</li>
								<li> No. of Tasks </li>
								<li> Team Members  </li>
								<li> Task Assigned  </li>
								<li> Task Completed  </li>
								<li> Task Pending  </li>
							</ul>
							<hr>
					</div>
					<div class="counterstats">
							<ul>
								<li>0</li>
								<li>0</li>
								<li>0</li>
								<li>0</li>
								<li>0</li>
								<li>0</li>
							</ul>
						
							
					</div>
					<div class="critcalStats">
						
								<h4>Critical Path</h4>
								<div class="criticalpath" id="crtical" align="left"> 
										
										<%--<s:label title="Critical Path" label="Critical Path" value="%{crit}"></s:label> --%>					
										<!-- <b style="font-style: italic; ">Critical Path:</b> &nbsp; --> 
										<s:property value="alist"  /> 

								<hr>		
								<p>* Note: "This Provides the overall stats of the project </p>
								</div>
									
					</div>
			
				</div>
			</div>
			
			<!-- ------------------------------------------------------------------------------ -->
			<!-- end of centrepan -->
			</div>
			<!-- ------------------------------------------------------------------------------ -->
	</div>











	<script src="pm/js/jquery.fn.gantt.js"></script>
	<script src="pm/js/jquery-te-1.4.0.min.js"></script>
	<script type="text/javascript">
		var src = [];
	</script>
	<s:iterator value="tasks">
		<script>
			$(function() {

				var sdate = new Date("${taskPostedOn}");
				var smilli = sdate.getTime();

				var ldate = new Date("${taskDuration}");
				var lmilli = ldate.getTime();

				src.push({
					name : "${taskTitle}",
					desc : "${taskDescrption}",
					values : [ {
						from : "/Date(" + smilli + ")/",
						to : "/Date(" + lmilli + ")/",
						label : "${taskDomain}",
						customClass : "ganttRed"
					} ]

				});
			});
		</script>
	</s:iterator>
	<script>
		$(function() {
			"use strict";
			$(".gantt").gantt({
				source : src,
				navigate : "scroll",
				maxScale : "hours",
				itemsPerPage : 10
			});
			prettyPrint();

		});
	</script>
</body>
</html>