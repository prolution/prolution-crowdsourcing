<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>



<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>Dashboard</title>
	
	 <link href="pm/css/dashboardpm.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet"> 
	
	
<!-- 	 <link rel="stylesheet" href="en/css/layout.css" type="text/css"/>
	 <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css"> 
    

    Custom CSS
    
	<link href="en/css/bootstrap.min.css" rel="stylesheet">
    Morris Charts CSS
    
<link href="en/css/morris.css" rel="stylesheet">
    Custom Fonts
    
        <link href="en/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 -->
	</head>


<body>

<div class="leftpan">

</div>
<div class="centerpan">
					
					<div class="Midpan">
						<%-- <div class="col-lg-12">
                        	<div class="alert alert-info alert-dismissable">
                            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            	<i class="fa fa-info-circle"></i>  <strong>You Have Some Pending Requests</strong> Click <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">Check Notfications</a>
                        	</div>
                    	</div> --%>
                    	<div class="textstyle">
                    	
                    		<h4 class="subheaderhome">  
                    			<strong> <s:a action="myPmIdeas">My Jobs 
										<s:param name="proOwner" value="#session.userid" />
										</s:a>  
								</strong>   | <s:a action="myProjects">My Projects
								<s:param name="proOwner" value="#session.userid" />
								</s:a>  | <s:a action="ideasAvailable">Opportunities
										  <s:param name="bidsUser" value="#session.userid" />
										  </s:a> 
							</h4>
                    	
								 
						
                    	</div>
                    	
                    	<div class="notifyticker">
                    		
                    		<i class="fa fa-info-circle"></i><strong> There are Thousands of Jobs waiting for you</strong>
                    		<ul>
                    			<li> Find Great Ideas </li>
                    			<li> Propose Bids on Ideas</li>
                    			<li> Hire your Team </li>
                    		</ul>
                    	
                    	</div>
                    	
                    	<div class="tabbar">
                    	
                    				
                    	
						<%-- 		<div class="sortdesc">
										<label> Sort By </label> &nbsp; 
									<span>
										<select>
											<option>By name</option>
											<option>By Job</option>
										</select>
									</span>
								</div> --%>
							<!-- <div class="verticalline">
							</div> -->
						</div>
						
						<div>
						
									<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											<th>Person</th>
											<!-- <th>Project Description</th> -->
											<th>Bid Task</th>
											<th>Bid Proposal</th>
											<th>Review</th>
											
    										
                                    </tr>
                                </thead>
                                <tbody>
                                    	<%-- <s:iterator value="project"> --%>
											<tr>
												<%-- <td>
													<s:a action="projectTasks"><s:property value="proTitle" />
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
												</td>
												<td><s:property value="proDescrption" /></td>
												<td><s:property value="proDomain" /></td>
												<td><s:property value="proPostedOn" /></td>
												<td><s:property value="proBudget" /></td>
												<td><s:property value="proDuration" /></td>
												<td>
													<a href="#">
														<i class="glyphicon glyphicon-remove-circle">
														</i>
													</a>
												</td> --%>
											</tr>
										<%-- </s:iterator> --%>
										  
                                    
                                    
                                </tbody>
                            </table>
                        </div>
									
						
						
						
						</div>
			

					</div>
					<div class="rightpan">
							<div class="messagesblock">
										<h3>Inbox</h3>
										<hr>
									<ul>
										
										<li>Action Required</li>
										<li>Messages</li>
										<li>Invites</li>
										<li>All</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							<div class="idea">
							<div class="messagesblock">
										<h3>Ideas</h3>
										<hr>
									<ul>
										
										<li>No.of Ideas</li>
										<li>Completed Ideas</li>
										<li>Pending Ideas</li>
										<li>Completed Tasks</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							</div>
							<div class="project">
							
							<div class="messagesblock">
										<h3> Projects </h3>
										<hr>
									<ul>
										<li>No.of Projects</li>
										<li>Completed Projects</li>
										<li>Pending Tasks</li>
										<li>Completed Tasks</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							</div>
					</div>
</div>
<!-- <div class="Midpan">

</div>
<div class="rightpan">

</div>
 -->

<%-- 
 <!-- <h1>Project Manager Dashboard </h1> -->	
			<div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>You Have Some Pending Requests</strong> Click <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">Check Notfications</a>
                        </div>
                    </div>
                </div>


                 <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">26</div>
                                        <div>New Comments!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">12</div>
                                        <div>New Tasks!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">124</div>
                                        <div>New Orders!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">13</div>
                                        <div>Support Tickets!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

			

                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Donut Chart</h3>
                            </div>
                            <div class="panel-body">
                                <div id="morris-donut-chart"></div>
                                <div class="text-right">
                                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Tasks Panel</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <span class="badge">just now</span>
                                        <i class="fa fa-fw fa-calendar"></i> Calendar updated
                                    </a>
                                    
                                </div>
                                <div class="text-right">
                                    <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Panel</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Order #</th>
                                                <th>Order Date</th>
                                                <th>Order Time</th>
                                                <th>Amount (USD)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>3326</td>
                                                <td>10/21/2013</td>
                                                <td>3:29 PM</td>
                                                <td>$321.33</td>
                                            </tr>
                                            <tr>
                                                <td>3325</td>
                                                <td>10/21/2013</td>
                                                <td>3:20 PM</td>
                                                <td>$234.34</td>
                                            </tr>
                                            <tr>
                                                <td>3324</td>
                                                <td>10/21/2013</td>
                                                <td>3:03 PM</td>
                                                <td>$724.17</td>
                                            </tr>
                                            <tr>
                                                <td>3323</td>
                                                <td>10/21/2013</td>
                                                <td>3:00 PM</td>
                                                <td>$23.71</td>
                                            </tr>
                                            <tr>
                                                <td>3322</td>
                                                <td>10/21/2013</td>
                                                <td>2:49 PM</td>
                                                <td>$8345.23</td>
                                            </tr>
                                            <tr>
                                                <td>3321</td>
                                                <td>10/21/2013</td>
                                                <td>2:23 PM</td>
                                                <td>$245.12</td>
                                            </tr>
                                            <tr>
                                                <td>3320</td>
                                                <td>10/21/2013</td>
                                                <td>2:15 PM</td>
                                                <td>$5663.54</td>
                                            </tr>
                                            <tr>
                                                <td>3319</td>
                                                <td>10/21/2013</td>
                                                <td>2:13 PM</td>
                                                <td>$943.45</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="en/js/raphael.min.js"></script>
    <script src="en/js/morris.min.js"></script>
    <script src="en/js/morris-data.js"></script>
 --%>
                    <div>
	
	
	
</div>
</body>

</html>








