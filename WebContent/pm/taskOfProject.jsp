<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
                    <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<link rel="stylesheet" href="pm/css/bidsbyme.css">
	
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet">
	
        	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});
	</script>
</head>
<body>



		<div class="mybidform">
		
			<div class="headertext">
			<h2> Bidder Information For the task '<s:property value="%{task.taskTitle}"/>'</h2>
			</div>
			
			<div class="mybidleftpan">
					<div class="tabbar">
							<div class="sortdesc">
								<!-- <label> Sort By </label> &nbsp; --> 
								<%-- <span>
									<select>
										<option>By name</option>
										<option>By Amount</option>
									</select>
								</span> --%>
							</div>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
										<!-- <th align="left" >Idea Title </th> 
    									<th>Description</th> 
    									<th>Domain</th> 
    									<th>Bid Date</th> 
    									<th>Idea Budget</th> -->
    									<!-- <th>Duration</th> -->
    									<!-- <th> username</th> -->
										<!-- <th> username</th> -->
										<th>Name</th>
										<!-- <th>lastname</th> -->
										<th>country</th>
										<th>Bids Amount</th>
										<th>Rating</th>
    									<!-- <th>Cancel</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
										<%-- <td>
											<s:property value="%{user.username}"/>
										</td> --%>
										<td>
											<s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/>		
										</td>			
										
										<td>		
											<s:property value="%{user.country}"/>	
										</td>	
										<td>		
											<s:property value="%{bidsAmount}"/>	
										</td>	
										<td>
											<div  id="kRate" style="height:10px;width: 40px;"></div>
										</td>
                   					</tr>	 
      
                                </tbody>
                            </table>
                            <div class="taskacceptbutton">
                            	<s:form action="assignTask" >
									<s:hidden name="taskOwner" value="%{user.userid}" ></s:hidden>
									<s:hidden name="taskID" value="%{task.taskID}" ></s:hidden>	    			
	  								<s:hidden name="taskFlag" value="2" ></s:hidden>    
      								<s:submit cssClass="btn btn-success" value="Accept bids"></s:submit>
                   				</s:form>
                   			</div>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Task Title</h4>
		 				 <h4><s:property value="%{task.taskTitle}"/></h4>  
		 				<ul>
		 					<li>Description<p><s:property value="%{task.taskDescrption}"/></p></li>
		 					<li>Duration<p><s:property value="%{task.taskDuration}"/></p></li>
		 					<li>Budget <p><s:property value="%{task.taskBudget}"/></p></li>
		 					
		 					
		 					
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the task </p>
		 			
		 			</div>
			
			</div>
			
			
			<%-- 
							<table class="tablesorter"> 
								<thead> 
									<tr> 
   					 
					    				<th align="left" >Idea Title </th> 
    									<th>Description</th> 
    									<th>Domain</th> 
    									<th>Posted on</th> 
    									<th>Budget</th>
    									<th>Duration</th>
									</tr> 
								</thead> 
										<s:iterator value="ideas">
										<tr>
											<td><s:property value="ideaTitle" /></td>
											<td><s:property value="ideaDescrption" /></td>				
											<td><s:property value="ideaDomain" /></td>
											<td><s:property value="ideaPostedOn" /></td>
											<td><s:property value="ideaBudget" /></td>
											<td><s:property value="ideaDuration" /></td>
										</tr>
										</s:iterator>
							</table> --%>
				
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>

</body>
</html>