<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>upload tasks</title>
	
	
		 <link href="pm/css/projectTasks.css" rel="stylesheet" type="text/css"> 
	    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">

	
			<!-- this one is for the ganttchart -->
			<link href="pm/css/style.css" type="text/css" rel="stylesheet">

	<!-- important for popup windows -->
		<link rel="stylesheet" href="pm/css/jquery-ui.css"> 
		<script src="pm/js/jquery.min.js"></script>
		<script src="pm/js/jquery-1.11.2.min.js"></script>
		<script src="pm/js/jquery-ui.js"></script>

<script>
	//function pfunc() {
	//$(this).parent('s:form').trigger('submit');
	//$(this).closest('#pform').trigger('submit');
	//this.form.submit();
	//}

	$(function() {
				var tmp,dialog, form, taskTitle = $("#taskTitle"),taskBudget = $("#taskBudget"),  taskPostedOn = $("#taskPostedOn"), taskDomain = $("#taskDomain"), dinid = $("#dinid"), precedor = $("#precedor"), taskDuration = $("#taskDuration"), taskDescrption = $("#taskDescrption"), taskProId = $("#taskProId"), allFields = $(
						[]).add(taskTitle).add(taskPostedOn).add(taskDomain)
						.add(dinid).add(precedor).add(taskDuration).add(
								taskDescrption).add(taskProId).add(taskBudget),
				tips = $( ".validateTips" );

				function updateTips( t ) {
				      tips
				        .text( t )
				        .addClass( "ui-state-highlight" );
				      setTimeout(function() {
				        tips.removeClass( "ui-state-highlight", 1500 );
				      }, 500 );
				    }

				 function checkLength( o, n, min, max ) {
				      if ( o.val().length > max || o.val().length < min ) {
				        o.addClass( "ui-state-error" );
				        updateTips( "Length of " + n + " must be between " +
				          min + " and " + max + "." );
				        return false;
				      } else {
				        return true;
				      }
				    }

				function checkRegexp( o, regexp, n ) {
				      if ( !( regexp.test( o.val() ) ) ) {
				        o.addClass( "ui-state-error" );
				        updateTips( n );
				        return false;
				      } else {
				        return true;
				      }
				    }

				function checkCost( tb, tp, n ) {
				      if (  tb >= tp   ) {
					      //alert(tp+2);
				        taskBudget.addClass( "ui-state-error" );
				        updateTips( n );
				        return false;
				      } else {
				        return true;
				      }
				    }
								
				function checkDays( td, pd, n ) {
				      if (  td > pd   ) {
					      //alert(tp+2);
				        dinid.addClass( "ui-state-error" );
				        updateTips( n );
				        return false;
				      } else {
				        return true;
				      }
				    }
				
				function addTask() {
					var valid = true;
					 tmp=parseInt($("#remain").val());
						//alert(parseInt(taskBudget.val())+2);
				      allFields.removeClass( "ui-state-error" );
				 
				      valid = valid && checkLength( taskTitle, "Title", 3, 80 );
				      valid = valid && checkRegexp( taskTitle, /^[a-z]([0-9a-z_\s])+$/i, "Title may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );

				      valid = valid && checkLength( taskDomain, "Domain", 3, 80 );
				      valid = valid && checkRegexp( taskDomain, /^[a-z]([0-9a-z_\s])+$/i, "Domain may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );

				      valid = valid && checkLength( taskBudget, "Budget", 1, 8 );
				      valid = valid && checkRegexp(taskBudget, /^([0-9])+$/,"Please Enter the Numbers");      
				      valid = valid && checkCost( parseInt(taskBudget.val()),  tmp, "Budget should be less than Remaining Budget" );

				      
				      valid = valid && checkLength( dinid, "Days digits", 1, 2 );
				      valid = valid && checkRegexp( dinid, /^([0-9])+$/, "Days field only allow : 0-9" );
				      valid = valid && checkDays( parseInt(dinid.val()),parseInt($("#proDays").val()), "Task Duration must be less than Project Duration" );

				      valid = valid && checkLength( taskDescrption, "Descrption", 3, 100 );
				 
				      valid = valid && checkRegexp( taskDescrption, /^[a-z]([0-9a-z_\s])+$/i, "Descrption may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
				     
				      if ( valid ) {
						form[0].submit();
					//	allFields.removeClass("ui-state-error");
					dialog.dialog("close");
				      }
				      return valid;		
				      }

				dialog = $("#dialog-form").dialog({
					autoOpen : false,
					height : 700,
					width : 550,
					modal : true,
					buttons : {
						"Create an Task" : addTask,
						Cancel : function() {
							dialog.dialog("close");
						}
					},
					close : function() {
						form[0].reset();
						allFields.removeClass("ui-state-error");
					}
				});

				form = dialog.find("#frmToSubmit").on("submit",
						function(event) {
							event.preventDefault();
							addTask();
						});

				$("#create-task").click(function() {
					//alert(valueOf("${paisa}"));
				 tmp=parseInt($("#remain").val());
					
					//alert(taskBudget.val()+2);
					//alert(parseInt($("#remain").val())+2);
					dialog.dialog("open");

				});


				$("#dinid")
						.on(
								"change",
								function() {
									var date = new Date($("#taskPostedOn")
											.val()), days = parseInt(
											$("#dinid").val(), 10);
									if (!isNaN(date.getTime())) {

										date.setDate(date.getDate() + days);
										$("#taskDuration").val(
												date.toInputFormat());
									} else {
										alert("Invalid Date");
									}
								});

				Date.prototype.toInputFormat = function() {
					var yyyy = this.getFullYear().toString();
					var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
					var dd = this.getDate().toString();
					return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
							+ (dd[1] ? dd : "0" + dd[0]); // padding
				};

				$("#datepicker").datepicker({
					dateFormat : 'yy-mm-dd'
				});

				$("#taskDomain").selectmenu();

				$("#files").selectmenu();

				$("#number").selectmenu().selectmenu("menuWidget").addClass(
						"overflow");

			})(jQuery, this, document);
</script>
<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}

.ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>

</head>

<body>


	<div class="maincontainer">
			
			<div class="leftpan">
				<div class="projectdetails">
			 		<s:form action="updateProj">
						<s:iterator>
							<h3> Project </h3>
							<h4><strong><s:property value="%{proj.proTitle}"/></strong></h4>
							<ul>
								<li><s:property value="%{proj.proDescrption}"/> </li> 
								<li> Budget :<s:property value="%{proj.proBudget}"/></li>
								<li> Posted On:<s:property value="%{proj.proPostedOn}"/></li>
								<li> Duration:<s:property value="%{proj.proDuration}"/></li>
								<li> Domain:<s:property value="%{proj.proDomain}"/></li>
								<li> Days:<s:property value="%{proj.proDays}"/></li>

								<li> Status: In Process</li>
								<li><a href="#">Edit</a></li>
							<c:set var="pCost" value="${proj.proBudget}" ></c:set>
							</ul>
					<input id="proDays" type="hidden" value="${proj.proDays}" >					
							
						</s:iterator>
					</s:form> 
					<hr>
					<div class="projectlinks">
						<ul>
							<li><a href="#">Project Details </a></li>
							
							<li><a href="#">Project Team </a></li>
							<li>
													<s:a action="projectTasksGantview">Project Gantt Chart 
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
							</li>
							<li>
													<s:a action="projectTasks">All Tasks
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
							</li>
							<li><a href="#">Assigned Tasks </a></li>
							<li><a href="#">Pending Tasks </a></li>
							<li><a href="#">Completed Tasks </a></li>
							
						</ul>
					</div>
					<%-- <s:form action="updateProj">
					<s:textfield label="Project Name" value="%{proj.proTitle}" name="proTitle"></s:textfield>
					<s:textfield label="Project Cost" name="proBudget" value="%{proj.proBudget}"></s:textfield>
					<s:textfield label="Project" name="proDuration" value="%{proj.proDuration}"></s:textfield>
					<s:textfield label="Project" name="proDescrption" value="%{proj.proDescrption}"></s:textfield>


												<s:submit value="Update" ></s:submit>
						<input type="hidden" class="form-control"
							value="<%=session.getAttribute("userid")%>" name="proOwner" />
						<s:hidden name="proID" value="%{proj.proID}"></s:hidden>

					</s:form> --%>
					<hr>
					</div>
					
					
			</div>
			
			<!-- ------------------------------------------------------------------------------ -->
			<!-- start of centrepan -->
			<div class="centrepanfor">
			<!-- ------------------------------------------------------------------------------ -->
			
			
						<!-- start of mid pan -->
			<div class="midpan">
				<div class="toptimelinebar">
					<h3>Project Timeline</h3>
				</div>	
				<div class="contain">
				
						<div class="gantt"></div>

				</div>
				<div class="btngroup">
									<button class="btn btn-success" id="create-task">Create new task</button>
				</div>
			
			<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											<!-- <th>Project Name</th>
											<th>Project Description</th>
											<th>Domain</th>
											<th>Posted on</th>
											<th>Budget</th>
											<th>Duration</th>
    										<th>Cancel</th> -->
    										<th>Task</th>
											<th>Title</th>
											<!-- <th>Description</th> -->
											<th>Domain</th>
											<th>Posted on</th>
											<th>Budget</th>
											<th>Duration</th>
											<th>Set Precedence</th>
							
                                    </tr>
                                </thead>
                                <tbody>
                       	<c:set  var="paisa" value="0"  />
                                
                                <p class="alerttext"><s:if test="%{tasks.isEmpty()}">No Tasks!</s:if></p>
                                
										<s:else>
										<s:iterator value="tasks">
										
											<tr>
												<c:set var="counter" value="${counter + 1}" scope="page" />
												<td>${counter}</td>

												<td>
													<s:a action="projectTask"><s:property value="taskTitle" />
													<s:param name="taskID" value="%{taskID}" />
													</s:a>
												</td>
												<%-- <td><s:property value="taskDescrption" /></td> --%>
												<td><s:property value="taskDomain" /></td>
												<td><s:property value="taskPostedOn" /></td>
												<td><s:property value="taskBudget" /></td>
												<td><s:property value="taskDuration" /></td>
												<td>
														<s:form id="formp" action="sendPrecedor">
																<select name="precedor" id="dropdown1">
																	<c:forEach items="${tasks}" var="item">
																	<c:set var="count" value="${count + 1}" scope="page" />
																	<c:if test="${item.taskID != taskID}">
																	<option title="hello" value="${item.taskID}">${count}</option>
																	</c:if>
																	</c:forEach>
																	<c:set var="count" value="0" scope="page" />
																</select>
															<s:hidden name="taskID" value="%{taskID}"></s:hidden>
															<s:hidden name="taskProId" value="%{taskProId}"></s:hidden>
															<s:submit value="Set Precedor"></s:submit>
														</s:form>
												</td>
												
												<!-- <td>
													<a href="#">
														<i class="glyphicon glyphicon-remove-circle">
														</i>
													</a>
												</td> -->
											</tr>
												<c:set  var="paisa" value="${paisa + taskBudget}"  />
											
										</s:iterator>
										</s:else>
										<c:set var="remaining" value="${pCost - paisa} "></c:set>
									<p class="remainingbudget">Project Remaining Budget: &nbsp;  $<c:out value="${remaining} "></c:out> </p> 
                                </tbody>
                            </table>
									<input id="remain" value="${remaining}" type="hidden" >
                        </div>
                        
                        	<!-- <div class="btngroup">
									<button class="btn btn-success" id="create-task">Create new task</button>
							</div> -->
			
			
				</div>
				<!-- end of mid pan -->
			
			
			
			
			
			<div class="rightpan">
					<div class="rightpaninside">
						<div class="projectstatsview">	
								<h4> Project Stats </h4>
								<ul>
									<li> Project Posted On</li>
									<li> No. of Tasks </li>
									<li> Team Members  </li>
									<li> Task Assigned  </li>
									<li> Task Completed  </li>
									<li> Task Pending  </li>
								</ul>
								<hr>
						</div>
						<div class="counterstats">
								<ul>
									<li>0</li>
									<li>0</li>
									<li>0</li>
									<li>0</li>
									<li>0</li>
									<li>0</li>
								</ul>
						
							
						</div>
						<div class="critcalStats">
						
								<h4>Critical Path</h4>
								<div class="criticalpath" id="crtical" align="left"> 
										
										<%--<s:label title="Critical Path" label="Critical Path" value="%{crit}"></s:label> --%>					
										<!-- <b style="font-style: italic; ">Critical Path:</b> &nbsp; --> 
										<s:property value="alist"  /> 
								<hr>		
								<p>* Note: "This Provides the overall stats of the project </p>

								</div>
												
						</div>
					
					</div>
					
			</div>
			
			
			<!-- ------------------------------------------------------------------------------ -->
			</div>
			<!-- ------------------------------------------------------------------------------ -->
			<!-- end of centrepan -->
			

				
				
			
			
	
	</div>








	 
	
	
	
	<!--  Project Edit Code  -->
<%-- 	
			<h3>Project Edit</h3>
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Project Name" value="%{proj.proTitle}"
					name="proTitle"></s:textfield>
				<s:textfield label="Project Cost" name="proBudget"
					value="%{proj.proBudget}"></s:textfield>
				<s:textfield label="Project" name="proDuration"
					value="%{proj.proDuration}"></s:textfield>
				<s:textfield label="Project" name="proDescrption"
					value="%{proj.proDescrption}"></s:textfield>


										<s:submit value="Update" ></s:submit>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{proj.proID}"></s:hidden>

			</s:form>
		</div> 	
		
		 --%>
		
		
<!--  Project ends Edit Code  -->

			<%-- <s:form id="frmTask" action="deleteProject">
			<s:submit value="Delete" ></s:submit>
			<s:hidden name="proID" value="%{proj.proID}"></s:hidden>			
<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="proOwner" /> 
			</s:form> --%>

		
		
<%-- 		<div  title="Critical Path" id="crtical" align="left" style="background-color:white; border-color:red; border-style:double; width: auto;height:auto; overflow: hidden;   "> 
			<b style="font-style: italic; ">Critical Path:</b> &nbsp; <s:property value="alist"  />
		</div>
 --%>
	<!-- 	<div class="module-body"></div>
		
		</article>

		<div class="tab_container">
 -->				

			<%-- <div id="tab1" class="tab_content">
				<header>
				<h3>Task of the Project</h3>
				</header>

				<table class="tablesorter" cellspacing="0">
					<thead>
						<tr>
							<th align="left">Task</th>
							<th>Title</th>
							<th>Description</th>
							<th>Domain</th>
							<th>Posted on</th>
							<th>Budget</th>
							<th>Duration</th>
						</tr>
					</thead>
					<s:iterator value="tasks">
						<tr>
							<c:set var="counter" value="${counter + 1}" scope="page" />

							<td><label>${counter}</label></td>

							<td>
									<s:a action="projectTask"><s:property value="taskTitle" />
									<s:param name="taskID" value="%{taskID}" />
									</s:a>
							</td>
							<td><s:property value="taskDescrption" /></td>
							<td><s:property value="taskDomain" /></td>
							<td><s:property value="taskPostedOn" /></td>
							<td><s:property value="taskBudget" /></td>
							<td><s:property value="taskDuration" /></td>
							<td><s:form id="formp" action="sendPrecedor">

									<select name="precedor" id="dropdown1">
										<c:forEach items="${tasks}" var="item">
											<c:set var="count" value="${count + 1}" scope="page" />

											<option title="hello" value="${item.taskID}">${count}</option>
										</c:forEach>
										<c:set var="count" value="0" scope="page" />

									</select>
									<s:hidden name="taskID" value="%{taskID}"></s:hidden>
									<s:hidden name="taskProId" value="%{taskProId}"></s:hidden>
									<s:submit value="Set Precedor"></s:submit>
									<!-- 									<input type="button" onclick="pfunc()" id="btnPre" value="Set Precedor" /> -->
								</s:form></td>

							<td><s:form action="projectTask">
									<s:hidden name="taskID" value="%{taskID}"></s:hidden>
									<s:submit value="Open"></s:submit>
								</s:form></td>
						</tr>
					</s:iterator>

				</table>

			</div>
 --%>
		<!-- </div>

		
	</div>
 -->	
 		<div id="dialog-form">
		<header>
		<h3>Upload Task</h3>
		</header>
			<p class="validateTips">All form fields are required.</p>
				<s:form id="frmToSubmit" action="projectTaskPost">
				<label> Task Title </label>
				<input type="text" class="form-control" id="taskTitle" name="taskTitle" placeholder="Enter Task Title">
				<input type="hidden" class="form-control" id="taskPostedOn" name="taskPostedOn" value="${currentDate}" />

				<label> Task Category </label>
				<fieldset>
					<select name="taskDomain" id="taskDomain">
						<option>web</option>
						<option>Android</option>
						<option selected="selected">IOS</option>
						<option>Desktop</option>
						<option>other</option>
					</select>
				</fieldset>

				<label> Task Budget </label>
				<input id="taskBudget" type="text" class="form-control" name="taskBudget" placeholder="Enter Project Budget">
				<label> Task Duration in days </label>
				<!-- <input type="text" id="datepicker" class="form-control"
					placeholder="Enter Project Duration" name="taskDuration" size="30" /> -->
				<input id="dinid" type="text" class="form-control" name="taskDays" placeholder="Enter Task Duration" />
			<!-- <label>Precedor </label>

				<input type="text" id="precedor" class="form-control" name="precedor"
					placeholder="Precedor" /> -->
				<input id="taskDuration" type="hidden" class="form-control" name="taskDuration" readonly />
				<label> Task Description </label>
				<textarea id="taskDescrption" name="taskDescrption" class="form-control" rows="3"></textarea>
				<s:hidden id="taskProId" name="taskProId" value="%{proj.proID}"></s:hidden>
			<%-- 		<s:textfield label="Project Id" name="%{#parameters.taskProId} " ></s:textfield> --%>
			<%-- 	<s:submit value="submit Task"></s:submit> --%>
			<!-- 			<button  value="Submit Task" id="btnSubmit" ></button> -->
		</s:form>
	</div>
	

	<script src="pm/js/jquery.fn.gantt.js"></script>
	<script src="pm/js/jquery-te-1.4.0.min.js"></script>
	<script type="text/javascript">
		var src = [];
	</script>
	<s:iterator value="tasks">
		<script>
			$(function() {

				var sdate = new Date("${taskPostedOn}");
				var smilli = sdate.getTime();

				var ldate = new Date("${taskDuration}");
				var lmilli = ldate.getTime();

				src.push({
					name : "${taskTitle}",
					desc : "${taskDescrption}",
					values : [ {
						from : "/Date(" + smilli + ")/",
						to : "/Date(" + lmilli + ")/",
						label : "${taskDomain}",
						customClass : "ganttRed"
					} ]

				});
			});
		</script>
	</s:iterator>
	<script>
		$(function() {
			"use strict";
			$(".gantt").gantt({
				source : src,
				navigate : "scroll",
				maxScale : "hours",
				itemsPerPage : 10
			});
			prettyPrint();

		});
	</script>
</body>
</html>