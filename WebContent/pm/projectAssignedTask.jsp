<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	
	<link href="pm/css/ideaTasks.css" rel="stylesheet" type="text/css">
		  	<link href="en/css/bootstrap.min.css" rel="stylesheet">
	    	<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">	
		
	<script src="pm/js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
	$(function() {
		
		$("#jRate").jRate({
			rating: 3,
			strokeColor: 'black',
			width: 20,
			height: 20,						
		 	onChange: function(rating) {
			 	
				console.log("OnChange: Rating: "+rating);
			}, 
			onSet: function(rating) {
				$('#rating').val( Math.round(rating));
				//alert("hello baba"+rating);
			}
		});
	});
	</script>
</head>
<body>


<div class="maincontainer">
		<div class="leftpan">
					
							<div class="ideadetails">			
								<h3> Task Summary </h3>
								<h4><strong><s:property value="%{task.taskTitle}" /> </strong></h4>
								<ul>
								
									<li> <s:property value="%{task.taskDescrption}" /></li> 
									<li> Budget :<s:property value="%{task.taskBudget}" /></li>
									<li> Posted On: <s:property value="%{task.taskPostedOn}" /></li>
									<li> Completion Date:<s:property value="%{task.taskDuration}" /></li>
									<li> Task Domain:<s:property value="%{task.taskDomain}" /></li>
									<li> Status: Assigned</li>
									<!-- <li><a href="#">Edit</a></li> -->
								</ul>
							</div>
							
							<%-- <div class="linksdetails">
								<ul>
									<li>
										<s:a action="ideaTasks">Idea Details
										<s:param name="userid" value="#session.userid" />
										<s:param name="ideaId" value="%{ideaId}" />
										</s:a>
									</li>
									<li><a href="#">Idea Owner </a></li>									
									<li>
										<s:a action="ideataskGantchart">Idea Gantt Chart 
										<s:param name="userid" value="#session.userid" />
										<s:param name="ideaId" value="%{ideaId}" />
										</s:a>
									</li>		
								</ul>
							</div> --%>
		
		</div>
		<!-- mid pan starts here  -->
		<div class="middlepan">
				
				<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											
    										<th>Project</th>
											<th>Title</th>
											
											<th>Domain</th>
											<th>Posted on</th>
											<th>Budget</th>
											
											
											
							
                                    </tr>
                                </thead>
                                <tbody>
								<tr>
                                    		
                                </tr>    
                                </tbody>
                            </table>
                       </div>
                        
			
				
		</div>
		<!-- mid pan ends here  -->
		<div class="rightpan">
					<div class="ideaowner">
						<h4> Task Contributor</h4>
						<%-- <h3>
						<strong>
						<a href="#">
						<s:property value="%{user.firstname}" /> 
						<s:property value="%{user.lastname}" /></a></strong></h3> --%>
						
						<h3>
												<strong>
													<s:url id="viewPmpublic" action="viewPmpublic">
			             							<s:param name="userid" value="%{user.userid}"></s:param>
			             							<s:param name="username" value="%{user.username}"></s:param>
			            			 				</s:url>			
													<s:a href="%{viewPmpublic}"><s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/></s:a>
												</strong>	
						</h3>
						<p><s:property value="%{user.country}" /></p>
							<!-- <li><s:property value="%{user.username}" /></li> -->
							<!-- <li><s:property value="%{user.firstname}" /></li> -->
							<!-- <li><s:property value="%{user.lastname}" /></li> -->
							
						<div id="jRate" style="height: 10px; width: 40px;">
						</div>
						<div Class="ratebtn">
							<s:form action="rateCn">
								<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="rater" />
								<s:hidden id="rating" name="rating"></s:hidden>									
								<s:hidden name="taskID" value="%{task.taskID}"></s:hidden>									
								<s:submit cssClass="btn btn-success" value="Rate"></s:submit>
      						</s:form>
									
									<%-- <s:submit cssClass="btn btn-success" value="Rate"></s:submit> --%>
									
						<%-- </s:form> --%>
						
						</div>
						<hr>
						<div>
							<s:form action="taskCompleted">
								<s:hidden name="taskID" value="%{task.taskID}"></s:hidden>	
								<s:hidden name="taskFlag" value="4"></s:hidden>																	
								<s:submit cssClass="btn btn-success" value="Completed"></s:submit>
      						</s:form>
      					</div>
<%-- 						<div  title="Critical Path" id="crtical" align="left" style="background-color:white; border-color:red; border-style:double; width: auto;height:auto; overflow: hidden;   "> 
							<b style="font-style: italic; ">Critical Path:</b> &nbsp; <s:property value="alist"  />

						</div> --%>
						
						
						<%-- <div>
								<!-- <h3>Send to Entrepreneaur</h3> -->
								<s:form action="p2eMsg">
								
									<s:textarea name="ideaCommit"></s:textarea>
									<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>
									<s:submit value="Send message"></s:submit>
								</s:form>
							
								<s:label cssClass="commitlabel" label="enpm" value="%{idea.ideaCommit}"></s:label>						
						</div>
						 --%>	
					
					</div>
		
		</div>
</div>






<!-- nnew section of code ends here -->




</body>
</html>