<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="pm/css/bidsbyme.css">
	
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet"> 
	
</head>
<body>


		<div class="mybidform">
		
			<div class="headertext">
			<h2> Task Bid Request Proposals </h2>
			</div>
			
			<div class="mybidleftpan">
					<div class="tabbar">
							<div class="sortdesc">
								<label> Sort By </label> &nbsp; 
								<span>
									<select>
										<option>By name</option>
										<option>By Amount</option>
									</select>
								</span>
							</div>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
										<!-- <th align="left" >Idea Title </th> 
    									<th>Description</th> 
    									<th>Domain</th> 
    									<th>Bid Date</th> 
    									<th>Idea Budget</th> -->
    									<!-- <th>Duration</th> -->
    									<!-- <th> username</th> -->
										<th>firstname</th>
										<th>lastname</th>
										<th>country</th>
										<th>Bid Amount</th>
										<th>Detail</th>
    									<!-- <th>Cancel</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    	<s:if test="%{taskBids.isEmpty()}">No bids!</s:if>
										<s:else>
									<s:iterator value="taskBids">
									<tr>
										<%-- <td>
											<s:property value="username"/>
										</td> --%>
										<td>
											<s:property value="firstname"/>		
										</td>			
										<td>		
											<s:property value="lastname"/>
										</td>		
										<td>		
											<s:property value="country"/>	
										</td>
										<td>		
											<s:property value="bidsAmount"/>	
										</td>		
										<td>		
 											<s:form action="taskBidder" >
											<s:hidden name="userid" value="%{userid}" ></s:hidden>
											<s:hidden name="bidsAmount" value="%{bidsAmount}" ></s:hidden>	 
											<s:hidden name="taskID" value="%{task.taskID}" ></s:hidden>	    			
							    	 		<s:submit cssClass="btn btn-success" value="Open"></s:submit>
                   						</s:form>
                   						</td>
                   			</tr>	 
                   
      </s:iterator>
      </s:else>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Task Title</h4>
		 				 <h4><s:property value="%{task.taskTitle}"/></h4>  
		 				<ul>
		 					<li>Description<p><s:property value="%{task.taskDescrption}"/></p></li>
		 					<li>Duration<p><s:property value="%{task.taskDuration}"/></p></li>
		 					<li>Budget <p><s:property value="%{task.taskBudget}"/></p></li>
		 					
		 					
		 					
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the task </p>
		 			
		 			</div>
			
			</div>
			
			
			<%-- 
							<table class="tablesorter"> 
								<thead> 
									<tr> 
   					 
					    				<th align="left" >Idea Title </th> 
    									<th>Description</th> 
    									<th>Domain</th> 
    									<th>Posted on</th> 
    									<th>Budget</th>
    									<th>Duration</th>
									</tr> 
								</thead> 
										<s:iterator value="ideas">
										<tr>
											<td><s:property value="ideaTitle" /></td>
											<td><s:property value="ideaDescrption" /></td>				
											<td><s:property value="ideaDomain" /></td>
											<td><s:property value="ideaPostedOn" /></td>
											<td><s:property value="ideaBudget" /></td>
											<td><s:property value="ideaDuration" /></td>
										</tr>
										</s:iterator>
							</table> --%>
				
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>

<!-- new code addition ends here  -->
<%-- 	<h3>List of bids</h3>
	
	
	
			<s:form action="updateProj">
				<s:textfield label="Task Name" value="%{task.taskTitle}" name="taskTitle"></s:textfield>
				<s:textfield label="Task Cost" name="taskBudget"
					value="%{task.taskBudget}"></s:textfield>
				<s:textfield label="Task duration" name="taskDuration"
					value="%{task.taskDuration}"></s:textfield>
				<s:textfield label="Task description" name="taskDescrption"
					value="%{task.taskDescrption}"></s:textfield>
										<s:submit value="Update" ></s:submit>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden>

			</s:form>
	
	
			<br>
					<table class="tablesorter">
						<thead>
							<tr>
								<th> username</th>
								<th>firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Detail</th>
							</tr>
						</thead>
						<s:if test="%{taskBids.isEmpty()}">No bids!</s:if>
						<s:else>
							<s:iterator value="taskBids">
							<tr>
							<td>
								<s:property value="username"/>
							</td>
							<td>
								<s:property value="firstname"/>		
							</td>			
							<td>		
								<s:property value="lastname"/>
							</td>		
							<td>		
								<s:property value="country"/>	
							</td>		
							<td>		
 								<s:form action="taskBidder" >
									<s:hidden name="userid" value="%{userid}" ></s:hidden>
									<s:hidden name="bidsAmount" value="%{bidsAmount}" ></s:hidden>	 
									<s:hidden name="taskID" value="%{task.taskID}" ></s:hidden>	    			
							    	 <s:submit value="Open"></s:submit>
                   				</s:form>
                   			</td>
                   			</tr>	 
                   
      </s:iterator>
      </s:else>
      </table>
 --%>
</body>
</html>