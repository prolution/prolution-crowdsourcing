<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
<title>upload tasks</title>
	
	
			<link href="pm/css/ideaTasks.css" rel="stylesheet" type="text/css">
		  	<link href="en/css/bootstrap.min.css" rel="stylesheet">
	    	<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
			

	
			<!-- this one is for the ganttchart -->
			<link href="pm/css/style.css" type="text/css" rel="stylesheet">
			<link rel="stylesheet" href="pm/css/jquery-ui.css">
			 <script src="pm/js/jquery.min.js"></script>
			<%-- <script src="pm/js/jquery-1.11.2.min.js"></script> --%>
				<script src="pm/js/jquery-ui.js"></script> 


<script>

		$(function() {
			var dialog, form, taskTitle = $("#taskTitle"), taskPostedOn = $("#taskPostedOn"), taskBudget = $("#taskBudget"),  
			taskDomain = $("#taskDomain"), dinid = $("#dinid"), precedor = $("#precedor"),
			 taskDuration = $("#taskDuration"), taskDescrption = $("#taskDescrption"), taskIdeaId = $("#taskIdeaId"),
			  allFields = $([]).add(taskTitle).add(taskPostedOn).add(taskDomain)
					.add(dinid).add(precedor).add(taskDuration).add(
							taskDescrption).add(taskIdeaId).add(taskBudget),
							tips = $( ".validateTips" );

			function updateTips( t ) {
			      tips
			        .text( t )
			        .addClass( "ui-state-highlight" );
			      setTimeout(function() {
			        tips.removeClass( "ui-state-highlight", 1500 );
			      }, 500 );
			    }

			 function checkLength( o, n, min, max ) {
			      if ( o.val().length > max || o.val().length < min ) {
			        o.addClass( "ui-state-error" );
			        updateTips( "Length of " + n + " must be between " +
			          min + " and " + max + "." );
			        return false;
			      } else {
			        return true;
			      }
			    }

			function checkRegexp( o, regexp, n ) {
			      if ( !( regexp.test( o.val() ) ) ) {
			        o.addClass( "ui-state-error" );
			        updateTips( n );
			        return false;
			      } else {
			        return true;
			      }
			    }
							

			function addTask() {
				var valid = true;
			      allFields.removeClass( "ui-state-error" );
			 
			      valid = valid && checkLength( taskTitle, "taskTitle", 3, 80 );
			      valid = valid && checkLength( taskDomain, "taskDomain", 3, 80 );
			      valid = valid && checkLength( dinid, "dinid", 1, 8 );
			      valid = valid && checkLength( taskBudget, "taskBudget", 1, 8 );

			      valid = valid && checkLength( taskDescrption, "ideaDescrption", 3, 100 );
			 
			      valid = valid && checkRegexp( taskTitle, /^[a-z]([0-9a-z_\s])+$/i, "ideaTitle may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
			      valid = valid && checkRegexp( taskDomain, /^[a-z]([0-9a-z_\s])+$/i, "taskDomain may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
			      valid = valid && checkRegexp( taskDescrption, /^[a-z]([0-9a-z_\s])+$/i, "taskDescrption may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
			      valid = valid && checkRegexp( taskBudget, /^([0-9a-zA-Z])+$/, "taskBudget field only allow : 0-9" );

			      valid = valid && checkRegexp( dinid, /^([0-9a-zA-Z])+$/, "dinid field only allow : 0-9" );

			      if ( valid ) {
					form[0].submit();
				//	allFields.removeClass("ui-state-error");
				dialog.dialog("close");
			      }
			      return valid;		
			      }

			dialog = $("#dialog-form").dialog({
				autoOpen : false,
				height : 600,
				width : 550,
				modal : true,
				buttons : {
					"Create an Task" : addTask,
					Cancel : function() {
						dialog.dialog("close");
					}
				},
				close : function() {
					form[0].reset();
					allFields.removeClass("ui-state-error");
				}
			});

			form = dialog.find("#frmToSubmit").on("submit",
					function(event) {
						event.preventDefault();
						addTask();
					});

			$("#create-task").click(function() {
				dialog.dialog("open");

			});

				$("#btnSubmit").click(function() {
					$("#frmToSubmit").submit();
					//alert("testing");
				});

				$("#dinid")
						.on(
								"change",
								function() {
									var date = new Date($("#taskPostedOn")
											.val()), days = parseInt(
											$("#dinid").val(), 10);
									if (!isNaN(date.getTime())) {

										date.setDate(date.getDate() + days);
										$("#taskDuration").val(
												date.toInputFormat());
									} else {
										alert("Invalid Date");
									}
								});
				Date.prototype.toInputFormat = function() {
					var yyyy = this.getFullYear().toString();
					var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
					var dd = this.getDate().toString();
					return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
							+ (dd[1] ? dd : "0" + dd[0]); // padding
				};

				$("#datepicker").datepicker({
					dateFormat : 'yy-mm-dd'
				});

				$("#taskDomain").selectmenu();

				$("#files").selectmenu();

				$("#number").selectmenu().selectmenu("menuWidget").addClass(
						"overflow");

		})(jQuery, this, document);

</script>
<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}
.ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }

table th:first-child {
	width: 150px;
}
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}
</style>

</head>

<body>
<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%>


<div class="maincontainer">
		<div class="leftpan">
					
							<div class="ideadetails">			
								<h3> Idea </h3>
								<h4><strong><s:property value="%{idea.ideaTitle}" /> </strong></h4>
								<ul>
									<li> <s:property value="%{idea.ideaDescrption}" /></li> 
									<li> Budget :<s:property value="%{idea.ideaBudget}" /></li>
									<li> Posted On: <s:property value="%{idea.ideaPostedOn}" /></li>
									<li> Completion Date:<s:property value="%{idea.ideaDuration}" /></li>
									<li> Idea Domain:<s:property value="%{idea.ideaDomain}" /></li>
									<li> Status: In Process</li>
									<!-- <li><a href="#">Edit</a></li> -->
								</ul>
							</div>
							
							<div>
								<ul>
									<li>
										<s:a action="ideaTasks">Idea Details
										<s:param name="userid" value="#session.userid" />
										<s:param name="ideaId" value="%{ideaId}" />
										</s:a>
									</li>
									<li><a href="#">Idea Owner </a></li>									
									<li>
										<s:a action="ideataskGantchart">Idea Gantt Chart 
										<s:param name="userid" value="#session.userid" />
										<s:param name="ideaId" value="%{ideaId}" />
										</s:a>
									</li> 		
								</ul>
							</div>
		
		</div>
		<!-- mid pan starts here  -->
		<div class="middlepan">
				<div>                        
					<div class="contain">
						<div class="gantt"></div>
					</div>
				</div>
				
		</div>
		<!-- mid pan ends here  -->
		<div class="rightpan">
					<div class="ideaowner">
						<h4> Idea Owner </h4>
						<h3><strong><a href="#"><s:property value="%{user.firstname}" /> <s:property value="%{user.lastname}" /></a></strong></h3>
						<p><s:property value="%{user.country}" /></p>
							<!-- <li><s:property value="%{user.username}" /></li> -->
							<!-- <li><s:property value="%{user.firstname}" /></li> -->
							<!-- <li><s:property value="%{user.lastname}" /></li> -->
							
						<div id="jRate" style="height: 10px; width: 40px;">
						</div>
						<div>
						<s:form action="rateEn">
									<input type="hidden" value="<%=session.getAttribute("userid")%>" name="rater" />
									<s:hidden id="rating" name="rating"></s:hidden>
									<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>
									<s:submit value="Rate User"></s:submit>
						</s:form>
						</div>
						<div  title="Critical Path" id="crtical" align="left" style="background-color:white; border-color:red; border-style:double; width: auto;height:auto; overflow: hidden;   "> 
							<b style="font-style: italic; ">Critical Path:</b> &nbsp; <s:property value="alist"  />

						</div>
						
						
							
							
					
					</div>
		
		</div>
</div>



<!-- new code starts here -->


	


	
	
<%-- 	<h3>Assigned Idea</h3>

			<div class="form-group">
				<table class="tablesorter">
						<thead>
							<tr>
								<th>Idea</th>
								<th>Description</th>
								<th>Domain</th>
								<th>Posted on</th>
								<th>Budget</th>
								<th>Duration</th>
							</tr>
						</thead>
							<tr>
								<td><s:property value="%{idea.ideaTitle}" /></td>
								<td><s:property value="%{idea.ideaDescrption}" /></td>
								<td><s:property value="%{idea.ideaDomain}" /></td>
								<td><s:property value="%{idea.ideaPostedOn}" /></td>
								<td><s:property value="%{idea.ideaBudget}" /></td>
								<td><s:property value="%{idea.ideaDuration}" /></td>

							</tr>
						</table>
							</div>
	 --%>
						


		
		
					<div  id="dialog-form" style="overflow: hidden;">
						<header>
								<h3>Upload Task</h3>
						</header>
								<s:form id="frmToSubmit" action="ideaTaskPost">
									<label> Task Title </label>
									<input type="text" class="form-control" id="taskTitle" name="taskTitle" placeholder="Enter Project Title">
									<input type="hidden" class="form-control" name="taskPostedOn" id="taskPostedOn" value="${currentDate}" />
									<label> Task Category </label>
									<fieldset>
										<select name="taskDomain" id="taskDomain">
											<option>web</option>
											<option>Android</option>
											<option selected="selected">IOS</option>
											<option>Desktop</option>
											<option>other</option>
										</select>
									</fieldset>

									<label> Project Budget </label>
									<input type="text" class="form-control" id="taskBudget" name="taskBudget" placeholder="Enter Project Budget">
									<label> Task Duration in days </label>
									<input id="dinid" type="text" class="form-control" name="taskDays" placeholder="Enter Task Duration" />
									<!-- <label>Precedor </label>
									<input type="text" class="form-control" id="precedor" name="precedor" placeholder="Precedor" /> -->
									<input id="taskDuration" type="hidden" class="form-control" name="taskDuration" readonly />
									<label> Task Description </label>
									<textarea id="taskDescrption" name="taskDescrption" class="form-control" rows="3"></textarea>
									<s:hidden id="taskIdeaId" name="taskIdeaId" value="%{idea.ideaId}"></s:hidden>
									<%-- 		<s:textfield label="Project Id" name="%{#parameters.taskProId} " ></s:textfield> --%>
								</s:form>
					</div>
		
						
		


	

	<%-- 							<div style="overflow: hidden;">
								<header>
										<h3>Send to Entrepreneaur</h3>
								</header>
								<s:form action="p2eMsg">
									<s:textarea name="ideaCommit"></s:textarea>
									<s:hidden name="ideaId" value="%{idea.ideaId}"></s:hidden>
									<s:submit value="Send message to Entrepreneaur"></s:submit>
								</s:form>
							
							<s:label label="communication b/w Pm and En" value="%{idea.ideaCommit}"></s:label>
	
					</div> --%>
	


	 <s:iterator value="tasks">
		<script>
			$(function() {

				var sdate = new Date("${taskPostedOn}");
				var smilli = sdate.getTime();

				var ldate = new Date("${taskDuration}");
				var lmilli = ldate.getTime();

				src.push({
					name : "${taskTitle}",
					desc : "${taskDescrption}",
					values : [ {
						from : "/Date(" + smilli + ")/",
						to : "/Date(" + lmilli + ")/",
						label : "${taskDomain}",
						customClass : "ganttRed"
					} ]

				});
			});
		</script>
	</s:iterator>
	
	<script src="js/jRate.min.js"></script>
	<script src="pm/js/jquery.fn.gantt.js"></script>

	<script type="text/javascript">
		var src = [];

		$(function() {

			$("#jRate").jRate({
				rating : 4,
				strokeColor : 'black',
				width : 20,
				height : 20,
				onChange : function(rating) {
					//$('#rating').val(rating);

					//console.log("OnChange: Rating: "+rating);
				},
				onSet : function(rating) {
					$('#rating').val(Math.round(rating));
					//alert("hello baba"+rating);
				}
			});
		});
	</script>
	<script>
		$(function() {
			"use strict";
			$(".gantt").gantt({
				source : src,
				navigate : "scroll",
				maxScale : "hours",
				itemsPerPage : 10
			});
			prettyPrint();

		});
	</script>
	<%-- <script src="pm/js/jquery-te-1.4.0.min.js"></script> --%>
 
	
</body>
</html>