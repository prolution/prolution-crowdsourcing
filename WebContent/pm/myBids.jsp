<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
<title> Ideas bidded by me !</title>

	<link rel="stylesheet" href="pm/css/bidsbyme.css">
	
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet"> 
	
	
							
	
</head>
<body>


		
		<div class="mybidform">
		
			<div class="headertext">
			<h2> Bid Request Proposals </h2>
			</div>
			
			<div class="mybidleftpan">
					<div class="tabbar">
							<div class="sortdesc">
								<label> Sort By </label> &nbsp; 
								<span>
									<select>
										<option>By name</option>
										<option>By Job</option>
									</select>
								</span>
							</div>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
										<th align="left" >Idea Title </th> 
    									<!-- <th>Description</th>  -->
    									<th>Domain</th> 
    									<th>Bid Date</th> 
    									<th>Idea Budget</th>
    									<!-- <th>Duration</th> -->
    									<th>Cancel</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <s:iterator value="ideas">
										
										<tr>
											<td>
												<%-- <s:a action="ideaTasks"><s:property value="ideaTitle" />
												<s:param name="userid" value="#session.userid" />
												<s:param name="ideaId" value="%{ideaId}" />
												
												</s:a> --%>
												<s:property value="ideaTitle" />
											</td>
											<%-- <td><a href="#"><s:property value="ideaTitle" /></a></td> --%>
											<%-- <td><s:property value="ideaDescrption" /></td> --%>				
											<td><s:property value="ideaDomain" /></td>
											<td><s:property value="ideaPostedOn" /></td>
											<td><s:property value="ideaBudget" /></td>
											<%-- <td><s:property value="ideaDuration" /></td> --%>
											<!-- <td><input type="checkbox"/></td> -->
											<!-- <td><input type="button" value="x" class="btn" /></td> -->
											<td><a href="#"><i class="glyphicon glyphicon-remove-circle"></i></a></td>
										</tr>
									</s:iterator>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite"> Invite Stats <span>(Last 0 Days)</span></h4>
		 				<ul>
		 					<li>Job Invites</li>
		 					<li>Submitted Proposals</li>
		 					<li>Declined</li>
		 					<li>No Response</li>
		 				</ul>
		 			</div>
		 			<div class="counterstats">
		 				<ul>
		 					<li>0</li>
		 					<li>0</li>
		 					<li>0</li>
		 					<li>0</li>
		 				</ul>
		 			</div>
		 			<div class="note">
		 			<p>Note: This section provides the stats, which gives the overall overview </p>
		 			
		 			</div>
			
			</div>
			
			
			<%-- 
							<table class="tablesorter"> 
								<thead> 
									<tr> 
   					 
					    				<th align="left" >Idea Title </th> 
    									<th>Description</th> 
    									<th>Domain</th> 
    									<th>Posted on</th> 
    									<th>Budget</th>
    									<th>Duration</th>
									</tr> 
								</thead> 
										<s:iterator value="ideas">
										<tr>
											<td><s:property value="ideaTitle" /></td>
											<td><s:property value="ideaDescrption" /></td>				
											<td><s:property value="ideaDomain" /></td>
											<td><s:property value="ideaPostedOn" /></td>
											<td><s:property value="ideaBudget" /></td>
											<td><s:property value="ideaDuration" /></td>
										</tr>
										</s:iterator>
							</table> --%>
				
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>

</body>
</html>