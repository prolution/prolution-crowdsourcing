<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pro-Lution</title>
 
		<link href="pm/css/myProject.css" rel="stylesheet" type="text/css">
	    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
		
		
				<link rel="stylesheet" href="pm/css/jquery-ui.css"> 
		
		<script src="pm/js/jquery.min.js"></script>
		<script src="pm/js/jquery-1.11.2.min.js"></script>
		<script src="pm/js/jquery-ui.js"></script>
		
<script>
window.history.forward();function noBack(){window.history.forward();}

	$(function() {

		var dialog, form, proTitle = $("#proTitle"), proBudget = $("#proBudget"), proPostedOn = $("#proPostedOn"), proDomain = $("#proDomain"), dinid = $("#dinid"), proDuration = $("#proDuration"), proDescrption = $("#proDescrption"), allFields = $(
				[]).add(proTitle).add(proPostedOn).add(proDomain).add(dinid).add(proBudget)
				.add(proDuration).add(proDescrption),
				tips = $( ".validateTips" );
				

		function updateTips( t ) {
		      tips
		        .text( t )
		        .addClass( "ui-state-highlight" );
		      setTimeout(function() {
		        tips.removeClass( "ui-state-highlight", 1500 );
		      }, 500 );
		    }

		 function checkLength( o, n, min, max ) {
		      if ( o.val().length > max || o.val().length < min ) {
		        o.addClass( "ui-state-error" );
		        updateTips( "Length of " + n + " must be between " +
		          min + " and " + max + "." );
		        return false;
		      } else {
		        return true;
		      }
		    }

		function checkRegexp( o, regexp, n ) {
		      if ( !( regexp.test( o.val() ) ) ) {
		        o.addClass( "ui-state-error" );
		        updateTips( n );
		        return false;
		      } else {
		        return true;
		      }
		    }
		
		function addTask() {
			var valid = true;
		      allFields.removeClass( "ui-state-error" );
		 
		      valid = valid && checkLength( proTitle, "Title", 3, 80 );
		      valid = valid && checkLength( proDomain, "Domain", 3, 80 );
		      valid = valid && checkLength( dinid, "Days", 1, 8 );
		      valid = valid && checkLength( proBudget, "Budget", 1, 8 );

		      valid = valid && checkLength( proDescrption, "Descrption", 3, 100 );
		 
		      valid = valid && checkRegexp( proTitle, /^[a-z]([0-9a-z_\s])+$/i, "Title may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
		      valid = valid && checkRegexp( proDomain, /^[a-z]([0-9a-z_\s])+$/i, "Domain may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
		      valid = valid && checkRegexp( proDescrption, /^[a-z]([0-9a-z_\s])+$/i, "Descrption may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
		      valid = valid && checkRegexp( proBudget, /^([0-9a-zA-Z])+$/, "Budget field only allow : 0-9" );

		      valid = valid && checkRegexp( dinid, /^([0-9a-zA-Z])+$/, "Days field only allow : 0-9" );

		      if ( valid ) {
				form[0].submit();
			//	allFields.removeClass("ui-state-error");
			dialog.dialog("close");
		      }
		      return valid;		
		      }

		dialog = $("#dialog-form").dialog({
			autoOpen : false,
			height : 700,
			width : 600,
			modal : true,
			buttons : {
				"Create a Project" : addTask,
				Cancel : function() {
					dialog.dialog("close");
				}
			},
			close : function() {
				form[0].reset();
				allFields.removeClass("ui-state-error");
			}
		});

		form = dialog.find("#frmToSubmit").on("submit",
				function(event) {
					event.preventDefault();
					addTask();
				});

		$("#create-task").click(function() {
			dialog.dialog("open");

		});

	$("#btnSubmit").click(function() {
			$("#frmToSubmit").submit();
			//alert("testing");
		}); 

		$("#dinid")
				.on(
						"change",
						function() {
							var date = new Date($("#proPostedOn").val()), days = parseInt(
									$("#dinid").val(), 10);
							if (!isNaN(date.getTime())) {

								date.setDate(date.getDate() + days);
								$("#proDuration").val(date.toInputFormat());
							} else {
								alert("Invalid Date");
							}
						});
		Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
			var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = this.getDate().toString();
			return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"
					+ (dd[1] ? dd : "0" + dd[0]); // padding
		};
		$("#datepicker").datepicker({
			dateFormat : 'yy-mm-dd'
		});

		$("#proDomain").selectmenu();

		$("#files").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
</script>

<style>
fieldset {
	border: 0;
	width: 200px;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}

table th:first-child {
	width: 150px;
}
.ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
/* Bootstrap 3.x re-reset */
.fn-gantt *,.fn-gantt *:after,.fn-gantt *:before {
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
}
</style>
</head>
<body >


	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%> 
	
	<div class="leftpan">
			<div class="sidesublinks">
				<ul>
					<li><strong><a href="#"> All Projects </a></strong> </li>
					<li><a href="#"> Pending Projects</a></li>
					<li><a href="#"> Completed Projects </a></li>
					<li><a href="#"> My Teams </a></li>
				</ul>
			</div>
	</div>
	<div class="rightpan">
		<button class="btn btn-primary" id="create-task" type="button">Create new Project</button> 
	
	<!-- popup dialog form -->
		
		<div id="dialog-form">
			<header>
			<h3>Upload New Project</h3>
			</header>
					<p class="validateTips">All form fields are required.</p>

					<s:form id="frmToSubmit"  action="projectposted" method="post">
					<label> Project Title </label>
						<%-- 				<s:hidden name="proOwner" value="%{sessionScope.userid}"></s:hidden> --%>
						<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" id="proOwner" name="proOwner" />
<input type="text" class="form-control" id="proTitle" name="proTitle" placeholder="Enter Project Title">

<input type="hidden" class="form-control" name="proPostedOn" id="proPostedOn" value="${currentDate}" />

				<label> Project Category </label>
	<fieldset>
					<select name="proDomain" id="proDomain">
						<option>web</option>
						<option>Android</option>
						<option selected="selected">IOS</option>
						<option>Desktop</option>
						<option>other</option>
					</select>
				</fieldset>
				<label> Project Budget </label>
<input type="text" class="form-control" id="proBudget" name="proBudget"
					placeholder="Enter Project Budget">

				<label> Project Duration in days </label>

	<input id="dinid" type="text" class="form-control" name="proDays"
					placeholder="Enter Project Duration" />
 <input id="proDuration" type="hidden" class="form-control"
					name="proDuration" readonly />
				<!-- 							<input type="text" id="datepicker" -->
				<!-- 							class="form-control" placeholder="Enter Project Duration" -->
				<!-- 							name="proDuration" size="30">  -->
				<label> Project Description </label>
				<textarea name="proDescrption" id="proDescrption"
					class="form-control" rows="3"></textarea>
			</s:form>
		</div>
	
<!-- popup dialog form -->
			
			<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											<th>Project Name</th>
											<!-- <th>Project Description</th> -->
											<th>Domain</th>
											<th>Posted on</th>
											<th>Budget</th>
											<th>Duration</th>
    										<th>Cancel</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <s:if test="%{project.isEmpty()}">No Projects!</s:if>
										<s:else>
                                    	<s:iterator value="project">
											<tr>
												<td>
													<s:a action="projectTasks"><s:property value="proTitle" />
													<s:param name="userid" value="#session.userid" />
													<s:param name="proID" value="%{proID}" />
													</s:a>
												</td>
												<%-- <td><s:property value="proDescrption" /></td> --%>
												<td><s:property value="proDomain" /></td>
												<td><s:property value="proPostedOn" /></td>
												<td><s:property value="proBudget" /></td>
												<td><s:property value="proDuration" /></td>
												<td>
													<a href="#">
														<i class="glyphicon glyphicon-remove-circle">
														</i>
													</a>
												</td>
											</tr>
										</s:iterator>
										  </s:else>
                                    
                                    
                                </tbody>
                            </table>

                        </div>
	
	</div>
	
	
	
	


	





</body>
</html>