<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
 <link href="en/css/sb-admin.css" rel="stylesheet">     
 <!-- <link href="pm/css/pmbanner.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="pm/css/dropdownstyle.css" type="text/css" media="screen"/>
	<link href="pm/css/topdropdown.css" rel="stylesheet" type="text/css"> -->
	
	
	  
     <link href="en/css/bootstrap.min.css" rel="stylesheet">     
    
	<!-- <link href="en/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->


		<link href="public/css/pfindusers.css" rel="stylesheet" type="text/css">
		<link href="public/css/findcrowdusers.css" rel="stylesheet" type="text/css">
			<link rel="stylesheet" href="bootstrap/css/prolution-style.css"> 
			<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> 
			<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
	<div class="newcontainer">
	
				<ul class="topmenunew">
					
					<li><a href="findwork">Find Work</a></li>
					<li><a href="crowduserSearch">Find Pro-Users</a></li>
					<li><a href="goHome #our-work">How it Works</a></li>
					<!-- <li><a href="projectSearch">Browse Projects</a></li> -->
					<li><a href="prelogin">LogIn</a> <span style="color:white;">or</span> <a href="preSignup">Signup</a></li>	
						
			
					
				</ul>
				
				
				
	
	</div>
	
	<div class="container">
				<div class="logo-wrapper">
					<a class="navbar-brand" href="goHome"> <!-- Pro<em>- Lution</em>-->
						<img class="projectlogo" src="bootstrap/img/ProLogo.png">

					</a>
				</div>

	</div>
	</nav>
	
	<div class="bodycontainer">
	
				<div class="navSearch">
		
		
						<!-- 		<p>Search Bar </p> -->
		
					<div class="submenulinks">
						<p>   <strong> <a href="crowduserSearch">Crowd Users</a></strong>  &nbsp;&nbsp; |&nbsp;&nbsp; <a href="searchedIdeas">Find Ideas</a> |&nbsp;&nbsp; <a href="viewTasks">Find Tasks</a>  </p>
					</div>	
				
				 		<!-- <form class="navbar-form" > -->
				 	<form id="frms" action="searchedUsers">
	    				<div class="input-group stylish-input-group">
        					<span class="input-group-addon">
            					<button id="btnSearch" type="submit"><span class="glyphicon glyphicon-search"></span></button>  
        					</span>
        						<input name="str" id="str" type="text" class="form-control input-sm searchbox" placeholder="Search">
    					</div>
	     			</form>
    						<!-- /.search -->
				 
						<div class="submenubutton">
							<span>Want to post a Job ?</span> &nbsp;<input class="btnpostproject" type="button" value="Post a Job "/>
						</div> 
				</div>
				
				
	</div>
		
		
		
		
			
		
		<div class="leftFilterbar">
		
					
		 <ul class="menubar">
			
		 	<li class="headerline"><strong> Everyone </strong></li>
		 	<li> <a href="findcrowdusersEn">Entrepreneurs</a> </li>
		 	<li> <a href="findcrowdusersPm">Project Managers</a> </li>
		 	<li> <a href="findcrowdusersCn">Contributors</a> </li>
		 	
		 	<br><br>
		 	<li class="headerline"> <strong>All Categories</strong> </li>
		 	<li> <a href="">Desktop Development</a></li>
		 	<li> <a href="">Web Development </a></li>
		 	<li> <a href="">Android Development </a></li>
		 	<li> <a href="">IOS Development </a></li>
		 	<br><br>
		 	
		 	<li class="headerline"><strong> All Users Location</strong> </li>
		 	<li> <a href="">Country </a> </li>
		 	<br><br>
		 	<li class="headerline"> <strong>All Feedback</strong> </li>
		 	<li> <a href="">Atleast 5</a>  </li>
		 	<li> <a href="">Atleast 4</a> </li>
		 	<li> <a href="">Atleast 3 </a> </li>
		 	<br><br>
		 	<li class="headerline"> <strong>Any Number of Reviews</strong> </li>
		 	<li> <a href="">Atleast 5 </a> </li>
		 	<li> <a href="">Atleast 10 </a> </li>
		 	<li> <a href="">Atleast 15  </a></li>
		 </ul>
		 
		 
		
		</div>
		
		<div class="mainUserBar">
		
				<div class="resultset">
					<p> All Free Lancers &nbsp; <span>( Results Count )</span></p> 
				</div>
				
				<div class="resultsetn">
					
					<span>Sort By</span> &nbsp;
						<div class="btn-group" role="group">
    				
    						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      						Default	
      						<span class="caret"></span>
    						</button>
	    					<ul class="dropdown-menu" role="menu">
      						<li><a href="#">Level (High)</a></li>
      						<li><a href="#">Level (Low)</a></li>
    						</ul>
  						</div>
				</div>
			
			
			

			
					<div class="persondetails">
						<s:iterator value="users">
						
								<div class="follow-ava">
                            	      <img src="public/img/thumbnail.jpg" alt="">
                                </div>
									<div class="userdetails">
									
									<!-- 
									<label>Userid </label>
									<s:property value="userid"/> -->
									
									<!-- <label>Fullname </label> 
									<s:property value="fullName" />-->
									
									<strong>
									<s:url id="viewpublicprofile" action="viewpublicprofile">
			             					<s:param name="userid" value="%{userid}"></s:param>
			             					<s:param name="username" value="%{username}"></s:param>
			            			 	</s:url>			
									<s:a href="%{viewpublicprofile}"><s:property value="fullName" /></s:a>
									</strong>
									
									
									<br>
									
									<s:property value="usertype" />
									
									
			             			<span> &nbsp; | &nbsp; </span>
		
									<!-- <label>Country</label> -->
									<s:property value="country" />
									<span> &nbsp; | &nbsp;  Category &nbsp; | &nbsp; Jobs &nbsp; | &nbsp; Ratings &nbsp;</span>
									<%-- <label>Email</label>
									<s:property value="email" /> --%>
									<br>
									<p> Short Brief description of the user profile which will include a user brief introduction. <br>Short Brief description of the user profile</p>
									<span> Portfolio |  &nbsp; Skills </span>
									</div>
									
									<br><br>
						</s:iterator>
			
				   </div>
		
			
		
		</div>

		
		
</body>
</html>