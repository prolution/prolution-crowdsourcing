<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<title>Pro-Lutions :: Find Work</title>
<link href="public/css/findwork.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="bootstrap/css/prolution-style.css"> 
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> 
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

	<script src="pm/js/jquery-1.11.2.min.js"></script>
		
 <script type="text/javascript">
	//alert("${ideas}");
 		             		
/*  		             			
$.each(users,function (i,u){

}); */

$(document).ready(function(){

	
	 $("#btnSearch").click(function(){

		 $("#frms").submit();
		 });

		 /*
		 var str=$("#txtSearch").text();
		    var json = {"str" : str};
		$.ajax({
	        url: "searchedUsersAjax"+"?jsonRequestdata="+JSON.stringify(json),
	        type: 'POST',
	        success:function(response){
	            //alert(response); iusers
	        	//$("#iusers").val(response.users); 

	        	$.each(response.users, function(index, user) {
	                $('#susers').append('<li><a href="#">'+ user.userid +'</a></li>');
	                $('#susers').append('<li><a href="#">'+ user.fullName +'</a></li>');
	                $('#susers').append('<li><s:property value='+ user.country +'/></li>');
	                
	                
	            });            
	        },
	        error:function(jqXhr, textStatus, errorThrown){
	            alert(textStatus);
	        }
	    });		
	}); */

	           			 
	

	$("#kRate").each(function(index, value) { 
	   // alert($(this).attr('id')); 
	});
	    // alert("say hello");
	    
	    $.each(users.user,function (i,u){

	alert(u.rating);
}); 

	function stars () {
			var that = this;
			//alert("${rating}");
			$("this").jRate({
				rating: 3,
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		}
});

  </script>

</head>
<body>
		
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="newcontainer">
	
				<ul class="topmenunew">
					
					<li><a href="findwork">Find Work</a></li>
					<li><a href="crowduserSearch">Find Pro-Users</a></li>
					<li><a href="go Home #our-work">How it Works</a></li>
					<!-- <li><a href="projectSearch">Browse Projects</a></li> -->
					<li><a href="prelogin">LogIn</a> <span style="color:white;">or</span> <a href="preSignup">Signup</a></li>	
						
			
					
				</ul>
				
				
				
	
	</div>
	
	<div class="container">
				<div class="logo-wrapper">
					<a class="navbar-brand" href="goHome"> <!-- Pro<em>- Lution</em>-->
						<img class="projectlogo" src="bootstrap/img/ProLogo.png">

					</a>
				</div>

	</div>
	</nav>
	
	<div class="bodycontainer">
	
				<div class="navSearch">
		
		
						<!-- 		<p>Search Bar </p> -->
		
					<div class="submenulinks">
						<p>   <strong> <a href="crowduserSearch">Crowd Users</a></strong>  &nbsp;&nbsp; |&nbsp;&nbsp; <a href="searchedIdeas">Find Ideas</a> |&nbsp;&nbsp; <a href="viewTasks">Find Tasks</a>  </p>
					</div>	
				
				 		<!-- <form class="navbar-form" > -->
				 	<form id="frms" action="searchedUsers">
	    				<div class="input-group stylish-input-group">
        					<span class="input-group-addon">
            					<button id="btnSearch" type="submit"><span class="glyphicon glyphicon-search"></span></button>  
        					</span>
        					
        						<input name="str" id="str" type="text" class="form-control input-sm searchbox" placeholder="Search">
    					</div>
	     			</form>
    						<!-- /.search -->
				 
						<div class="submenubutton">
							<span>Want to post a Job ?</span> &nbsp;<input class="btnpostproject" type="button" value="Post a Job "/>
						</div> 
				</div>
				
				
				
				<!-- left pan bar for search resutls  -->
				
				
				<div class="leftFilterbar">
		
					
		 
		 <ul class="menubar">
			
		 	<li class="headerline"><strong> All </strong></li>
		 	<li> <a href="findwork">All Categories</a> </li>
		 	
		 	<br><br>
		 	<li class="headerline"> <strong>Categories</strong> </li>
		 	<li> <a href="searchedDesktopProjects">Desktop Development</a></li>
		 	<li> <a href="webSearch">Web Development </a></li>
		 	<li> <a href="searchedAndroidProjects">Android Development </a></li>
		 	<li> <a href="searchedIosProjects">IOS Development </a></li>
		 	<br><br>
		 	
		 	<li class="headerline"><strong> Budget</strong> </li>
		 	<li> <a href="">Any Budget </a> </li>
		 	<li> <a href="">Specific Budget </a> </li>
		 	
		 </ul>
		 
		 
		
		</div>
		
		<div class="mainUserBar">
		
				<div class="resultset">
					<p> All Jobs &nbsp; <span>( Results Count )</span></p> 
				</div>
				
				<div class="resultsetn">
					
					<span>Sort By</span> &nbsp;
					
				</div>
			
			
			

			
					<div class="persondetails">
					
				
					
					<p>Click on the links on left side to refine search </p>
					
					<hr>
					
						 <s:iterator value="ideas">
						
								<div class="follow-ava">
                            	      <img src="public/img/thumbnailold.jpg" alt="">
                                </div>
									<div class="userdetails">
									
									<label>Idea: </label>
										<strong>
											<s:url id="viewPmpublic" action="viewPmpublic">
			             						<s:param name="ideaId" value="%{ideaId}"></s:param>
			             						<s:param name="ideaTitle" value="%{ideaTitle}"></s:param>
			            			 		</s:url>			
											<s:a href="%{viewPmpublic}"><s:property value="ideaTitle" /></s:a>
										</strong>
									
									<%-- <label>ideaTitle </label>
									<s:property value="ideaTitle" /> --%>
									
									
									<br>
									<%-- <s:url id="viewPmpublic" action="viewPmpublic">
			             			<s:param name="userid" value="%{userid}"></s:param>
			             			<s:param name="username" value="%{username}"></s:param>
			            			 </s:url>			
									<s:a href="%{viewPmpublic}"><s:property value="ideaTitle" /></s:a> --%>
									
									<!-- <label>UserType</label> -->
									<strong><span>Domain:</strong><s:property value="ideaDomain" /></span>
	
			             			<span> &nbsp; | &nbsp; </span>
	
									<span><strong>Posted On:</strong><s:property value="ideaPostedOn" /></span>
									<span> &nbsp; | &nbsp;  Category &nbsp; | &nbsp; Jobs &nbsp; | &nbsp;  	
																<s:property value="rating" />
									 &nbsp;</span>
									<label>No.of Bids: </label>
									<s:property value="noOfBids" />
									<br>
									<p> <s:property value="ideaDescrption" />. <br></p>
									<%-- <span> Portfolio |  &nbsp; Skills </span> --%>
									</div>
									
									<br><br>
						</s:iterator>
						<s:iterator value="tasks">
						
								<div class="follow-ava">
                            	      <img src="public/img/thumbnailold.jpg" alt="">
                                </div>
									<div class="userdetails">
									
									
									<label><strong>Task:</strong> </label>
									
										<strong>
											<s:url id="viewPmpublic" action="viewPmpublic">
			             						<s:param name="ideaId" value="%{ideaId}"></s:param>
			             						<s:param name="ideaTitle" value="%{ideaTitle}"></s:param>
			            			 		</s:url>			
											<s:a href="%{viewPmpublic}"><s:property value="ideaTitle" /></s:a>
										</strong>
									
									<br>
									<s:url id="viewPmpublic" action="viewPmpublic">
			             			<s:param name="userid" value="%{userid}"></s:param>
			             			<s:param name="username" value="%{username}"></s:param>
			            			 </s:url>			
									<s:a href="%{viewPmpublic}"><s:property value="ideaTitle" /></s:a>
									
									<!-- <label>UserType</label> -->
									<s:property value="ideaDomain" />
									
									
			             			<span> &nbsp; | &nbsp; </span>
	
  
									<!-- <label>Country</label> -->
									<s:property value="ideaPostedOn" />
									<span> &nbsp; | &nbsp;  Category &nbsp; | &nbsp; Jobs &nbsp; | &nbsp; Ratings 	
																<s:property value="rating" />
									 &nbsp;</span>
									<label>noOfBids</label>
									<s:property value="noOfBids" />
									<br>
									<p> <s:property value="ideaDescrption" /></p>
									<%-- <span> Portfolio |  &nbsp; Skills </span> --%>
									</div>
									
									<br><br>
						</s:iterator>
						
						
					
						
			
				  
			
				   </div>
				   <div id="susers" class="persondetails"></div>
		
			
		
		</div>

				
				
				
				<!--  left pan bar ends here  -->
	
	</div>

		
</body>
</html>