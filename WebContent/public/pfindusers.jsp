<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
 <link href="en/css/sb-admin.css" rel="stylesheet">     
 <!-- <link href="pm/css/pmbanner.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="pm/css/dropdownstyle.css" type="text/css" media="screen"/>
	<link href="pm/css/topdropdown.css" rel="stylesheet" type="text/css"> -->
	
	
	  
     <link href="en/css/bootstrap.min.css" rel="stylesheet">     
    
	<!-- <link href="en/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->


		<link href="public/css/pfindusers.css" rel="stylesheet" type="text/css">

</head>
<body>

<%-- <h2>find users </h2>

<p>
	<a href="publicEnSearchp"><i class="icon-user"></i>Enterpreneurs </a> |  
	<a href="publicPmSearchp"><i class="icon-cog"></i>Project Managers</a> |
	<a href="publicCnSearchp"><i class="icon-remove"></i>Contributors</a>
	
</p>

		<div id="tab1" class="tab_content">
			

		
			<table class="tablesorter" cellspacing="0">
				<thead>
					<tr>

						<th align="left">User ID</th>
						<th>Full Name</th>
						
						<th>User name</th>
						<th>Country</th>
						<th>email</th>
						
					</tr>
				</thead>
				<s:iterator value="users">
					<tr>
						<td><s:property value="userid" /></td>
						<td><s:property value="fullName" /></td>
						<td><s:property value="username" /></td>
						
						<td>	
			             <s:url id="viewPmpublic" action="viewPmpublic">
			             <s:param name="userid" value="%{userid}"></s:param>
			             <s:param name="username" value="%{username}"></s:param>
			             </s:url>			
						<s:a href="%{viewPmpublic}"><s:property value="username" /></s:a>
						</td>


						<td><s:property value="country" /></td>
						<td><s:property value="email" /></td>
						
						<td>
							<s:url id="viewProfile" action="viewProfile">
								<s:param name="userid" value="%{userid}"></s:param>
								<s:param name="username" value="%{username}"></s:param>
							</s:url> <s:a href="%{viewProfile}">View Profile</s:a>
						</td>
						
						<td>
							<s:url id="blockprofile" action="blockprofile">
								<s:param name="userid" value="%{userid}"></s:param>
								<s:param name="userstatus" value="I"></s:param>
							</s:url> <s:a href="%{blockprofile}">Block</s:a>
						</td>
						
						<td><a href="">Warn</a></td>
						
						
						
					</tr>
				</s:iterator>
			
			</table>
			 
		</div> --%>
		
		
		
		<div class="navSearch">
		
		
		<!-- 		<p>Search Bar </p> -->
		
					<div class="submenulinks">
						<p>
							   	<strong> 	<s:a action="publicsearching">Find Users
											<s:param name="userid" value="#session.userid" />
											</s:a>
								</strong>  &nbsp;&nbsp; |&nbsp;&nbsp; 
											<s:a action="ideasAvailable">Find Ideas
											<s:param name="bidsUser" value="#session.userid" />
											</s:a>   
								</p>
					</div>	
				
				 <!-- <form class="navbar-form" > -->
				 <form>
	    			<div class="input-group stylish-input-group">
        				<span class="input-group-addon">
            				<button type="submit"><span class="glyphicon glyphicon-search"></span></button>  
        				</span>
        					<input type="text" class="form-control input-sm searchbox" placeholder="Search">
    				</div>
     			</form>
    <!-- /.search -->
				
				<div class="submenubutton">
				<input class="btnpostproject" type="button" value="Post a Project "/>
				</div>
		</div>
		
		<div class="leftFilterbar">
		
					<!-- <a href="publicEnSearchp"><i class="icon-user"></i>Enterpreneurs </a> |  
					<a href="publicPmSearchp"><i class="icon-cog"></i>Project Managers</a> |
					<a href="publicCnSearchp"><i class="icon-remove"></i>Contributors</a>
		 			-->
		 
		 <ul class="menubar">
			
		 	<li class="headerline"><strong> Everyone </strong></li>
		 	<li> <a href="publicEnSearchp">Entrepreneurs</a> </li>
		 	<li> <a href="publicPmSearchp">Project Managers</a> </li>
		 	<li> <a href="publicCnSearchp">Contributors</a> </li>
		 	
		 	<br><br>
		 	<li class="headerline"> <strong>All Categories</strong> </li>
		 	<li> <a href="">Desktop Development</a></li>
		 	<li> <a href="">Web Development </a></li>
		 	<li> <a href="">Android Development </a></li>
		 	<li> <a href="">IOS Development </a></li>
		 	<br><br>
		 	
		 	<li class="headerline"><strong> All Users Location</strong> </li>
		 	<li> <a href="">Country </a> </li>
		 	<br><br>
		 	<li class="headerline"> <strong>All Feedback</strong> </li>
		 	<li> <a href="">Atleast 5</a>  </li>
		 	<li> <a href="">Atleast 4</a> </li>
		 	<li> <a href="">Atleast 3 </a> </li>
		 	<br><br>
		 	<li class="headerline"> <strong>Any Number of Reviews</strong> </li>
		 	<li> <a href="">Atleast 5 </a> </li>
		 	<li> <a href="">Atleast 10 </a> </li>
		 	<li> <a href="">Atleast 15  </a></li>
		 </ul>
		 
		 
		
		</div>
		
		<div class="mainUserBar">
		
				<div class="resultset">
					<p> All Free Lancers &nbsp; <span>( Results Count )</span></p> 
				</div>
				
				<div class="resultsetn">
					
					<span>Sort By</span> &nbsp;
						<div class="btn-group" role="group">
    				
    						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      						Default	
      						<span class="caret"></span>
    						</button>
	    					<ul class="dropdown-menu" role="menu">
      						<li><a href="#">Level (High)</a></li>
      						<li><a href="#">Level (Low)</a></li>
    						</ul>
  						</div>
				</div>
			
			
			

			
					<div class="persondetails">
						<s:iterator value="users">
						
								<div class="follow-ava">
                            	      <img src="public/img/thumbnail.jpg" alt="">
                                </div>
									<div class="userdetails">
									
									<!-- 
									<label>Userid </label>
									<s:property value="userid"/> -->
									
									<!-- <label>Fullname </label> 
									<s:property value="fullName" />-->
									
									<strong>
									<s:url id="viewPmpublic" action="viewPmpublic">
			             			<s:param name="userid" value="%{userid}"></s:param>
			             			<s:param name="username" value="%{username}"></s:param>
			            			 </s:url>			
									<s:a href="%{viewPmpublic}"><s:property value="fullName" /></s:a>
									</strong>
									
									<%-- <label>Username </label>
									<s:property value="username" /> --%>
									
									
									<br>
									<%-- <s:url id="viewPmpublic" action="viewPmpublic">
			             			<s:param name="userid" value="%{userid}"></s:param>
			             			<s:param name="username" value="%{username}"></s:param>
			            			 </s:url>			
									<s:a href="%{viewPmpublic}"><s:property value="username" /></s:a> --%>
									
									<!-- <label>UserType</label> -->
									<s:property value="usertype" />
									
									
			             			<span> &nbsp; | &nbsp; </span>
		
									<!-- <label>Country</label> -->
									<s:property value="country" />
									<span> &nbsp; | &nbsp;  Category &nbsp; | &nbsp; Jobs &nbsp; | &nbsp; Ratings &nbsp;</span>
									<%-- <label>Email</label>
									<s:property value="email" /> --%>
									<br>
									<p> Short Brief description of the user profile which will include a user brief introduction. <br>Short Brief description of the user profile</p>
									<span> Portfolio |  &nbsp; Skills </span>
									</div>
									
									<br><br>
						</s:iterator>
			
				   </div>
		
			
		
		</div>
		
		<!--  Functionality to be added later  -->
		<!-- 
		 	<div class="footerbottom">
		
			</div> -->
		

    

		
		
</body>
</html>