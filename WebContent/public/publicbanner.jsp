<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<title>Pro-Lutions :: Find Crowd Users</title>
			<link href="public/css/findcrowdusers.css" rel="stylesheet" type="text/css">
			<link rel="stylesheet" href="bootstrap/css/prolution-style.css"> 
			<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> 
			<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
 
 			<link rel="stylesheet" href="pm/css/jquery-ui.css">
	        <link rel="stylesheet" href="pm/css/style.css">
			
			<script src="pm/js/jquery-1.11.2.min.js"></script>
			<script src="pm/js/jquery-ui.js"></script>
			<script src="js/jquery.min.js"></script>
 			<script src="js/jRate.min.js"></script>
			<script type="text/javascript">

 		             			//alert("${users}");
 	 		             		
/*  $(document).ready(function(){
  		             			
$.each("${users}",function (i,u){
	alert(u);
}); 


		
		 $("#btnSearch").click(function(){

			 $("#frms").submit();
			 });

	$("#kRate").each(function(index, value) { 
	   // alert($(this).attr('id')); 
	});
	    // alert("say hello");
	    
	    $.each(users.user,function (i,u){

	alert(u.rating);
}); 

	function stars () {
			var that = this;
			//alert("${rating}");
			$("this").jRate({
				rating: 3,
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		}
}); */

  </script> 
</head>
<body>
		
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="newcontainer">
	
				<ul class="topmenunew">
					
					<li><a href="findwork">Find Work</a></li>
					<li><a href="crowduserSearch">Find Pro-Users</a></li>
					<li><a href="goHome #our-work">How it Works</a></li>
					<!-- <li><a href="projectSearch">Browse Projects</a></li> -->
					<li><a href="prelogin">LogIn</a> <span style="color:white;">or</span> <a href="preSignup">Signup</a></li>	
						
			
					
				</ul>
				
				
				
	
	</div>
	
	<div class="container">
				<div class="logo-wrapper">
					<a class="navbar-brand" href="goHome"> <!-- Pro<em>- Lution</em>-->
						<img class="projectlogo" src="bootstrap/img/ProLogo.png">

					</a>
				</div>

	</div>
	</nav>
	
	<div class="bodycontainer">
	
				<div class="navSearch">
		
		
						<!-- 		<p>Search Bar </p> -->
		
					<div class="submenulinks">
						<p>   <strong> <a href="crowduserSearch">Crowd Users</a></strong>  &nbsp;&nbsp; |&nbsp;&nbsp; <a href="searchedIdeas">Find Ideas</a> |&nbsp;&nbsp; <a href="viewTasks">Find Tasks</a>  </p>
					</div>	
				
				 		<!-- <form class="navbar-form" > -->
				 	<form id="frms" action="searchedUsers">
	    				<div class="input-group stylish-input-group">
        					<span class="input-group-addon">
            					<button id="btnSearch" type="submit"><span class="glyphicon glyphicon-search"></span></button>  
        					</span>
        						<input name="str" id="str" type="text" class="form-control input-sm searchbox" placeholder="Search">
    					</div>
	     			</form>
    						<!-- /.search -->
				 
						<div class="submenubutton">
							<span>Want to post a Job ?</span> &nbsp;<input class="btnpostproject" type="button" value="Post a Job "/>
						</div> 
				</div>
	
	</div>

		
</body>
</html>