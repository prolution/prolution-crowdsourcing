<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="bootstrap/css/prolution-style.css">
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">

        <script src="bootstrap/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body>
   
	<div class="cta">
        <div class="row"> 
          <div class="col-md-12">
            <h3 class="htxt">Where Ideas Meet Solutions</h3>
            <!--<span>Join us</span>-->
	    <span> have an account? </span> <s:form  action="prelogin"><s:submit value="SignIn" cssClass="btn" /></s:form><span> or </span><s:form action="preSignup"><s:submit  value="SignUp" cssClass="btn" /></s:form>
           						
          </div>
        </div>
    </div>

        <div class="row">
          <div class="first-section" >
            <div class="col-md-4 col-sm-12" id="our-work">
              
	      <h3>Be an <br><em>enterpreneur</em></h3>
              <br>
              <img class="first-icon" src="bootstrap/img/enterpreneur.png" alt="">
		
            </div>
            <div class="col-md-4 col-sm-12">
              <h3>Become A<br><em>Project Manager</em></h3>
              <img class="second-icon" src="bootstrap/img/projectmanager.png" alt="">
            </div> 	  
	    <div  class="col-md-4 col-sm-12">
              <h3>Contribute<br><em>Your Skills</em></h3>
              <img class="second-icon" src="bootstrap/img/contributor.png" alt="">
            </div> 
          </div>
        </div>


        <!--

        <div class="row">
          <div class="go-pro-btn">
            <div class="col-md-12 col-sm-12">
              <a href="#">PRO Level</a>
            </div>
          </div>
        </div> -->
        
        <div class="row">
          <div class="first-discription" id="who-we-are">
            <div class="col-md-4 col-md-offset-8 col-sm-12">
           <!--  <h6>Responsive Layout</h6> -->
           <!--  <p>Freshness is free HTML5 layout by <span class="blue">template</span><span class="green">mo</span>. You can modify and use this website template for any purpose. Credits go to <a rel="nofollow" href="http://www.smashingmagazine.com/2012/11/20/art-professions-icons-png/">Art Professions</a> Icon Set by Smashing Magazine and <a rel="nofollow" href="http://unsplash.com">Unsplash</a> for images used in this template.<br><br>Vestibulum sagittis, eros eget ullamcorper dictum, dolor orci imperdiet dui. Morbi ac leo hendrerit, iaculis felis porta, mattis est.mollis elit. Mauris euismod nullased mi ultrices, non tincidunt ex sodal.</p> -->
            </div>
          </div>
        </div>
        
        <div class="row">
        
          <div class="big-icon hidden-xs">
            <div class="col-md-12 col-sm-12">
             <!-- <img src="img/big-icon.png" alt=""> -->
            </div>
          </div>
          
          <div class="small-icon visible-xs">
            <div class="col-md-12 col-sm-12">
             <!-- <img src="img/small-icon.png" alt=""> -->
            </div>
          </div>
          
        </div>
        
        <div class="row">
          <div class="second-discription">
            <div class="col-md-4 col-sm-12">
             <!-- <h6>Mobile Friendly</h6> -->
             <!-- <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus semper varius lacinia. Vestibulum sagittis, eros eget ullamcorper dictum, dolor orci imperdiet dui, et feugiat lorem s.<br><br>Morbi ac leo hendrerit, iaculis felis porta, mattis est.mollis elit. Mauris euismod nullased mi ultrices, non tincidunt ex sodal.</p> -->
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="small-icons">
            <div class="col-md-10 col-sm-12 col-md-offset-1">
              <ul>
                <li class="icon-hover">
                  <!-- <img class="first-small" src="img/first-small-icon.png" alt=""> -->
                  <!-- <h4>First Step</h4> -->
                </li>
                <li class="icon-hover">
                  <!-- <img class="first-small" src="img/second-small-icon.png" alt=""> -->
                  <!-- <h4>Second Step</h4> -->
                </li>
                <li class="icon-hover">
                  <!-- <img class="first-small" src="img/third-small-icon.png" alt=""> -->
                  <!-- <h4>Third Step</h4> -->
                </li>
                <li class="icon-hover">
                  <!-- <img class="first-small" src="img/fourth-small-icon.png" alt=""> -->
                  <!-- <h4>Fourth Step</h4> -->
                </li>
              </ul>
            </div>
          </div>
        </div>
        
        <div class="row">
        
          <div class="contact-us" id="contact">
          
            <div class="col-md-6 col-sm-12">
              <form class="contact-form" action="#submit-form" method="post">
           <!--     <h1>Contact Us</h1>
                <label>
                    <input id="name" type="text" name="name" placeholder="Name..." />
                </label>
                
                <label>
                    <input id="email" type="email" name="email" placeholder="Email..." />
                </label>
                
                <label>
                    <input id="subject" type="text" name="subject" placeholder="Subject..." />
                </label>
                
                <label>
                    <textarea id="message" name="message" placeholder="Message..."></textarea>
                </label>   
                 <label>
                    <input name="Submit" type="submit" class="button" value="Send Message" /> 
                </label>    -->
              </form>
            </div>
            
          <!--
	    <div class="map">
              <div class="col-md-6 col-sm-12">
                <img src="img/map.png" title="Our Location" alt="Where We Are">
              </div>
            </div><!-- map END -->
           


          </div><!-- contact END -->
          
        </div><!-- row END -->
        
      </div><!-- container END -->
      
    </div><!-- main content END -->
     <script src="bootstrap/js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="bootstrap/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="bootstrap/js/vendor/bootstrap.js"></script>
        <script src="bootstrap/js/main.js"></script>
</body>
</html>