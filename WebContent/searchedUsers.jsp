<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css"
	media="screen" />

</head>
<body>
	
	<h3>Available Users</h3>
			<label> </label>
		<div class="tab_container">
			<div id="tab1" class="tab_content">
				<table class="tablesorter" cellspacing="0">
					<thead>
						<tr>

							<th align="left">firstname</th>
							<th>lastname</th>
							<th>country</th>
							<th>email</th>
							<th>username</th>
						</tr>
					</thead>
					<s:iterator value="users">
							<tr>
								<td><s:property value="firstname" /></td>
								<td><s:property value="lastname" /></td>
								<td><s:property value="country" /></td>
								<td><s:property value="email" /></td>
								<td><s:property value="username" /></td>
							</tr>
					</s:iterator>
				</table>
			</div>

		</div>
		<div class="clear"></div>
</body>
</html>