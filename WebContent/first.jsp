<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Login Form</title>

  		<!-- <link rel="stylesheet" href="bootstrap/css/signin.css"> -->
  
		 <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">
		 <!-- <link rel="stylesheet" href="bootstrap/css/prolution-style.css">  -->
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">

        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
		<!-- <link rel="stylesheet" href="bootstrap/css/dropdownstyle.css" type="text/css" media="screen"/>
		
		<link rel="stylesheet" href="bootstrap/css/slidercss.css" type="text/css" media="screen"/> -->
   			<link rel="stylesheet" href="bootstrap/css/login.css">
  
  <!-- new file addition to the code  -->
<!-- code for popup -->
<%-- <script type="text/javascript" src="bootstrap/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="bootstrap/js/jquery.leanModal.min.js"></script>
<link type="text/css" rel="stylesheet" href="bootstrap/css/loginpopup.css" />
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css"> --%>
<!-- code for popup -->


</head>


<body onload="window.history.forward(1);">
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>

			<nav class="navbar navbar-default">
				
					<div>
						<a href="goHome"> <!-- Pro<em>- Lution</em>-->
							<img class="projectlogodesign" src="bootstrap/img/ProLogo.png">
						</a>
					</div>

					
				
			</nav>
	 			<hr/> 
			<div class="toplabel">
				<h2 class="headertext" id="topmsg"> Welcome to Pro-Lutions</h2>
				<h4 class="headertext"> Platform For Crowd Sourcing</h4>
			</div>
			<div class="picturesession">
			<img class="siginbg" src="bootstrap/img/signinbg.png">
			</div>

<div class="verticalLine">
 <div class="formdetails">
		<h3 class="headertext" id="topmsg">  Sign In </h3>
		<s:form method="post" action="login">
				<s:actionerror />
				
				<%-- 
        		<p><s:textfield cssClass="form-control" name="username" required="true" placeholder="Username or Email"></s:textfield></p>
        		
        		
        		<p><s:password  cssClass="form-control" name="upassword" required="true" label="Password" ></s:password></p> --%>        
        		<div class="logindetails">
        		<label id="ulabel">Enter Username</label>
        		<input class="form-control" type="text" name="username" required="required" placeholder="Username"/>
        		
        		<label>Enter Password</label>
        		<input class="form-control" type="password" name="upassword" required="required" placeholder="****"/>
        		</div>
        		<br><span class="forgot"><a href="#">Forgot ?</a></span>
        		<p class="remember_me">
          				<label>
            			<input type="checkbox" name="remember_me" id="remember_me">
						Keep me Signed In  [?]
          				</label>
          				</p>
        		<p class="submit"><s:submit cssClass="btn btn-success" value="Sign In" ><i class="glyphicon glyphicon-lock"></i></s:submit>
        		
        		 </p>
      		
			<%--<s:form method="post" action="rememberusername">
      					<p class="remember_me">
          				<label>
            			<input type="checkbox" name="remember_me" id="remember_me">
						Remember me on this computer
          				</label>
          				</p>
			</s:form> --%>
      		</s:form>
			
			<hr class="signbottom"/>     
			
			<p>Not a Member Yet ?</p>
			<h2 class="headertext" id="topmsg"> Create An Account ?</h2>
			<a href="preSignup" class="btn btn-info"> 
			 Continue <span class="glyphicon glyphicon-circle-arrow-right"></span>
			</a>
			
			 
	</div>
</div>


  	<%-- <div class="incontainer">
    	<div class="login">
      		<h3 align="left"><strong>L o g i n </strong></h3>
      		
      		<s:form method="post" action="login">
				<s:actionerror />
        		<p><s:textfield name="username" required="true" label="Username" placeholder="Username or Email"></s:textfield></p>
        		<p><s:password name="upassword" required="true" label="Password" ></s:password></p>        
        		<p class="submit"><s:submit value="Login" ></s:submit> </p>
      		</s:form>
			
			<s:form method="post" action="rememberusername">
      			<p class="remember_me">
          				<label>
            			<input type="checkbox" name="remember_me" id="remember_me">
						Remember me on this computer
          				</label>
          				</p>
			</s:form>        		
		</div>

    	<div class="login-help">
      		<p>Forgot your password? <a href="#">Click here to reset it</a>.</p>
    	</div>
  	
  	
  	</div>
 --%>  	
  	
  	<!-- new code integration for the signin  -->
  	
  	<!-- code for popup -->
  	
  	<%-- <div class="container">

	<a id="modal_trigger" href="#modal" class="btn">Click here to Login or register</a>

	<div id="modal" class="popupContainer" style="display:none;">
		<header class="popupHeader">
			<span class="header_title">Login</span>
			<span class="modal_close"><i class="fa fa-times"></i></span>
		</header>
		
		<section class="popupBody">
			<!-- Social Login -->
			


			<!-- Username & Password Login form -->
			<div class="user_login">
				<form>
					<label>Email / Username</label>
					<input type="text" />
					<br />

					<label>Password</label>
					<input type="password" />
					<br />

					<div class="checkbox">
						<input id="remember" type="checkbox" />
						<label for="remember">Remember me on this computer</label>
					</div>

					<div class="action_btns">
						<!-- <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div> -->
						<div class="one_half last"><a href="#" class="btn btn_red">Login</a></div>
					</div>
				</form>

				<a href="#" class="forgot_password">Forgot password?</a>
			</div>

		</section>
	</div>
</div> --%>
<!-- 
<script type="text/javascript">
	$("#modal_trigger").leanModal({top : 200, overlay : 0.6, closeButton: ".modal_close" });

	$(function(){
		// Calling Login Form
		$("#modal_trigger").click(function(){
			
			$(".user_login").show();
			return false;
		});

		/* // Calling Register Form 
		$("#register_form").click(function(){
			$(".social_login").hide();
			$(".user_register").show();
			$(".header_title").text('Register');
			return false;
		}); 

		// Going back to Social Forms
		$(".back_btn").click(function(){
			$(".user_login").show();
			$(".user_register").hide();
			$(".social_login").hide();
			$(".header_title").text('Login');
			return false;
		});
 */
	})
</script>
  	 -->
  	<!-- code for popup -->


</body>
</html>