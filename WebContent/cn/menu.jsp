<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cn</title>
	
</head>
<body>
<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
%>

<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper"> 
		<form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>

		<h3>Profile</h3>
            
           
            <ul class="sidebar-nav">
				
			<li class="icn_categories">
					<s:a action="myCnProfile">My Profile
					<s:param name="userid" value="#session.userid" />
					</s:a>
			</li>
			</ul>
		<h3>Tasks</h3>
            <ul class="sidebar-nav">
		   
		   <li class="icn_new_article">
 					<s:a action="myTasks">My Tasks
					<s:param name="taskOwner" value="#session.userid" />
					</s:a>
 			</li> 

<li class="icn_edit_article">
 					<s:a action="allTasks">All Tasks
					<s:param name="bidsUser" value="#session.userid" />
					</s:a>
 </li>
 	
 		
 		<li class="icn_categories"> 
			<s:a action="findusersCn">Find Users
					<s:param name="userid" value="#session.userid" />
					</s:a>
			</li>
			
			
			<li class="icn_categories"> 		
			<s:a action="cnBids">My Bids
					<s:param name="bidsUser" value="#session.userid" />
					</s:a>
			</li>
 		
 		
 </ul>     			
 </div>
 </div>			


</body>
</html>