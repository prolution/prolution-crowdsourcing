<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                        <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

		<link href="pm/css/myProject.css" rel="stylesheet" type="text/css">
	    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
		
<link href="pm/css/style.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet"> 

</head>
<body>



	<%
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
	%> 
	
	<div class="leftpan">
			<div class="sidesublinks">
				<ul>
					<li><strong><a href="#"> All Tasks </a></strong> </li>
					<li><a href="#"> Pending Ideas</a></li>
					<li><a href="#"> Completed Ideas </a></li>
					
				</ul>
			</div>
	</div>
	<div class="rightpan">
		
			<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											<th align="left">Project Name</th>
								<th align="left">Project Description</th>
								<th>Domain</th>
								<th>Posted on</th>
								<th>Budget</th>
								<th>Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    	
										<s:iterator value="tasks">
	
	<tr>
	<td>
		<s:property value="taskTitle"/>
		</td>
			<td>
		
		<s:property value="taskBudget"/>		
					</td>			
			<td>		
		<s:property value="taskDuration"/>
				</td>		
			<td>		
		<s:property value="taskDomain"/>	
				</td>		
			<td>		
		<s:property value="taskPostedOn"/> 	
				</td>		
			<td>		
		<s:property value="taskDescrption"/> 
			</td>
			<td><s:form action="myTask">
										<s:hidden name="taskID" value="%{taskID}"></s:hidden>
										<s:submit cssClass="btn btn-success" value="Open"></s:submit>
									</s:form></td>	
			</tr>	 
</s:iterator>  
                                    
                                    
                                </tbody>
                            </table>
                        </div>
		<div class="contain">
		<div class="gantt"></div>
	</div>
	
	</div>
	




				<script src="pm/js/jquery.min.js"></script>

	<script src="pm/js/jquery.fn.gantt.js"></script>
	<script src="pm/js/jquery-te-1.4.0.min.js"></script>

	<script type="text/javascript">
	var src = [];

	
	</script>
	<s:iterator value="tasks">
		<script>
			$(function() {

				var sdate = new Date("${taskPostedOn}");
				var smilli = sdate.getTime();

				var ldate = new Date("${taskDuration}");
				var lmilli = ldate.getTime();

				src.push({
					name : "${taskTitle}",
					desc : "${taskDescrption}",
					values : [ {
						from : "/Date(" + smilli + ")/",
						to : "/Date(" + lmilli + ")/",
						label : "${taskDomain}",
						customClass : "ganttBlue"
					} ]

				});
			});
		</script>
	</s:iterator>
	<script>
		$(function() {
			"use strict";
			$(".gantt").gantt({
				source : src,
				navigate : "scroll",
				maxScale : "hours",
				itemsPerPage : 10
			});
			prettyPrint();

		});
	</script>
</body>
</html>