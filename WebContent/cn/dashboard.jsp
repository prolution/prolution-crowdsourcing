<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>



<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>Dashboard</title>
	
	 <link href="cn/css/dashboardcn.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet"> 

	</head>


<body>

<div class="leftpan">

</div>
<div class="centerpan">
					
					<div class="Midpan">
						<%-- <div class="col-lg-12">
                        	<div class="alert alert-info alert-dismissable">
                            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            	<i class="fa fa-info-circle"></i>  <strong>You Have Some Pending Requests</strong> Click <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">Check Notfications</a>
                        	</div>
                    	</div> --%>
                    	<div class="textstyle">
                    	
                    		<h4 class="subheaderhome">  
                    			<strong> <s:a action="#">My Jobs 
										<s:param name="#" value="#session.userid" />
										</s:a>  
								</strong>   | <s:a action="#">Opportunities
										  <s:param name="#" value="#session.userid" />
										  </s:a> 
							</h4>
                    	
								 
						
                    	</div>
                    	
                    	<div class="notifyticker">
                    		
                    		<i class="fa fa-info-circle"></i><strong> There are Thousands of Jobs waiting for you</strong>
                    		<ul>
                    			<li> Find Great Opportunities </li>
                    			<li> Propose Bids on Tasks</li>
                    			<li> Earn The Way you Want </li>
                    		</ul>
                    	
                    	</div>
                    	
                    	<div class="tabbar">
                    	
						<div>
						
									<div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                              	      <tr>
											<th>Task Title</th>
											<!-- <th>Project Description</th> -->
											<th>Submission Deadline</th>
											<th>Bid Review</th>
											
											
    										
                                    </tr>
                                </thead>
                                <tbody>
                                    	<tr>
											</tr>
										
										  
                                    
                                    
                                </tbody>
                            </table>
                        </div>
									
						
						
						
						</div>
			

					</div>
					<div class="rightpan">
							<div class="messagesblock">
										<h3>Inbox</h3>
										<hr>
									<ul>
										
										<li>Action Required</li>
										<li>Messages</li>
										<li>Invites</li>
										<li>All</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							<div class="idea">
							<div class="messagesblock">
										<h3>Tasks</h3>
										<hr>
									<ul>
										
										<li>No.of Tasks</li>
										<li>Completed Tasks</li>
										<li>Pending Tasks</li>
										<li>Completed Tasks</li>
									</ul>
									<ul class="values">
										<li>0</li>
										<li>0</li>
										<li>0</li>
										<li>0</li>
									</ul>
									
							</div>
							</div>
							
					</div>
</div>
                    <div>
	
	
	
</div>

</body>

</html>








