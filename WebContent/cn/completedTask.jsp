<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                    <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 		<link rel="stylesheet" href="pm/css/bidsbyme.css">
	
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
	<link href="en/css/bootstrap.min.css" rel="stylesheet">
		
        	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});
	</script>
</head>
<body>
	
	
	


<div class="mybidform">
		
			<div class="headertext">
			<h2> Completed Task </h2>
			</div>
			
			<div class="mybidleftpan">
					<div class="tabbar">
							<%-- <div class="sortdesc">
								<label> Sort By </label> &nbsp; 
								<span>
									<select>
										<option>By name</option>
										<option>By Amount</option>
									</select>
								</span>
							</div> --%>
							<!-- <div class="verticalline">
							</div> -->
					</div>
					 
					 
					 
   					 
					 		<div class="tabledata">
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
										
											<!-- <th> username</th> -->
											<th>Task Owner</th>
											<!-- 	<th>lastname</th> -->
											<th>country</th>
											<th>Rating</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    	
									<tr>
										<%-- <td>
											<s:property value="%{user.username}"/>
										</td> --%>
										<td>
											<s:property value="%{user.firstname}"/>&nbsp;<s:property value="%{user.lastname}"/>		
										</td>			
										
										<td>		
											<s:property value="%{user.country}"/>	
										</td>	
										<td>
											<div  id="kRate" style="height:10px;width: 40px;"></div>
										</td>
                   					</tr>	 
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
    				 
    				 
    				 
    				
    				
					
					
			</div>
		 	<div class="mybidrightpan">
		 	
		 			<div class="statshead">
		 				<h4 class="hinvite">Task Title</h4>
		 				 <h4><s:property value="%{task.taskTitle}"/></h4>  
		 				<ul>
		 					<li>Description<p><s:property value="%{task.taskDescrption}"/></p></li>
		 					<li>Duration<p><s:property value="%{task.taskDuration}"/></p></li>
		 					<li>Budget <p><s:property value="%{task.taskBudget}"/></p></li>
		 					
		 					
		 					
		 				</ul>
		 				
		 			</div>
		 			
		 			<div class="note">
		 			<p>Overview of the task </p>
		 			
		 			</div>
			
			</div>
		
			 </div>
			
			<hr>

			<div> 
			 <h4> Copyrights @2015 - Pro-Lutions.com.pk</h4>
			</div>











<!-- new code finished here  -->
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<%-- 
	
	
	
	<h3>The Completed Task</h3>
	
			<s:form action="updateProj">
				<s:textfield label="Task Name" value="%{task.taskTitle}" name="taskTitle"></s:textfield>
				<s:textfield label="Task Cost" name="taskBudget" value="%{task.taskBudget}"></s:textfield>
				<s:textfield label="Task duration" name="taskDuration" value="%{task.taskDuration}"></s:textfield>
				<s:textfield label="Task description" name="taskDescrption" value="%{task.taskDescrption}"></s:textfield>
										<s:submit value="Update" ></s:submit>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden>

			</s:form>
	
	
          <br>
          	<br>
          		
	<h3>Manager of Task</h3>
	
	
				<!-- <div id="tab1" class="tab_content"> -->
					<table class="tablesorter" >
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Rating</th>

							</tr>
						</thead>
							<tr>
								<td>
									<s:property value="%{user.username}"/>
								</td>
								<td>
									<s:property value="%{user.firstname}"/>		
								</td>			
								<td>		
									<s:property value="%{user.lastname}"/>
								</td>		
								<td>		
									<s:property value="%{user.country}"/>	
								</td>		
								<td>
									<div  id="kRate" style="height:10px;width: 40px;"></div>
								</td>
                   			</tr>	 
                   	</table>
 --%>                        
</body>
</html>