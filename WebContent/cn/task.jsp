<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                    <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
	<link href="pm/css/availableideas.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
	 
        	<script src="js/jquery.min.js"></script>
	<script src="js/jRate.min.js"></script>
	<script type="text/javascript">
		$(function () {
			var that = this;
			//alert("${rating}");
			$("#kRate").jRate({
				rating: "${rating}",
				strokeColor: 'black',
				width: 20,
				height: 20,						
				readOnly:"true" 
				
			});
			
		});
	</script>
</head>
<body>

		<!-- new code for the session begins here -->




<div class="navSearch">
		
		
		
		
					<div class="submenulinks">
								<p>
							   	 			<s:a action="#">Find Users
											<s:param name="userid" value="#session.userid" />
											</s:a>
								 			 &nbsp;&nbsp; |&nbsp;&nbsp; 
								<strong>			<s:a action="#">Find Tasks
											<s:param name="bidsUser" value="#session.userid" />
											</s:a>
								</strong>   
								</p>
					</div>	
				
					
		</div>

		<div class="maincontainer">
		
				<div class="leftcontainer">
							
							<div class="sideheadings">
								 <h3> Task Domain </h3>
								 <ul>
								 	<li><a href="#">Desktop Development</a></li>
								 	<li><a href="#">Android Development</a></li>
								 	<li><a href="#">Web Development</a></li>
								 	<li><a href="#">IOS Development</a></li>
								 </ul>
							</div>
				</div>
				<div class="rightcontainer">
				
							<div class="ideadescription">
								<h2>Task Summary</h2>
								<h3 class="heading"> &nbsp;&nbsp; <s:property value="%{task.taskTitle}" /></h3>
								<p>Description:<s:property value="%{task.taskDescrption}" /></p>
								<p>Domain:<s:property value="%{task.ideaDomain}" /></p>
								<p>Posted On:<s:property value="%{task.taskPostedOn}" /></p>
								<p>Budget:<s:property value="%{task.taskBudget}" /></p>
								<p>Completion Date:	<s:property value="%{task.taskDuration}" /></p>
								<hr>
							</div>
								
							<div class="biddingbox">	
								<s:form action="bidsTask">
									<s:actionerror />
									<input type="hidden" value="<%=session.getAttribute("userid")%>" name="bidsUser" />
									<label id="bidlabel">Enter Bid Amount &nbsp;</label>
									<input  type="number" id="bidlabel" name="bidsAmount" min="1" required="required"/>
									<%-- <s:textfield id="bidlabel" name="bidsAmount" min="5" required="required" ></s:textfield> --%>
									<br>
									<s:hidden type="hidden" value="%{task.taskID}" name="bidsTask" />
									<input type="hidden" value="1" name="bidsFlag" />
									
									<s:submit cssClass="btn btn-success" value="bids"></s:submit>
								</s:form>
								<%-- <s:form action="bidsTask">
                   						<s:textfield name="bidsAmount" label="bids Amount" ></s:textfield>
										<input type="hidden" value="<%=session.getAttribute("userid")%>" name="bidsUser" />
										<s:hidden type="hidden" value="%{task.taskID}" name="bidsTask" />
										<input type="hidden" value="1" name="bidsFlag" />
									<s:submit value="bids">
									</s:submit>
								</s:form>      	 --%>
							</div>
						<div class="rightsubcontainer">
						
										
										<div class="userdetail">
											<h3> Idea Posted By</h3>
											<div class="igbox">
											</div>					
											<p>
												<strong>
													<s:url id="viewPmpublic" action="viewPmpublic">
			             							<s:param name="userid" value="%{user.userid}"></s:param>
			             							<s:param name="username" value="%{user.username}"></s:param>
			            			 				</s:url>			
													<s:a href="%{viewPmpublic}"><s:property value="%{user.firstname}"/> &nbsp;<s:property value="%{user.lastname}"/></s:a>
												</strong>	
											</p>
											<%-- <s:property value="%{user.username}"/> --%>
											<p><s:property value="%{user.country}"/></p>
											
											
									
											<div  id="kRate" style="height:10px;width: 40px;"></div>	
										</div>
						</div>			
						
				</div>
		</div>


















		<!-- new code for the session ends here -->












<%-- 




	<div class="module_content">
		<article class="stats_graph">
				<header>
	<h3>The Task</h3>
	</header>
		<div class="form-group">
			<s:form action="updateProj">
				<s:textfield label="Task Name" value="%{task.taskTitle}" name="taskTitle"></s:textfield>
				<s:textfield label="Task Cost" name="taskBudget" value="%{task.taskBudget}"></s:textfield>
				<s:textfield label="Task duration" name="taskDuration" value="%{task.taskDuration}"></s:textfield>
				<s:textfield label="Task description" name="taskDescrption" value="%{task.taskDescrption}"></s:textfield>
										<s:submit value="Update" ></s:submit>
				<input type="hidden" class="form-control"
					value="<%=session.getAttribute("userid")%>" name="proOwner" />
				<s:hidden name="proID" value="%{idea.ideaId}"></s:hidden>

			</s:form>
	
	<div class="module_content">
		<article class="stats_graph">
		<div class="form-group">
			<br>
		</div>
	
  <br>
          	<br>
          		<header>
	<h3>Manager of Task</h3>
	</header>
						<div  id="kRate" style="height:10px;width: 40px;"></div>
	
				<div id="tab1" class="tab_content">
					<table class="tablesorter" cellspacing="0">
						<thead>
							<tr>
								<th align="left"> username</th>
								<th align="left">firstname</th>
								<th>lastname</th>
								<th>country</th>
								<th>Rating</th>

							</tr>
						</thead>
<tr>
	<td>
		<s:property value="%{user.username}"/>
		</td>
			<td>
		<s:property value="%{user.firstname}"/>		
				</td>			
			<td>		
		<s:property value="%{user.lastname}"/>
				</td>		
			<td>		
		<s:property value="%{user.country}"/>	
				</td>		
                   			</tr>	 
                   </table>
                   			<s:form action="bidsTask">
                   						<s:textfield name="bidsAmount" label="bids Amount" ></s:textfield>
										<input type="hidden" value="<%=session.getAttribute("userid")%>" name="bidsUser" />
										<s:hidden type="hidden" value="%{task.taskID}" name="bidsTask" />
										<input type="hidden" value="1" name="bidsFlag" />
									<s:submit value="bids">
									</s:submit>
								</s:form>                  
 --%>
 </body>
</html>