<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/struts-tags" prefix="s"%>
 


<!doctype html>
<html>

<head>
	<meta charset="utf-8"/>
	<title>Dash board(Contributor)</title>


	
	
		 <link href="pm/css/pmbanner.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="pm/css/dropdownstyle.css" type="text/css" media="screen"/>
	<link href="pm/css/topdropdown.css" rel="stylesheet" type="text/css">
	
	
	 <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">   
     <link href="en/css/bootstrap.min.css" rel="stylesheet">    
    <link href="en/css/sb-admin.css" rel="stylesheet">     
	<link href="en/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
    <!-- Custom CSS -->
	
	
	
	
	
	
	<!-- 
		<link rel="stylesheet" href="en/css/layout.css" type="text/css"/> 
	    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"> 
    	<link href="en/css/bootstrap.min.css" rel="stylesheet">
		<link href="en/css/sb-admin.css" rel="stylesheet">
		<link href="en/css/morris.css" rel="stylesheet">
    	<link href="en/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="pm/css/pmbanner.css" rel="stylesheet" type="text/css"> -->

        
        
	</head>


<body>

			<nav class="navbar navbar-default">
				
					<div>
						 <a href="#"> 
							<img class="homelogo" src="bootstrap/img/ProLogo.png"/>
						 </a>  
					</div>

			<div class="topnavitems">
			<ul class="nav navbar-right top-nav">
				<li class="dropdown">
					<a href="#"> Contributor </a> 
					
				</li>
				
            	<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${sessionScope.username} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                         	<s:form action="logout">
                         	<a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                         	
                            <!-- <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a> -->
                            </s:form>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                
                
			</ul>
					
				</div>
			</nav>
	


<!--  end of new code settings -->





<nav id="nav">
	<ul id="navigation">
					
		<li>
		
			<s:url id="dashboard" action="navigatetocn">
			<s:param name="userid" value="%{userid}"></s:param>
			</s:url>
			<s:a href="%{dashboard}" class="first">
			
			<i class="glyphicon glyphicon-home"></i>&nbsp;My-Pro</s:a>
			
		</li>
		
		<li><a href="#">Hire &nbsp; &raquo;</a>
			<ul>
				
				<li> 
					<s:a action="findusersCn">Find Users
					<s:param name="userid" value="#session.userid" />
					</s:a>
				</li>
				<li>
					<s:a action="cnBids">My Bids
					<s:param name="bidsUser" value="#session.userid" />
					</s:a>
				</li>
			</ul>
		</li>
		<li><a href="#">Tasks &raquo;</a>
			<ul>
				<li>
					<%-- <s:a action="ideasAvailable">Projects Available
						<s:param name="bidsUser" value="#session.userid" />
					</s:a> --%>
					<s:a action="myTasks">My Tasks
					<s:param name="taskOwner" value="#session.userid" />
					</s:a>
				</li>
				<li>
					<s:a action="allTasks">All Tasks
					<s:param name="bidsUser" value="#session.userid" />
					</s:a>
					
					<ul>
						<li><a href="#"> Assigned</a></li>
						<li><a href="#"> Pending</a></li>
						<li><a href="#"> Completed</a></li>
					</ul>
				</li>
				
			</ul>				
		</li>
		<li><a href="#">Manage &raquo;</a>
			<ul>
				
				<li><a href="#">My Team &raquo;</a>
					<ul>
						<li><a href="#">Test</a></li>
					</ul>						
				</li>
				<li>
		   			<s:url id="lockscreen" action="lockscreen">
		   			<!--<s:property value="#session.userid"/>-->
		   			<s:param name="userid" value="#session.userid"></s:param>
					</s:url> <s:a href="%{lockscreen}">Lock Screen</s:a>
				</li>
				<li>
					<s:form action="logout">
                         	<a href="logout"> Log Out</a>
					 </s:form>
				</li>
				
				
			</ul>
		</li>
		<li>
					<s:a action="myCnProfile">My Profile
					<s:param name="userid" value="#session.userid" />
					</s:a>
					
		</li>
		<li><a href="#" class="last">Account</a></li>
	</ul>
</nav>

 <!-- mid bar -->
			<div class="searchbox">
				<form id="ui_element" class="sb_wrapper">
                    <p>
						<span class="sb_down"></span>
						<input class="sb_input" type="text" placeholder="Search"/>
						<input class="sb_search" type="submit" value="Go"/>
						
					</p>
					<ul class="sb_dropdown" style="display:none;">
						<li class="sb_filter">Filter your search</li>
						<%-- <li><input type="checkbox"/><label for="all"><strong>All Categories</strong></label></li> --%>
						<li><input type="checkbox"/><label for="Enterpreneurs">Jobs</label></li>
						<li><input type="checkbox"/><label for="Projectmanagers">Crowd Users</label></li>
						<!-- <li><input type="checkbox"/><label for="Contributors">Contributors</label></li> -->
						
						
					</ul>
            	</form>
			</div>

<div class="topnavigationbg">

<!-- <input class="btnpostproject" type="button" value="Post a Project "/> -->
</div>




	
	
	
	<!-- Start of secondary bar  -->
	<%-- <section id="secondary_bar">
		<div class="user">
				
		
			
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs">
							<s:url id="dashboard" action="navigatetopm">
								<s:param name="userid" value="%{userid}"></s:param>
							</s:url> <s:a href="%{dashboard}">Project Manager</s:a>
			
			<!-- <a href="dashboardPm.jsp">Enterpreneur</a> -->
			 
			<div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
							
				 		<div class="headerlinks">
				 			<p id="hd"> 
				 				<a href="#"> Bid Requests <span class="badge">1</span></a> 
				 				<a href="#"> Messages <span class="badge">1</span></a>
				 				<a href="#"> Deadlines <span class="badge">1</span></a> 
				 				<button class="btn btn-primary" type="button">
					  				Bids Requests <span class="badge">5</span>
								</button>
				 				<button class="btn btn-primary" type="button">
					  				Deadlines <span class="badge">0</span>
								</button>
						 		<button class="btn btn-primary" type="button">
					  				Messages <span class="badge">4</span>
								</button>
								<button class="btn btn-primary" type="button">
					  				Account Settings
								</button>
				 			
				 			</p> 
				 		</div>
				 
				 					
		</div>
	</section>
	 --%>
	<!-- end of secondary bar -->



















    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
        <script type="text/javascript">
            $(function() {
				/**
				* the element
				*/
				var $ui 		= $('#ui_element');
				
				/**
				* on focus and on click display the dropdown, 
				* and change the arrow image
				*/
				$ui.find('.sb_input').bind('focus click',function(){
					$ui.find('.sb_down')
					   .addClass('sb_up')
					   .removeClass('sb_down')
					   .andSelf()
					   .find('.sb_dropdown')
					   .show();
				});
				
				/**
				* on mouse leave hide the dropdown, 
				* and change the arrow image
				*/
				$ui.bind('mouseleave',function(){
					$ui.find('.sb_up')
					   .addClass('sb_down')
					   .removeClass('sb_up')
					   .andSelf()
					   .find('.sb_dropdown')
					   .hide();
				});
				
				/**
				* selecting all checkboxes
				*/
				$ui.find('.sb_dropdown').find('label[for="all"]').prev().bind('click',function(){
					$(this).parent().siblings().find(':checkbox').attr('checked',this.checked).attr('disabled',this.checked);
				});
            });
        </script>
<!--  new javascript for the dropdown search ends here  -->

<script src="en/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="en/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="en/js/raphael.min.js"></script>
    <script src="en/js/morris.min.js"></script>
    <script src="en/js/morris-data.js"></script>

</body>

</html>