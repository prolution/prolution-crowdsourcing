<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tasks Available</title>
<!-- <link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" /> -->
        <!-- <link rel="stylesheet" href="pm/css/style.css"> -->
        		<link href="pm/css/availableideas.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
		<link href="en/css/bootstrap.min.css" rel="stylesheet">
        
        
          <link rel="stylesheet" href="pm/css/jquery-ui.css">
          <link rel="stylesheet" href="css/demo_page.css">
          <link rel="stylesheet" href="css/demo_table_jui.css">
          <link rel="stylesheet" href="css/dataTables.css">
     
			<%-- <script src="pm/js/jquery-1.11.2.min.js"></script> --%>
 			<script src="js/jquery.min.js"></script>
			<script src="js/jquery.dataTables.js"></script>
  			<script src="pm/js/jquery-ui.js"></script>
  			<script type="text/javascript">
		
	$(document).ready(function(){
		
	 $("#jqueryDataTable").dataTable({
         "columnDefs": [
{ className: "hidden", "targets": [ 0 ] }
                        
                    ],
         "sPaginationType" : "full_numbers",
             "bProcessing" : false,
             "bServerSide" : false,
             "sAjaxSource" : "AvailableTaskTable",
             "bJQueryUI" : true,
             "aoColumns" : [

                            { "mData": "taskID" },
                            
         { "mData": "taskTitle" },

         { "mData": "taskDescrption" },
         { "mData": "taskDomain" },
         { "mData": "taskPostedOn" },
         { "mData": "taskBudget" },
         { "mData": "taskDuration" }         
     ]
 } );
	
	
	 
	$('#jqueryDataTable tbody').on( 'click', 'tr', function () {
        //$(this).toggleClass('selected');
              // var firstcell = $(this).children('td:first');
                          $(this).toggleClass('selected');
              
               var name=$(this).find("td.hidden");
               $("#taskID").val(parseInt(name.text()));
               $("#frmTaskPoster").submit();
               //var name = $('td', this).eq().prev().text();
	 });
	});

  </script>
</head>
<body>
	
	
	<div class="navSearch">
		
		
		
		
					<div class="submenulinks">
								<p>
							   	 			<s:a action="#">Find Users
											<s:param name="userid" value="#session.userid" />
											</s:a>
								 			 &nbsp;&nbsp; |&nbsp;&nbsp; 
								<strong>			<s:a action="#">Find Tasks
											<s:param name="bidsUser" value="#session.userid" />
											</s:a>
								</strong>   
								</p>
					</div>	
				
		</div>

		<div class="maincontainer">
		
				<div class="leftcontainer">
							
							<div class="sideheadings">
								 <h3> Task Domain </h3>
								 <ul>
								 	<li><a href="#">Desktop Development</a></li>
								 	<li><a href="#">Android Development</a></li>
								 	<li><a href="#">Web Development</a></li>
								 	<li><a href="#">IOS Development</a></li>
								 </ul>
							</div>
				</div>
				<div class="rightcontainer">
				
				
							<h3 class="heading"> &nbsp;&nbsp; ' Search New Tasks '</h3>
		
		
	
							<div id="container">		
						<div id="demo_jui">
        					<table id="jqueryDataTable" class="display jqueryDataTable">
        					<thead>
        						<tr>
    								<th  align="left">ideaId</th> 
                					<th  >Idea Title </th> 
									<th>Description</th> 
    								<th>Domain</th> 
    								<th>Posted on</th> 
    								<th>Budget</th>
    								<th>Duration</th>
        						</tr>
        					</thead>
        					<tbody>
        					</tbody>
        					</table>
						</div>
					</div>
				<form id="frmTaskPoster" method="post" action="task">
									<s:hidden type="hidden" id="taskID" name="taskID" />					
				
					</form>
	
				
			</div>
		</div>
	
	
	
	
	
	<%-- 
	
		
			
				<h3>Available Projects</h3>
					<div id="container">		
						<div id="demo_jui">
        					<table id="jqueryDataTable" class="display jqueryDataTable">
        					<thead>
        						<tr>
    								<th  align="left">ideaId</th> 
                					<th  >Idea Title </th> 
									<th>Description</th> 
    								<th>Domain</th> 
    								<th>Posted on</th> 
    								<th>Budget</th>
    								<th>Duration</th>
        						</tr>
        					</thead>
        					<tbody>
        					</tbody>
        					</table>
						</div>
					</div>
				<form id="frmTaskPoster" method="post" action="task">
									<s:hidden type="hidden" id="taskID" name="taskID" />					
				
					</form>
			

 --%>

</body>
</html>