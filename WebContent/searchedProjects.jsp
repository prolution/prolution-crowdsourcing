<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css"
	media="screen" />
<script src="pm/js/jquery-1.11.2.min.js"></script>
<script src="pm/js/jquery-ui.js"></script>
<link rel="stylesheet" href="pm/css/jquery-ui.css">
<link rel="stylesheet" href="pm/css/style.css">
<script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
<link rel="stylesheet" type="text/css" href="css/noJS.css" />
<link rel="stylesheet" href="css/style.css">

<%--  <script>
  $(function() {
    $( "#speed" ).selectmenu();
 
    $( "#files" ).selectmenu();
 
    $( "#number" )
      .selectmenu()
      .selectmenu( "menuWidget" )
        .addClass( "overflow" );
  });
  $( "#speed" ).(function() {
  alert( $("#speed option:selected").text());
  });
  </script>
<style>
    fieldset {
      border: 0;
       width: 200px;
    }
    label {
      display: block;
      margin: 30px 0 0 0;
    }
    select {
      width: 200px;
    }
    .overflow {
      height: 200px;
    }
  </style> --%>
</head>
<body>
	<h3>Available Projects</h3>
	
	<br> <br> 
	
	
	<div class="top-navigation">
	
			
			<div id="dd" class="wrapper-dropdown-5" tabindex="1"
				style="background-color: #293E6A">
				<p style="color: #999; font-weight: bold;">Category</p>
				<ul class="dropdown">
					<li><a href="webProjectSearch"><i class="icon-user"></i>Web</a></li>
					<li><a href="projectSearch"><i class="icon-cog"></i>Desktop</a></li>
					<li><a href="projectSearch"><i class="icon-remove"></i>Mobile</a></li>
					<li><a href="projectSearch"><i class="icon-remove"></i>Other</a></li>

				</ul>
			</div>
			<div class="headingtext">
						<h4>Enterpreneurs | Project Managers | Contributors</h4>
			</div>
	</div>
	<div class="tab_container">



		<div id="tab1" class="tab_content">
			

			<%-- 	<fieldset>
			    <label for="speed">Category</label>
			
	<select name="speed" id="speed" >
      <option onclick="">Web</option>
      <option>Android</option>
      <option selected="selected">IOS</option>
      <option>Desktop</option>
      <option>other</option>
    </select>
    </fieldset> --%>
			<table class="tablesorter" cellspacing="0">
				<thead>
					<tr>

						<th align="left">Idea Title</th>
						<th>Bids</th>

						<th>Description</th>
						<th>Posted on</th>
						<th>Budget</th>
						<th>Duration</th>
					</tr>
				</thead>
				<s:iterator value="ideas">
					<tr>
						<td><s:property value="ideaTitle" /></td>
						<td><s:property value="noOfBids" /></td>

						<td><s:property value="ideaDescrption" /></td>
						<td><s:property value="ideaPostedOn" /></td>
						<td><s:property value="ideaBudget" /></td>
						<td><s:property value="ideaDuration" /></td>
					</tr>
				</s:iterator>
				
				<s:iterator value="tasks">
					<tr>
						<td><s:property value="taskTitle" /></td>
						<td><s:property value="noOfBids" /></td>

						<td><s:property value="taskDescrption" /></td>
						<td><s:property value="taskPostedOn" /></td>
						<td><s:property value="taskBudget" /></td>
						<td><s:property value="taskDuration" /></td>
					</tr>
				</s:iterator>
			</table>
		</div>

	</div>
	<div class="clear"></div>


	<script src="pm/js/jquery-1.11.2.min.js"></script>

	<script type="text/javascript">
		function DropDown(el) {
			this.dd = el;
			this.initEvents();
		}
		DropDown.prototype = {
			initEvents : function() {
				var obj = this;

				obj.dd.on('click', function(event) {
					$(this).toggleClass('active');
					event.stopPropagation();
				});
			}
		}

		$(function() {

			var dd = new DropDown($('#dd'));

			$(document).click(function() {
				// all dropdowns
				$('.wrapper-dropdown-5').removeClass('active');
			});

		});

	
	</script>
</body>
</html>