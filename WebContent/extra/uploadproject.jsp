<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="/struts-tags" prefix="s"%>
<%@page import="java.util.*"%>
 

<!doctype html>
<html lang="en">

<head>

	<title>Dash board(Project Manager)</title>
	
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />

	
	<script language="Javascript">

// please keep these lines on when you copy the source
// made by: Nicolas - http://www.javascript-page.com

// modified by kent

function displaydate() {
var today = new Date()
var month = today.getMonth() + 1
var day = today.getDate()
var year = today.getFullYear()
var s = "/"

if(month > 0 && month < 10) { month = "0" + month; }
if(day > 0 && day < 10) { day = "0" + day; }

document.form1.datetext.value = month + s + day + s + year
}

</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title">WELCOME</h1>
			<h2 class="section_title">Project Manager</h2>
			<div class="btn_view_site"> 
			  <!--this code to be changed  -->    
			    <!-- <a href="signin.jsp">Log Out</a> -->
				<s:form action="logout"><a href="">Log Out</a></s:form>    
		   </div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
			<div class="user">
					${sessionScope.username}
				<p class="topusername"><s:label label="%{sessionScope.username}"  ></s:label></p>
			
				<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
			</div>
			<div class="breadcrumbs_container">
				<article class="breadcrumbs"><a href="dashboardPm.jsp">Project Manager</a> <div class="breadcrumb_divider"></div> <a class="current">Upload Project</a></article>
			</div>
	
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>Profile</h3>
		<ul class="toggle">		
				
			<li class="icn_new_article"><a href="profilePm.jsp">My Profile</a></li>
			<li class="icn_edit_article"><a href="updateProfilePm.jsp">Update Profile</a></li>
			<li class="icn_categories"><a href="#">Profile Ratings</a></li>
			</ul>
		<h3>Projects</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="uploadproject.jsp">Upload Projects</a></li>
			<li class="icn_edit_article"><a href="currentprojects.jsp">Current Projects</a></li>
			<li class="icn_categories"><a href="#">Completed Projects</a></li>
			</ul>
		<h3>Task</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="#">New Requests</a></li>
			<li class="icn_view_users"><a href="#">In progress</a></li>
			<li class="icn_view_users"><a href="#">Accepted</a></li>
			</ul>
		<h3>bids</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="#">Accepted Bids</a></li>
			<li class="icn_photo"><a href="#">Rejected Bids</a></li>
			<li class="icn_audio"><a href="#">New Bids</a></li>
			
		</ul>
		<h3>scheduling</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Gantt Chart</a></li>
			
		</ul>
		<h3>Account</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Pro-Coins</a></li>
			
		</ul>
		<ul class="toggle">
			<li class="icn_settings"><a href="#">Scoring</a></li>
		</ul>
		
		<h3><s:form action="logout"><a href="">Log Out</a></s:form></h3>
		
		<footer>
			<hr />
			<p><strong>Copyrights@ prolutions.com</strong></p>
			<p></p>
		</footer>
	</aside>
	
	
	<!-- end of sidebar -->
	
	
	
	
	
	<section id="main" class="column">
		
		
		
		<article class="module width_full">
			<header>
			  	 <h3>Upload New Project</h3>
			</header>
						<div class="module_content">
							<article class="stats_graph">
								<div class="form-group">
    							
	    								<!-- <textarea class="form-control" rows="1" cols="60" placeholder="Post Your Idea"></textarea> -->
	    								  <br>			
								</div>

	        					<div class="module-body">
									
									<input type="text" name="proTitle" class="input-short" placeholder="title" />   <br>
									<input type="text" class="input-short" placeholder="Description" /> <br>
									<input type="text" name="proDomain" class="input-short" placeholder="Domain" /> <br>
									<input type="text" class="input-short" placeholder="Budget" /> <br>
									<input type="text" class="input-short" placeholder="Duration" /> <br>
									<!-- <input type="text" class="input-short" placeholder="Duration" /> <br> -->
									<s:submit value="Post"></s:submit>
							


							<!--
							<h1>PRINT CURRENT SYATEM DATE</h1> 
							date :<input type="text" value="<%= new Date().getDate()%>/<%= new Date().getMonth()%>/<%= new Date().getYear()%>"> <br><br><br><br><br> -->






								<!-- note the position -->


									<!--<p>
                                		 <label> Duration </label>
                                		
                                		
                                		<select class="input-short">
                                   		<option value="1"> Months </option>
                                    		<option value="2"> Days </option>
                                    		<option value="3"> Years </option>
                                		</select>
                           	 		</p>
									 <p>
                                		<label> Budget </label>
                                		<input type="text" class="input-short" placeholder="Your Budget"/> <span> $ </span>
									</p> -->
								</div>
							</article>
				
						<div class="clear"></div>
						</div>
		</article><!-- end of stats article -->
		
		
		
		<!-- 
		dynamic table view 
		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th></th> 
    				<th>Project Name</th> 
    				<th>Project Id</th> 
    				<th>Uploaded On</th> 
    				<th>Bid</th> 
				</tr> 
			</thead> 
			 
			</table>

			</div>
			
		</div>
		
		 -->
		<!-- end of .tab_container -->
		
		
		
		<!-- 
		<article class="module width_quarter">
			<header><h3>CONTRIBUTORS</h3></header>
			<div class="message_list">
				<div class="module_content">
					<div class="message"><p>mng1</p>
					
					<div class="message"><p>mng2</p>
					
					<div class="message"><p>mng3</p>
					
					<div class="message"><p>mng4</p>
					
					<div class="message"><p>mng5</p>
					
				</div>
			</div>
			
					
				</form>
			</footer>
		</article>
		 -->
		
		
		<!-- end of messages article -->
		
		<div class="clear"></div>
		<!-- 
		<article class="module width_full">
			<header><h3>Post New Article</h3></header>
				<div class="module_content">
						<fieldset>
							<label>Post Title</label>
							<input type="text">
						</fieldset>
						<fieldset>
							<label>Content</label>
							<textarea rows="12"></textarea>
						</fieldset>
						<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
						<!-- 	<label>Category</label>
							<select style="width:92%;">
								<option>Articles</option>
								<option>Tutorials</option>
								<option>Freebies</option>
							</select>
						</fieldset>
						<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
						<!-- 	<label>Tags</label>
							<input type="text" style="width:92%;">
						</fieldset><div class="clear"></div>
				</div>
			<footer>
				<div class="submit_link">
					<select>
						<option>Draft</option>
						<option>Published</option>
					</select>
					<input type="submit" value="Publish" class="alt_btn">
					<input type="submit" value="Reset">
				</div>
			</footer>
		</article><!-- end of post new article -->
		
		
		  <!-- Forr future use to be implemented for generating warnings 
				<h4 class="alert_warning">A Warning Alert</h4>
		
				<h4 class="alert_error">An Error Message</h4>
		
				<h4 class="alert_success">A Success Message</h4> -->
				
				
				
		<!-- 
		<article class="module width_full">
			<header><h3>Basic Styles</h3></header>
				<div class="module_content">
					<h1>Header 1</h1>
					<h2>Header 2</h2>
					<h3>Header 3</h3>
					<h4>Header 4</h4>
					<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>

<p>Donec id elit non mi porta <a href="#">link text</a> gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>

					<ul>
						<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
						<li>Cras mattis consectetur purus sit amet fermentum.</li>
						<li>Donec ullamcorper nulla non metus auctor fringilla. </li>
						<li>Cras mattis consectetur purus sit amet fermentum.</li>
					</ul>
				</div>
		</article><!-- end of styles article --> 
		<div class="spacer"></div>
	</section>


</body>

</html>