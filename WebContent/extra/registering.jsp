<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
                <%@ taglib uri="/struts-tags" prefix="s" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Simple Sign Up Form by PremiumFreeibes.eu</title>

	<link rel="stylesheet" type="text/css" href="./css/style.css" />

</head>

<body>

    <!--BEGIN #signup-form -->
    <div id="signup-form">
        
        <!--BEGIN #subscribe-inner -->
        <div id="signup-inner">
        
        	<div class="clearfix" id="header">
        	
        		
        
                <h1>Sign up  <br/></h1>

            
            </div>
			<p>Please complete the fields below, </p>

            
<form action="register" method="post">            	
                <p>

                <label for="fname">First Name *</label>
                <input type="text" name="firstname" value="" data-validation="length" data-validation-length="min1" />
                </p>
                
                <p>
                <label for="lname">Last Name *</label>
                <input type="text" name="lastname" value="" />
                </p>
                
                <p>

                <label for="email">Email *</label>
                <input type="text" data-validation="email" name="email" />
                </p>
                
                <p>
                <label for="username">Username *</label>
                <input type="text" name="username" value="" />
                </p>
                
                <p>

                <label for="password">Password *</label>
                <input  type="password" name="upassword" value="" />
                </p>
                
                
                
                <p>
                <label for="signup">Signing up as *</label>
                 <div>
                 
                 <s:radio id="radiobutton" name="usertype" title="label1" 
    list="#{'p':'Project Manager','c':'Contributor','e':'Enterpreneur'}"/> 
                 </div>
                
                <p>
				<input type="submit" value="submit" />
                </p>
                
</form>
<script src="./jquery/jquery-1.11.1.min.js"></script>
<script src="./jquery/jquery.form-validator.min.js"></script>
<script>
  $.validate();
  $('#my-textarea').restrictLength( $('#max-length-element') );
   
</script>            
		<div id="required">
		<p>* Required Fields<br/>
		NOTE: You must activate your account after sign up</p>
		</div>


            </div>
        
        <!--END #signup-inner -->
        </div>
        
    <!--END #signup-form -->   
    

</body>
</html>