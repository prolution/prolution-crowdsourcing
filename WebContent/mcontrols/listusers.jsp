<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%>


<!DOCTYPE html>
<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
<title>View all Users</title>
			<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">  
			<link href="en/css/bootstrap.min.css" rel="stylesheet"> 

</head>
<body>
	
	
	
			<div class="headingtext">
						<h4><a href="EnSearch">Enterpreneurs |</a> <a href="PmSearch">Project Managers</a> | <a href="CnSearch">Contributors</a></h4>
			</div> 
		
			<table class="tablesorter">
				<thead>
					<tr>

						<th align="left">User ID</th>
						<th>Full Name</th>
						
						<th>User name</th>
						<th>Country</th>
						<th>email</th>
						<th>View Profile</th>
						<th>Warn</th>
						<th>Block</th>
						
					</tr>
				</thead>
				<s:iterator value="users">
					<tr>
						<td><s:property value="userid" /></td>
						<td><s:property value="fullName" /></td>
						<td><s:property value="username" /></td>

						<td><s:property value="country" /></td>
						<td><s:property value="email" /></td>
						
						<td>
							<s:url id="viewProfile" action="viewProfile">
								<s:param name="userid" value="%{userid}"></s:param>
								<s:param name="username" value="%{username}"></s:param>
							</s:url> <s:a href="%{viewProfile}">View Profile</s:a>
						</td>
						
						<td>
							<s:url id="blockprofile" action="blockprofile">
								<s:param name="userid" value="%{userid}"></s:param>
								<s:param name="userstatus" value="I"></s:param>
							</s:url> <s:a href="%{blockprofile}">Block</s:a>
						</td>
						
						<td><a href="">Warn</a></td>
						
						
						
					</tr>
				</s:iterator>
			
			</table>
			 






</body>
</html>