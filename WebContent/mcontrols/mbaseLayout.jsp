<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" />
</title>
</head>

<body>
<table border="1" cellpadding="1" cellspacing="1" width="100%" align="center">
	<tr>
		<td height="10px" colspan="2">
   		<tiles:insertAttribute  name="banner" /><br/>
   		</td>
   </tr>
   
   <tr>
   
   		<td width="100%" height="100%" style="position: relative;">
   			<tiles:insertAttribute name="body" /><br/>
   		</td>
   </tr>
   
   
   <tr>
   
   		<td height="10%" colspan="2" style="position: relative;">
   			<tiles:insertAttribute name="footer" /><br/>
   		</td>
   	</tr>
   
   </table>
</body>
</html>





