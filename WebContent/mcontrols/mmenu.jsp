<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="/struts-tags" prefix="s"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Management Panel :: Superuser</title>
	
	<link rel="stylesheet" href="bootstrap/css/layout.css" type="text/css" media="screen" />
</head>
<body>
		<form class="quick_search">
			<input type="text" value="Quick Search" onFocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>Profile</h3>
		<ul class="toggle">		
				
			<li class="icn_folder"><a href="#">Manage Users</a></li>
			<li class="icn_folder"><a href="#">Verify Users</a></li>
			<li class="icn_folder"><a href="#">Obnoxious User Requests</a></li>
			
			<li class="icn_categories"> 
			
			<s:form action="mAllusers" >
			<input type="hidden" class="form-control" value="<%=session.getAttribute("userid")%>" name="userid" />
			<s:submit>List Users</s:submit> 
			</s:form>
			 
					<s:a action="mAllusers">Session
					<s:param name="userid" value="#session.userid" />
					</s:a>
				
			</li>
			 			
		</ul>
		<h3>Ideas</h3>
		<ul class="toggle">
		   
			 <li class="icn_folder"><a href="#">Block User</a></li>
			 	 
			 
		</ul>
		<h3>Manage Account</h3>
		<ul class="toggle">
		   
			 <li class="icn_folder"><a href="#">Change Password</a></li>
			 	 
			 
		</ul>
		
		<h3><s:form action="logout"><a href="">Log Out</a></s:form></h3>
		
	
</body>
</html>