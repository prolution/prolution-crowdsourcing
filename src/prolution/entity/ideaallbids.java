package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ideaallbids", catalog = "db_prolution")
public class ideaallbids implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ideaId", nullable = true)
	private int ideaId;
	
	private int userid;
	private int bidsAmount;

	private String username;
	
	private String firstname;
	
	private String lastname;
	
	private String country;
	private String ideaDescrption;
	private String ideaPostedOn;
	private String ideaDomain;
	private String ideaDuration;
	private String ideaBudget;
	private String ideaTitle;


	
	







	public int getBidsAmount() {
		return bidsAmount;
	}





	public void setBidsAmount(int bidsAmount) {
		this.bidsAmount = bidsAmount;
	}











	public ideaallbids(int ideaId, int userid, int bidsAmount, String username,
			String firstname, String lastname, String country,
			String ideaDescrption, String ideaPostedOn, String ideaDomain,
			String ideaDuration, String ideaBudget, String ideaTitle) {
		super();
		this.ideaId = ideaId;
		this.userid = userid;
		this.bidsAmount = bidsAmount;
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.country = country;
		this.ideaDescrption = ideaDescrption;
		this.ideaPostedOn = ideaPostedOn;
		this.ideaDomain = ideaDomain;
		this.ideaDuration = ideaDuration;
		this.ideaBudget = ideaBudget;
		this.ideaTitle = ideaTitle;
	}





	public int getUserid() {
		return userid;
	}





	public void setUserid(int userid) {
		this.userid = userid;
	}





	public String getIdeaDescrption() {
		return ideaDescrption;
	}





	public void setIdeaDescrption(String ideaDescrption) {
		this.ideaDescrption = ideaDescrption;
	}





	public String getIdeaPostedOn() {
		return ideaPostedOn;
	}





	public void setIdeaPostedOn(String ideaPostedOn) {
		this.ideaPostedOn = ideaPostedOn;
	}





	public String getIdeaDomain() {
		return ideaDomain;
	}





	public void setIdeaDomain(String ideaDomain) {
		this.ideaDomain = ideaDomain;
	}





	public String getIdeaDuration() {
		return ideaDuration;
	}





	public void setIdeaDuration(String ideaDuration) {
		this.ideaDuration = ideaDuration;
	}





	public String getIdeaBudget() {
		return ideaBudget;
	}





	public void setIdeaBudget(String ideaBudget) {
		this.ideaBudget = ideaBudget;
	}





	public String getIdeaTitle() {
		return ideaTitle;
	}





	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}





	public int getIdeaId() {
		return ideaId;
	}





	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}





	public String getUsername() {
		return username;
	}





	public void setUsername(String username) {
		this.username = username;
	}





	public String getFirstname() {
		return firstname;
	}





	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}





	public String getLastname() {
		return lastname;
	}





	public void setLastname(String lastname) {
		this.lastname = lastname;
	}





	public String getCountry() {
		return country;
	}





	public void setCountry(String country) {
		this.country = country;
	}





	public ideaallbids() {
		// TODO Auto-generated constructor stub
	}
	
	

	

}
