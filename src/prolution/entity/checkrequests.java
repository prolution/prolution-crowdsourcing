package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "checkrequests", catalog = "db_prolution")
public class checkrequests implements java.io.Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "userid", nullable = true)
	private long userid;
	private String username;
	private long bids;
	public long getUserid() {
		return userid;
	}
	public void setUserid(long l) {
		this.userid = l;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getBids() {
		return bids;
	}
	public void setBids(long bids) {
		this.bids = bids;
	}
	public checkrequests(int userid, String username, long bids) {
		super();
		this.userid = userid;
		this.username = username;
		this.bids = bids;
	}
	public checkrequests() {
		super();
		// TODO Auto-generated constructor stub
	}
	


}
