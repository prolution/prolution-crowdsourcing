package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_idea", catalog = "db_prolution")
public class tb_idea implements java.io.Serializable {
	
	@Id
    @GeneratedValue
	@Column(name = "ideaId", unique = true, nullable = true)
	private int ideaId;
	
	@Column(name = "ideaOwner", unique = true, nullable = true)
	private int ideaOwner;
	
	@Column(name = "ideaTitle", nullable = false, length = 80)
	private String ideaTitle;
	
	@Column(name = "ideaDomain", nullable = false, length = 80)
	private String ideaDomain;
	
	@Column(name = "ideaPostedOn", nullable = false, length = 80)
	private String ideaPostedOn;
	
	@Column(name = "ideaBudget", nullable = false, length = 80)
	private String ideaBudget;
	
	@Column(name = "ideaDuration", nullable = false, length = 80)
	private String ideaDuration;
	
	@Column(name = "ideaFlag", nullable = false, length = 2)
	private int ideaFlag;
	
	@Column(name = "ideaPM",unique = true, nullable = true)
	private int ideaPM;
	
	
	@Column(name = "ideaDescrption", nullable = true)
	private String ideaDescrption;


	@Column(name = "ideaCommit", nullable = true)
	private String ideaCommit;

	@Column(name = "ideaDays", nullable = true)
	private int ideaDays;

	public int getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}

	public int getIdeaOwner() {
		return ideaOwner;
	}

	public void setIdeaOwner(int ideaOwner) {
		this.ideaOwner = ideaOwner;
	}

	public String getIdeaTitle() {
		return ideaTitle;
	}

	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}

	public String getIdeaDomain() {
		return ideaDomain;
	}

	public void setIdeaDomain(String ideaDomain) {
		this.ideaDomain = ideaDomain;
	}

	public String getIdeaPostedOn() {
		return ideaPostedOn;
	}

	public void setIdeaPostedOn(String ideaPostedOn) {
		this.ideaPostedOn = ideaPostedOn;
	}

	public String getIdeaBudget() {
		return ideaBudget;
	}

	public void setIdeaBudget(String ideaBudget) {
		this.ideaBudget = ideaBudget;
	}

	public String getIdeaDuration() {
		return ideaDuration;
	}

	public void setIdeaDuration(String ideaDuration) {
		this.ideaDuration = ideaDuration;
	}

	public int getIdeaFlag() {
		return ideaFlag;
	}

	public void setIdeaFlag(int ideaFlag) {
		this.ideaFlag = ideaFlag;
	}

	public int getIdeaPM() {
		return ideaPM;
	}

	public void setIdeaPM(int ideaPM) {
		this.ideaPM = ideaPM;
	}

	public String getIdeaDescrption() {
		return ideaDescrption;
	}

	public void setIdeaDescrption(String ideaDescrption) {
		this.ideaDescrption = ideaDescrption;
	}

	public String getIdeaCommit() {
		return ideaCommit;
	}

	public void setIdeaCommit(String ideaCommit) {
		this.ideaCommit = ideaCommit;
	}

	public int getIdeaDays() {
		return ideaDays;
	}

	public void setIdeaDays(int ideaDays) {
		this.ideaDays = ideaDays;
	}

	public tb_idea(int ideaId, int ideaOwner, String ideaTitle,
			String ideaDomain, String ideaPostedOn, String ideaBudget,
			String ideaDuration, int ideaFlag, int ideaPM,
			String ideaDescrption, String ideaCommit, int ideaDays) {
		super();
		this.ideaId = ideaId;
		this.ideaOwner = ideaOwner;
		this.ideaTitle = ideaTitle;
		this.ideaDomain = ideaDomain;
		this.ideaPostedOn = ideaPostedOn;
		this.ideaBudget = ideaBudget;
		this.ideaDuration = ideaDuration;
		this.ideaFlag = ideaFlag;
		this.ideaPM = ideaPM;
		this.ideaDescrption = ideaDescrption;
		this.ideaCommit = ideaCommit;
		this.ideaDays = ideaDays;
	}

	public tb_idea() {
		super();
		// TODO Auto-generated constructor stub
	}

	 
}
