package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tb_rate", catalog = "db_prolution")
public class tb_rate {
	
	@Id
	@Column(name = "rateId", unique = true, nullable = false)
	private int rateId;
	
	@Column(name = "rating", unique = true, nullable = true)
	private int rating;
	
	@Column(name = "rateIdea", unique = true, nullable = true)
	private int rateIdea;
	
	@Column(name = "rateTask", unique = true, nullable = true)
	private int rateTask;
	
	@Column(name = "rated", unique = true, nullable = true)
	private int rated;
	
	@Column(name = "rater", unique = true, nullable = true)
	private int rater;

	public int getRateId() {
		return rateId;
	}

	public void setRateId(int rateId) {
		this.rateId = rateId;
	}

	



	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getRateIdea() {
		return rateIdea;
	}

	public void setRateIdea(int rateIdea) {
		this.rateIdea = rateIdea;
	}

	public int getRateTask() {
		return rateTask;
	}

	public void setRateTask(int rateTask) {
		this.rateTask = rateTask;
	}

	public int getRated() {
		return rated;
	}

	public void setRated(int rated) {
		this.rated = rated;
	}

	public int getRater() {
		return rater;
	}

	public void setRater(int rater) {
		this.rater = rater;
	}





	public tb_rate(int rateId, int rating, int rateIdea, int rateTask,
			int rated, int rater) {
		super();
		this.rateId = rateId;
		this.rating = rating;
		this.rateIdea = rateIdea;
		this.rateTask = rateTask;
		this.rated = rated;
		this.rater = rater;
	}

	public tb_rate() {
		// TODO Auto-generated constructor stub
	}
	
	


}
