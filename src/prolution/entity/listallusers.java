
package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "listallusers", catalog = "db_prolution")
public class listallusers implements java.io.Serializable

{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9132629817079645687L;

	@Id
	@Column(name = "userid", nullable = true)
	private int userid;

	private String username;
	
	private String fullName;
	
	private String country;
	
	private String email;
	
	private String usertype;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public listallusers(int userid, String username, String fullName,
			String country, String email, String usertype) {
		super();
		this.userid = userid;
		this.username = username;
		this.fullName = fullName;
		this.country = country;
		this.email = email;
		this.usertype = usertype;
	}

	public listallusers() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	

	
}
