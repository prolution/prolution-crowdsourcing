package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "searchedideas", catalog = "db_prolution")
public class searchedideas implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */


	@Id
	@Column(name = "ideaId", nullable = true)
	private int ideaId;
	
	private String ideaTitle;
	
	private String ideaDescrption;
	
	private String ideaDomain;
	
	private String ideaPostedOn;
	
	private String ideaBudget;
	
	private String ideaDuration;

	private long noOfBids;

	public int getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}

	public String getIdeaTitle() {
		return ideaTitle;
	}

	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}

	public String getIdeaDomain() {
		return ideaDomain;
	}

	public void setIdeaDomain(String ideaDomain) {
		this.ideaDomain = ideaDomain;
	}

	public String getIdeaPostedOn() {
		return ideaPostedOn;
	}

	public void setIdeaPostedOn(String ideaPostedOn) {
		this.ideaPostedOn = ideaPostedOn;
	}

	public String getIdeaBudget() {
		return ideaBudget;
	}

	public void setIdeaBudget(String ideaBudget) {
		this.ideaBudget = ideaBudget;
	}

	public String getIdeaDuration() {
		return ideaDuration;
	}

	public void setIdeaDuration(String ideaDuration) {
		this.ideaDuration = ideaDuration;
	}

	public String getIdeaDescrption() {
		return ideaDescrption;
	}

	public void setIdeaDescrption(String ideaDescrption) {
		this.ideaDescrption = ideaDescrption;
	}


	public long getNoOfBids() {
		return noOfBids;
	}

	public void setNoOfBids(long noOfBids) {
		this.noOfBids = noOfBids;
	}

	public searchedideas() {
		// TODO Auto-generated constructor stub
	}

	public searchedideas(int ideaId, String ideaTitle, String ideaDomain,
			String ideaPostedOn, String ideaBudget, String ideaDuration,
			String ideaDescrption, long noOfBids) {
		super();
		this.ideaId = ideaId;
		this.ideaTitle = ideaTitle;
		this.ideaDomain = ideaDomain;
		this.ideaPostedOn = ideaPostedOn;
		this.ideaBudget = ideaBudget;
		this.ideaDuration = ideaDuration;
		this.ideaDescrption = ideaDescrption;
		this.noOfBids = noOfBids;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	

}
