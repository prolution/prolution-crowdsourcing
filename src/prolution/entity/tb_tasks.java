package prolution.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_tasks", catalog = "db_prolution")
public class tb_tasks implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "taskID", unique = true, nullable = true)
	private int taskID;
	
	@Column(name = "taskOwner", unique = true, nullable = true)
	private int taskOwner;
	
	@Column(name = "taskProId", unique = true, nullable = true)
	private int taskProId;
	
	@Column(name = "taskDays", nullable = false, length = 80)
	private int taskDays;
	
	@Column(name = "taskTitle", nullable = false, length = 80)
	private String taskTitle;
	
	@Column(name = "taskDomain", nullable = false, length = 80)
	private String taskDomain;
	
	@Column(name = "taskPostedOn", nullable = false, length = 80)
	private String taskPostedOn;
	
	@Column(name = "taskBudget", nullable = false, length = 80)
	private String taskBudget;
	
	@Column(name = "taskDuration", nullable = false, length = 80)
	private String taskDuration;
	
	
	
	@Column(name = "taskFlag", unique = true, nullable = true)
	private int taskFlag;
	
	@Column(name = "precedor", unique = true, nullable = true)
	private int precedor;
	
	@Column(name = "taskDescrption", nullable = true)
	private String taskDescrption;
	
	@Column(name = "taskCommit", nullable = true)
	private String taskCommit;
	
	@Column(name = "taskIdeaId", unique = true, nullable = true)
	private int taskIdeaId;


	
	
	public int getTaskDays() {
		return taskDays;
	}


	public void setTaskDays(int taskDays) {
		this.taskDays = taskDays;
	}


	public int getTaskIdeaId() {
		return taskIdeaId;
	}


	public void setTaskIdeaId(int taskIdeaId) {
		this.taskIdeaId = taskIdeaId;
	}


	public String getTaskCommit() {
		return taskCommit;
	}


	public void setTaskCommit(String taskCommit) {
		this.taskCommit = taskCommit;
	}


	public int getTaskID() {
		return taskID;
	}


	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}


	public int getTaskOwner() {
		return taskOwner;
	}


	public void setTaskOwner(int taskOwner) {
		this.taskOwner = taskOwner;
	}


	public int getTaskProId() {
		return taskProId;
	}


	public void setTaskProId(int taskProId) {
		this.taskProId = taskProId;
	}


	public String getTaskTitle() {
		return taskTitle;
	}


	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}


	public String getTaskDomain() {
		return taskDomain;
	}


	public void setTaskDomain(String taskDomain) {
		this.taskDomain = taskDomain;
	}





	public String getTaskBudget() {
		return taskBudget;
	}


	public void setTaskBudget(String taskBudget) {
		this.taskBudget = taskBudget;
	}








	public int getTaskFlag() {
		return taskFlag;
	}


	public void setTaskFlag(int taskFlag) {
		this.taskFlag = taskFlag;
	}


	public String getTaskDescrption() {
		return taskDescrption;
	}


	public void setTaskDescrption(String taskDescrption) {
		this.taskDescrption = taskDescrption;
	}











	public String getTaskPostedOn() {
		return taskPostedOn;
	}


	public void setTaskPostedOn(String taskPostedOn) {
		this.taskPostedOn = taskPostedOn;
	}


	public String getTaskDuration() {
		return taskDuration;
	}


	public void setTaskDuration(String taskDuration) {
		this.taskDuration = taskDuration;
	}







	public tb_tasks(int taskID, int taskOwner, int taskProId, int taskDays,
			String taskTitle, String taskDomain, String taskPostedOn,
			String taskBudget, String taskDuration, int taskFlag, int precedor,
			String taskDescrption, String taskCommit, int taskIdeaId) {
		super();
		this.taskID = taskID;
		this.taskOwner = taskOwner;
		this.taskProId = taskProId;
		this.taskDays = taskDays;
		this.taskTitle = taskTitle;
		this.taskDomain = taskDomain;
		this.taskPostedOn = taskPostedOn;
		this.taskBudget = taskBudget;
		this.taskDuration = taskDuration;
		this.taskFlag = taskFlag;
		this.precedor = precedor;
		this.taskDescrption = taskDescrption;
		this.taskCommit = taskCommit;
		this.taskIdeaId = taskIdeaId;
	}


	public int getPrecedor() {
		return precedor;
	}


	public void setPrecedor(int precedor) {
		this.precedor = precedor;
	}


	public tb_tasks() {
		// TODO Auto-generated constructor stub
	}


}