package prolution.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_users", catalog = "db_prolution")
public class view_users {
	
	private static final long serialVersionUID = 9132629817079645687L;

	@Id
	@Column(name = "userid", nullable = true)
	private int userid;
	
			private BigDecimal rating;

	private String username;
	
	private String fullName;
	
	private String country;
	
	private String email;
	
	private String usertype;

	private String userstatus;

	




	

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getUserstatus() {
		return userstatus;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	

	

	





	


	public view_users(int userid, BigDecimal rating, String username,
			String fullName, String country, String email, String usertype,
			String userstatus) {
		super();
		this.userid = userid;
		this.rating = rating;
		this.username = username;
		this.fullName = fullName;
		this.country = country;
		this.email = email;
		this.usertype = usertype;
		this.userstatus = userstatus;
	}

	public BigDecimal getRating() {
		return rating;
	}

	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

	public view_users() {
		// TODO Auto-generated constructor stub
	}
	
	

}
