package prolution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "TaskAllBids", catalog = "db_prolution")
public class TaskAllBids implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "taskID", nullable = true)
	private int taskID;
	
	private int userid;

	private String username;
	
	private String firstname;
	
	private String lastname;
	private int bidsAmount;

	private String country;
	private String taskDescrption;
	private String	taskPostedOn;
	private String taskDomain;
	private String taskDuration;
	private String taskBudget;
	private String	taskTitle;
	public int getTaskID() {
		return taskID;
	}
	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getTaskDescrption() {
		return taskDescrption;
	}
	public void setTaskDescrption(String taskDescrption) {
		this.taskDescrption = taskDescrption;
	}
	public String getTaskPostedOn() {
		return taskPostedOn;
	}
	public void setTaskPostedOn(String taskPostedOn) {
		this.taskPostedOn = taskPostedOn;
	}
	public String getTaskDomain() {
		return taskDomain;
	}
	public void setTaskDomain(String taskDomain) {
		this.taskDomain = taskDomain;
	}
	public String getTaskDuration() {
		return taskDuration;
	}
	public void setTaskDuration(String taskDuration) {
		this.taskDuration = taskDuration;
	}
	public String getTaskBudget() {
		return taskBudget;
	}
	public void setTaskBudget(String taskBudget) {
		this.taskBudget = taskBudget;
	}
	public String getTaskTitle() {
		return taskTitle;
	}
	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TaskAllBids(int taskID, int userid, String username,
			String firstname, String lastname, int bidsAmount, String country,
			String taskDescrption, String taskPostedOn, String taskDomain,
			String taskDuration, String taskBudget, String taskTitle) {
		super();
		this.taskID = taskID;
		this.userid = userid;
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.bidsAmount = bidsAmount;
		this.country = country;
		this.taskDescrption = taskDescrption;
		this.taskPostedOn = taskPostedOn;
		this.taskDomain = taskDomain;
		this.taskDuration = taskDuration;
		this.taskBudget = taskBudget;
		this.taskTitle = taskTitle;
	}
	public TaskAllBids() {
		// TODO Auto-generated constructor stub
	}
	public int getBidsAmount() {
		return bidsAmount;
	}
	public void setBidsAmount(int bidsAmount) {
		this.bidsAmount = bidsAmount;
	}
	
	

}
