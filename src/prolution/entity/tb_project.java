package prolution.entity;
import java.util.Date;

import javax.persistence.*;
import javax.websocket.Session;


@SuppressWarnings("unused")
@Entity
@Table(name = "tb_project", catalog = "db_prolution")
public class tb_project implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "proID", unique = true, nullable = true)
	private int proID;
	
	@Column(name = "proOwner", unique = true, nullable = true)
	private int proOwner;
	
	@Column(name = "proIdeaId", unique = true, nullable = true)
	private int proIdeaId;
	
	@Column(name = "proTitle", nullable = false, length = 80)
	private String proTitle;
	
	@Column(name = "proDomain", nullable = false, length = 80)
	private String proDomain;
	
	@Column(name = "proPostedOn", nullable = false, length = 80)
	private String proPostedOn;
	
	@Column(name = "proBudget", nullable = false, length = 80)
	private String proBudget;
	
	@Column(name = "proDays", unique = true, nullable = true)
	private int proDays;
	
	@Column(name = "proDuration", nullable = false, length = 80)
	private String proDuration;
	
	@Column(name = "proFlag", unique = true, nullable = true)
	private int proFlag;
	
	
	
	
	


	public int getProDays() {
		return proDays;
	}


	public void setProDays(int proDays) {
		this.proDays = proDays;
	}


	@Column(name = "proDescrption", nullable = true)
	private String proDescrption;


	public int getProID() {
		return proID;
	}


	public void setProID(int proID) {
		this.proID = proID;
	}


	public int getProOwner() {
		return proOwner;
	}


	public void setProOwner(int proOwner) {
		this.proOwner = proOwner;
	}


	public int getProIdeaId() {
		return proIdeaId;
	}


	public void setProIdeaId(int proIdeaId) {
		this.proIdeaId = proIdeaId;
	}


	public String getProTitle() {
		return proTitle;
	}


	public void setProTitle(String proTitle) {
		this.proTitle = proTitle;
	}


	public String getProDomain() {
		return proDomain;
	}


	public void setProDomain(String proDomain) {
		this.proDomain = proDomain;
	}


	public String getProPostedOn() {
		return proPostedOn;
	}


	public void setProPostedOn(String proPostedOn) {
		this.proPostedOn = proPostedOn;
	}


	public String getProBudget() {
		return proBudget;
	}


	public void setProBudget(String proBudget) {
		this.proBudget = proBudget;
	}


	public String getProDuration() {
		return proDuration;
	}


	public void setProDuration(String proDuration) {
		this.proDuration = proDuration;
	}


	

	public int getProFlag() {
		return proFlag;
	}


	public void setProFlag(int proFlag) {
		this.proFlag = proFlag;
	}


	public String getProDescrption() {
		return proDescrption;
	}


	public void setProDescrption(String proDescrption) {
		this.proDescrption = proDescrption;
	}








	public tb_project(int proID, int proOwner, int proIdeaId, String proTitle,
			String proDomain, String proPostedOn, String proBudget,
			int proDays, String proDuration, int proFlag, String proDescrption) {
		super();
		this.proID = proID;
		this.proOwner = proOwner;
		this.proIdeaId = proIdeaId;
		this.proTitle = proTitle;
		this.proDomain = proDomain;
		this.proPostedOn = proPostedOn;
		this.proBudget = proBudget;
		this.proDays = proDays;
		this.proDuration = proDuration;
		this.proFlag = proFlag;
		this.proDescrption = proDescrption;
	}


	public tb_project() {
		// TODO Auto-generated constructor stub
	}
	





	


	
	
}
