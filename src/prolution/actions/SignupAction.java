package prolution.actions;

import javax.persistence.Column;

import prolution.dao.UserDao;

import com.opensymphony.xwork2.ActionSupport;

public class SignupAction extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	
	private String upassword;
	
	private String email;
	
	private String firstname;
	
	private String lastname;
	
	private String usertype;
	
	private String country;

	private String userstatus;
    

    
    public String getUserstatus() {
		return userstatus;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUpassword() {
		return upassword;
	}

	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	UserDao dao=new UserDao();
    
    public String validReg(){
 
        if(dao.regis(getUsername() ,getUpassword()  , getFirstname()  , getLastname() , getEmail(), getUsertype(), getCountry()))
            return "success";
        
        else
        {
            this.addActionError("Invalid username and password");
            return "error";
        }
    }
    
    
    

}
