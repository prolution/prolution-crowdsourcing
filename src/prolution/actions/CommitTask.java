package prolution.actions;

import prolution.dao.taskDao;

public class CommitTask {
	
	private int taskId;
	private String taskCommit;
	
	
	taskDao tskDao=new taskDao();
	public String execute(){
		tskDao.commitTask(getTaskId(), getTaskCommit());
		return "success";
	}
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getTaskCommit() {
		return taskCommit;
	}
	public void setTaskCommit(String taskCommit) {
		this.taskCommit = taskCommit;
	}
	
	
	
	

}
