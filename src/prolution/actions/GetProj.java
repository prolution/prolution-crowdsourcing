package prolution.actions;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.entity.tb_idea;
import prolution.entity.tb_project;

public class GetProj {
	private Integer rows = 0;
    private Integer page = 0;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private Integer total = 0;
    private Integer records = 0;
    private Integer proOwner;
	private String currentDate;

	private ArrayList<tb_idea> ideas = new ArrayList<tb_idea>();


	
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public ArrayList<tb_idea> getIdeas() {
		return ideas;
	}
	public void setIdeas(ArrayList<tb_idea> ideas) {
		this.ideas = ideas;
	}
	public Integer getProOwner() {
		return proOwner;
	}
	public void setProOwner(Integer proOwner) {
		this.proOwner = proOwner;
	}
	private ArrayList<tb_project> project = new ArrayList<tb_project>();
	
	projectDao projectService = new projectDao();

	public ArrayList<tb_project> getProject() {
		return project;
	}
	public void setProject(ArrayList<tb_project> project) {
		this.project = project;
	}
	public String allProjects(){
		
		/* try{
	            int to = (rows * page);
	            int from = to - rows;
	            DetachedCriteria criteria = DetachedCriteria
	                    .forClass(tb_project.class);
	            records = studentManager.count(criteria);
	            criteria.setProjection(null);
	            criteria.setResultTransformer(Criteria.ROOT_ENTITY);
	            project = studentManager.find(criteria, from,
	                    rows);
	            total = (int) Math.ceil((double) records / (double) rows);
	            for (Student student : students ) {
	                ListObjectUtil listStudentUtil = new ListObjectUtil();
	                listStudentUtil .setObject(student);
	                this.project.add(listStudentUtil );
	            }
	            type = SUCCESS;
	        }catch (Exception e) {
	            type = ERROR;
	        }
	        return type;
	    }*/
	            
	            
		
		this.project=projectService.allproj();
		return "success";
		}
	
	
	ideaDao ideadao=new ideaDao();
	

	
	public String myProjects(){
	
	this.project=projectService.myProjects(getProOwner());
	this.ideas=ideadao.myProjects(getProOwner());
	Date dNow =new Date();
	   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
	setCurrentDate(ft.format(dNow));
	return "success";
	}
	
	
	
	
	public Integer getRows() {
        return rows;
    }
    public void setRows(Integer rows) {
        this.rows = rows;
    }
    public Integer getPage() {
        return page;
    }
    public void setPage(Integer page) {
        this.page = page;
    }
    public String getSord() {
        return sord;
    }
    public void setSord(String sord) {
        this.sord = sord;
    }
    public String getSidx() {
        return sidx;
    }
    public void setSidx(String sidx) {
        this.sidx = sidx;
    }
    public String getSearchField() {
        return searchField;
    }
    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }
    public String getSearchString() {
        return searchString;
    }
    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
    public String getSearchOper() {
        return searchOper;
    }
    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Integer getRecords() {
        return records;
    }
    public void setRecords(Integer records) {
        this.records = records;
    }

	

}
