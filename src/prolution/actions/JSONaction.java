package prolution.actions;

import java.util.List;

import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.entity.availableIdea;

import com.opensymphony.xwork2.ActionSupport;

public class JSONaction extends ActionSupport {

	private List<availableIdea>      gridModel;
	  private Integer     rows             = 0;
	  private Integer    page             = 0;
	  private String    sord;
	  private String     sidx;
	  private String   searchField;
	  private String              searchString;
	  private String              searchOper;
	  private Integer             total            = 0;
	  private Integer             records          = 0;
		private int bidsUser;

		
		
public int getBidsUser() {
			return bidsUser;
		}

		public void setBidsUser(int bidsUser) {
			this.bidsUser = bidsUser;
		}

		/*	  @Actions( {
		    @Action(value = "/jsontable", results = {
		      @Result(name = "success", type = "json")
		    })
		  })*/
		  public String execute()
		  {

		    int to = (rows * page);
		    int from = to - rows;
		    ideaDao idao= new ideaDao();
		    records = 3;
		    	    gridModel = idao.allIdeas(2);
		    total =(int) Math.ceil((double)records / (double)rows);
		    return SUCCESS;
		  }

		  public String getJSON()
		  {
		    return execute();
		  }		
}
