package prolution.actions;

import java.util.ArrayList;

import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.dao.taskDao;
import prolution.entity.TaskAllBids;
import prolution.entity.tb_project;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;

public class ProjectTask {
	
	private tb_tasks task=new tb_tasks();
	private tb_users user=new tb_users();
	private int taskID;
	private ArrayList<TaskAllBids> taskBids = new ArrayList<TaskAllBids>();
	private int taskOwner;
	private String taskCommit;
	UserDao daoUser=new UserDao();
	taskDao tskDao=new taskDao();
	projectDao projDao=new projectDao();
	ideaDao daoidea=new ideaDao();
	private double rating;
	private int userid;
	private int taskFlag;
	private int bidsAmount;

	
	
	public int getBidsAmount() {
		return bidsAmount;
	}

	public void setBidsAmount(int bidsAmount) {
		this.bidsAmount = bidsAmount;
	}

	public int getTaskFlag() {
		return taskFlag;
	}

	public void setTaskFlag(int taskFlag) {
		this.taskFlag = taskFlag;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String execute(){
		//System.out.println(student.getRegno());
			this.task = tskDao.projectTask(getTaskID());
		if(this.task.getTaskOwner()==0){
		this.taskBids=tskDao.taskAllBids((getTaskID()));
		return "success";
		}
		else {
			this.user=daoUser.taskOwner(this.task.getTaskOwner());
			if(this.task.getTaskFlag()==4){
		    	this.rating=daoUser.userRanking(this.task.getTaskOwner());	
				return "hello";
			}
			else{
			return "input";
			}
			}
	}

	public String taskBidder(){
		//System.out.println(student.getRegno());
			this.task = tskDao.projectTask(getTaskID());
			this.user=daoUser.taskOwner(getUserid());
	    	this.rating=daoUser.userRanking(this.user.getUserid());
	    	this.bidsAmount=this.getBidsAmount();
		return "success";		
		
	}
	
	public String myTask(){
		this.task = tskDao.projectTask(getTaskID());
		if(this.task.getTaskProId() !=0){
			this.user=daoUser.projectOwner(projDao.taskProOwnerId(this.task.getTaskProId()));
			}
			else
			{
				this.user=daoUser.projectOwner(daoidea.proOwnerId(this.task.getTaskIdeaId()));
			}
		if(this.task.getTaskFlag()==4){
			if(this.task.getTaskProId() !=0){
				this.user=daoUser.projectOwner(projDao.taskProOwnerId(this.task.getTaskProId()));
		    	this.rating=daoUser.userRanking(projDao.taskProOwnerId(this.task.getTaskProId()));	
			}
				else
				{
					this.user=daoUser.projectOwner(daoidea.proOwnerId(this.task.getTaskIdeaId()));
			    	this.rating=daoUser.userRanking(daoidea.proOwnerId(this.task.getTaskIdeaId()));

				}
			return "input";
		}
		else{
		return "success";
		}
		}
	

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String task(){
		this.task = tskDao.projectTask(getTaskID());
		if(this.task.getTaskProId() !=0){
			this.user=daoUser.projectOwner(projDao.taskProOwnerId(this.task.getTaskProId()));
	    	this.rating=daoUser.userRanking(this.user.getUserid());

		}
			else
			{
				this.user=daoUser.projectOwner(daoidea.proOwnerId(this.task.getTaskIdeaId()));
		    	this.rating=daoUser.userRanking(this.user.getUserid());

			}
		return "success";
}
	

	public String p2cMsg(){
			tskDao.p2cMsg(getTaskID(), getTaskCommit());
			this.task = tskDao.projectTask(getTaskID());
			this.user=daoUser.taskOwner(this.task.getTaskOwner());
			return "input";
	}
	
	public String taskCompleted(){
		tskDao.taskCompleted(getTaskID(), getTaskFlag());
		this.task = tskDao.projectTask(getTaskID());
		this.user=daoUser.taskOwner(this.task.getTaskOwner());
    	this.rating=daoUser.userRanking(this.task.getTaskOwner());
		return "input";
}
	
	public String c2pMsg(){
		tskDao.p2cMsg(getTaskID(), getTaskCommit());
		this.task = tskDao.projectTask(getTaskID());
		if(this.task.getTaskProId() !=0){
		this.user=daoUser.projectOwner(projDao.taskProOwnerId(this.task.getTaskProId()));
		}
		else
		{
			this.user=daoUser.projectOwner(daoidea.proOwnerId(this.task.getTaskIdeaId()));
		}
		return "input";
}
	
	public tb_users getUser() {
		return user;
	}

//proowner=project(this.taskproId)


	public String getTaskCommit() {
		return taskCommit;
	}


	public void setTaskCommit(String taskCommit) {
		this.taskCommit = taskCommit;
	}


	public void setUser(tb_users user) {
		this.user = user;
	}




	public int getTaskOwner() {
		return taskOwner;
	}




	public void setTaskOwner(int taskOwner) {
		this.taskOwner = taskOwner;
	}




	public tb_tasks getTask() {
		return task;
	}



	public ArrayList<TaskAllBids> getTaskBids() {
		return taskBids;
	}


	public void setTaskBids(ArrayList<TaskAllBids> taskBids) {
		this.taskBids = taskBids;
	}


	public void setTask(tb_tasks task) {
		this.task = task;
	}



	public int getTaskID() {
		return taskID;
	}



	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}
	
	

}
