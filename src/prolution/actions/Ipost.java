package prolution.actions;

import java.util.Date;

import javax.persistence.Column;

import prolution.dao.ideaDao;

import com.opensymphony.xwork2.ActionSupport;

public class Ipost extends ActionSupport{
	private static final long serialVersionUID = 1L;

private int ideaOwner;
	
	
	private String ideaTitle;
	
	private String ideaDomain;
	
	private String ideaPostedOn;
	
	private String ideaBudget;
	
	private String ideaDuration;
	private int ideaDays;
	/**
	private boolean ideaAproved;
	
	private boolean ideaSigned;
	
	private boolean ideaCompletion;
	
	*/
	private int ideaFlag;
	private String ideaDescrption;

	
	ideaDao ideaDao=new ideaDao();
	
	public String execute(){
		ideaDao.post(  ideaOwner, ideaTitle,  ideaDescrption, 
				 ideaDomain,  ideaPostedOn,
				 ideaBudget, ideaDuration, ideaFlag,ideaDays);
		return "success";
	}

	public int getIdeaOwner() {
		return ideaOwner;
	}

	public void setIdeaOwner(int ideaOwner) {
		this.ideaOwner = ideaOwner;
	}

	public String getIdeaTitle() {
		return ideaTitle;
	}

	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}

	public String getIdeaDomain() {
		return ideaDomain;
	}

	public void setIdeaDomain(String ideaDomain) {
		this.ideaDomain = ideaDomain;
	}

	public String getIdeaPostedOn() {
		return ideaPostedOn;
	}

	public void setIdeaPostedOn(String ideaPostedOn) {
		this.ideaPostedOn = ideaPostedOn;
	}

	public String getIdeaBudget() {
		return ideaBudget;
	}

	public void setIdeaBudget(String ideaBudget) {
		this.ideaBudget = ideaBudget;
	}

	public String getIdeaDuration() {
		return ideaDuration;
	}

	public void setIdeaDuration(String ideaDuration) {
		this.ideaDuration = ideaDuration;
	}

	

	public int getIdeaFlag() {
		return ideaFlag;
	}

	public void setIdeaFlag(int ideaFlag) {
		this.ideaFlag = ideaFlag;
	}

	public String getIdeaDescrption() {
		return ideaDescrption;
	}

	public void setIdeaDescrption(String ideaDescrption) {
		this.ideaDescrption = ideaDescrption;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getIdeaDays() {
		return ideaDays;
	}

	public void setIdeaDays(int ideaDays) {
		this.ideaDays = ideaDays;
	}
	
	


}
