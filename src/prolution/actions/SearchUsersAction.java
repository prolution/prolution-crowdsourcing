package prolution.actions;

import java.util.ArrayList;

import prolution.dao.UserDao;
import prolution.entity.view_users;

public class SearchUsersAction {
	
	private ArrayList<view_users> users = new ArrayList<view_users>();

	
	UserDao userDao=new UserDao();
	private String str;
	
	public String searchedUsers(){
		this.users=userDao.viewAllSearchingUsers(str);
		return "success";
		}

	public ArrayList<view_users> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<view_users> users) {
		this.users = users;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
	
	
	

}
