package prolution.actions;

import java.util.ArrayList;

import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.entity.availableIdea;
import prolution.entity.tb_idea;
import prolution.entity.tb_users;

public class bidsIdea {
	
	private  int bidsUser;
	private  int bidsAmount;

	private int bidsIdea;
	private int bidsFlag;
	private ArrayList<availableIdea> ideas = new ArrayList<availableIdea>();
	private int ideaId;
	tb_users user=new tb_users();
	private double rating;


	UserDao daouser=new UserDao();
	private tb_idea idea=new tb_idea();

	




	public int getBidsAmount() {
		return bidsAmount;
	}



	public void setBidsAmount(int bidsAmount) {
		this.bidsAmount = bidsAmount;
	}



	public tb_idea getIdea() {
		return idea;
	}



	public void setIdea(tb_idea idea) {
		this.idea = idea;
	}



	public int getIdeaId() {
		return ideaId;
	}



	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}



	public tb_users getUser() {
		return user;
	}



	public void setUser(tb_users user) {
		this.user = user;
	}



	public double getRating() {
		return rating;
	}



	public void setRating(double rating) {
		this.rating = rating;
	}



	public int getBidsFlag() {
		return bidsFlag;
	}



	public void setBidsFlag(int bidsFlag) {
		this.bidsFlag = bidsFlag;
	}



	public int getBidsUser() {
		return bidsUser;
	}



	public void setBidsUser(int bidsUser) {
		this.bidsUser = bidsUser;
	}



	public int getBidsIdea() {
		return bidsIdea;
	}



	public void setBidsIdea(int bidsIdea) {
		this.bidsIdea = bidsIdea;
	}



ideaDao IDEAdao=new ideaDao();

	public String execute(){
		IDEAdao. bidsIdea(  getBidsUser(), getBidsIdea(),  getBidsFlag(),getBidsAmount());
		this.ideas=IDEAdao.allIdeas(getBidsUser());
		return "success";
	}

	public String ideaPoster(){
		this.idea=IDEAdao.anIdea(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		this.rating=daouser.userRanking(this.idea.getIdeaOwner());

		return "success";
	}

	public ArrayList<availableIdea> getIdeas() {
		return ideas;
	}



	public void setIdeas(ArrayList<availableIdea> ideas) {
		this.ideas = ideas;
	}
	
	
}
