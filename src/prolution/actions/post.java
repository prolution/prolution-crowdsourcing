package prolution.actions;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;








import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.dao.taskDao;
import prolution.entity.ideaallbids;
import prolution.entity.tb_idea;
import prolution.entity.tb_project;
import prolution.entity.tb_users;

import com.opensymphony.xwork2.ActionSupport;

public class post extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	private ArrayList<ideaallbids> allBids = new ArrayList<ideaallbids>();

private int proOwner;
	private int proID;
	private int taskID;
	private int proDays;	

	private int taskIdeaId;
	private int proIdeaId;	
	private String proTitle;	
	private String proDomain;	
	private String proPostedOn;	
	private String proBudget;	
	private String proDuration;	
	private int proFlag;		
	private String proDescrption;
	private int taskOwner;	
	private int taskProId;	
	private String taskTitle;	
	private String taskDomain;	
	private String taskPostedOn;	
	private String taskBudget;	
	private String taskDuration;	
	private int taskFlag;		
	private String taskDescrption;
	private int ideaId;
	private double rating;
	private int din;
	private int precedor;
	private int taskDays;
	private int ideaOwner;
	private String ideaTitle;
	private String ideaDomain;
	private String ideaPostedOn;
	private String ideaBudget;
	private String ideaDuration;
	private int ideaDays;
	//private boolean ideaAproved;
	//private boolean ideaSigned;
	//private boolean ideaCompletion;
	private int ideaFlag;
	private String ideaDescrption;
	private ArrayList<tb_idea> ideas = new ArrayList<tb_idea>();
	tb_idea idea=new tb_idea();
	tb_users user=new tb_users();
	UserDao userdao=new UserDao();

	
	
//	
	

	public int getProFlag() {
		return proFlag;
	}
	
	

	public int getProDays() {
		return proDays;
	}



	public void setProDays(int proDays) {
		this.proDays = proDays;
	}



	public int getTaskID() {
		return taskID;
	}
	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}
	public int getTaskDays() {
		return taskDays;
	}
	public void setTaskDays(int taskDays) {
		this.taskDays = taskDays;
	}
	public int getPrecedor() {
		return precedor;
	}
	public void setPrecedor(int precedor) {
		this.precedor = precedor;
	}
	public int getDin() {
		return din;
	}
	public void setDin(int din) {
		this.din = din;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public tb_idea getIdea() {
		return idea;
	}
	public void setIdea(tb_idea idea) {
		this.idea = idea;
	}
	public tb_users getUser() {
		return user;
	}
	public void setUser(tb_users user) {
		this.user = user;
	}
	public int getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}
	public void setProFlag(int proFlag) {
		this.proFlag = proFlag;
	}
	public int getTaskFlag() {
		return taskFlag;
	}
	public void setTaskFlag(int taskFlag) {
		this.taskFlag = taskFlag;
	}
	public ArrayList<tb_idea> getIdeas() {
		return ideas;
	}
	public void setIdeas(ArrayList<tb_idea> ideas) {
		this.ideas = ideas;
	}
	public int getIdeaOwner() {
		return ideaOwner;
	}
	public void setIdeaOwner(int ideaOwner) {
		this.ideaOwner = ideaOwner;
	}
	public String getIdeaTitle() {
		return ideaTitle;
	}
	public void setIdeaTitle(String ideaTitle) {
		this.ideaTitle = ideaTitle;
	}
	public String getIdeaDomain() {
		return ideaDomain;
	}
	public void setIdeaDomain(String ideaDomain) {
		this.ideaDomain = ideaDomain;
	}
	public String getIdeaPostedOn() {
		return ideaPostedOn;
	}
	public void setIdeaPostedOn(String ideaPostedOn) {
		this.ideaPostedOn = ideaPostedOn;
	}
	public String getIdeaBudget() {
		return ideaBudget;
	}
	public void setIdeaBudget(String ideaBudget) {
		this.ideaBudget = ideaBudget;
	}
	public String getIdeaDuration() {
		return ideaDuration;
	}
	public void setIdeaDuration(String ideaDuration) {
		this.ideaDuration = ideaDuration;
	}
	
	public int getIdeaFlag() {
		return ideaFlag;
	}
	public void setIdeaFlag(int ideaFlag) {
		this.ideaFlag = ideaFlag;
	}
	
	
	public String getIdeaDescrption() {
		return ideaDescrption;
	}
	public void setIdeaDescrption(String ideaDescrption) {
		this.ideaDescrption = ideaDescrption;
	}
	public int getProID() {
		return proID;
	}
	public void setProID(int proID) {
		this.proID = proID;
	}
	public int getProOwner() {
		return proOwner;
	}
	public void setProOwner(int proOwner) {
		this.proOwner = proOwner;
	}
	public int getProIdeaId() {
		return proIdeaId;
	}
	public void setProIdeaId(int proIdeaId) {
		this.proIdeaId = proIdeaId;
	}
	public String getProTitle() {
		return proTitle;
	}
	public void setProTitle(String proTitle) {
		this.proTitle = proTitle;
	}
	public String getProDomain() {
		return proDomain;
	}
	public void setProDomain(String proDomain) {
		this.proDomain = proDomain;
	}
	public String getProPostedOn() {
		return proPostedOn;
	}
	public void setProPostedOn(String proPostedOn) {
		this.proPostedOn = proPostedOn;
	}
	public String getProBudget() {
		return proBudget;
	}
	public void setProBudget(String proBudget) {
		this.proBudget = proBudget;
	}
	public String getProDuration() {
		return proDuration;
	}
	public void setProDuration(String proDuration) {
		this.proDuration = proDuration;
	}

	public String getProDescrption() {
		return proDescrption;
	}
	public void setProDescrption(String proDescrption) {
		this.proDescrption = proDescrption;
	}

	private ArrayList<tb_project> project = new ArrayList<tb_project>();
	
	projectDao projectService = new projectDao();

	public ArrayList<tb_project> getProject() {
		return project;
	}
	public void setProject(ArrayList<tb_project> project) {
		this.project = project;
	}
	
	public String pPost(){
		projectService.post(proIdeaId, proOwner,proTitle, proDescrption, 
				 proDomain, proPostedOn,
				 proBudget, proDuration, proFlag,proDays);
		this.project=projectService.myProjects(getProOwner());
		this.ideas=IdeaDao.myProjects(getProOwner());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "success";
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	taskDao tskDao=new taskDao();
	ideaDao IdeaDao=new ideaDao();

	public int getTaskOwner() {
	return taskOwner;
}

public void setTaskOwner(int taskOwner) {
	this.taskOwner = taskOwner;
}

public int getTaskProId() {
	return taskProId;
}

public void setTaskProId(int taskProId) {
	this.taskProId = taskProId;
}

public String getTaskTitle() {
	return taskTitle;
}

public void setTaskTitle(String taskTitle) {
	this.taskTitle = taskTitle;
}

public String getTaskDomain() {
	return taskDomain;
}

public void setTaskDomain(String taskDomain) {
	this.taskDomain = taskDomain;
}




public String getTaskBudget() {
	return taskBudget;
}

public void setTaskBudget(String taskBudget) {
	this.taskBudget = taskBudget;
}




public String getTaskPostedOn() {
	return taskPostedOn;
}
public void setTaskPostedOn(String taskPostedOn) {
	this.taskPostedOn = taskPostedOn;
}
public String getTaskDuration() {
	return taskDuration;
}
public void setTaskDuration(String taskDuration) {
	this.taskDuration = taskDuration;
}
public String getTaskDescrption() {
	return taskDescrption;
}

public void setTaskDescrption(String taskDescrption) {
	this.taskDescrption = taskDescrption;
}



private String currentDate;
public void setCurrentDate(String date){
	   this.currentDate = date;
	}
	public String getCurrentDate(){
	   return currentDate;
	}

	public String ideaTaskPost() throws ParseException{
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		   SimpleDateFormat ftt = new SimpleDateFormat ("yyyy-MM-dd");

		   setCurrentDate(ft.format(dNow));	
		   Calendar c = Calendar.getInstance(); 
			c.setTime(ft.parse(getTaskPostedOn())); 
			c.add(Calendar.DATE,getTaskDays()); 
			String convertedDate=ftt.format(c.getTime());
			
		   tskDao.ideaTaskPost( taskIdeaId, taskOwner, taskTitle,  taskDescrption,  taskDomain,  taskPostedOn,
			 taskBudget, convertedDate, taskFlag,precedor,taskDays);
	return "success";
}
	
	public String projectTaskPost() throws ParseException{
			Date dNow =new Date();
			   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
			   SimpleDateFormat ftt = new SimpleDateFormat ("yyyy-MM-dd");

			setCurrentDate(ft.format(dNow));

		Calendar c = Calendar.getInstance(); 
			c.setTime(ft.parse(getTaskPostedOn())); 
			c.add(Calendar.DATE,getTaskDays()); 
			String convertedDate=ftt.format(c.getTime());			
			//this.taskDuration = this.taskPostedOn.plusDays(this.getDin());
		tskDao.post( taskProId, taskOwner, taskTitle,  taskDescrption,  taskDomain,  taskPostedOn,
				 taskBudget,convertedDate, taskFlag,precedor,taskDays);
		return "success";
	}
 	
 	public String sendPrecedor() throws ParseException{
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));

tskDao.sendPrecedor(getTaskID(), getPrecedor());	

	return "success";
}
 	
 	
 	

 	
 	
 	
	
public ArrayList<ideaallbids> getAllBids() {
		return allBids;
	}
	public void setAllBids(ArrayList<ideaallbids> allBids) {
		this.allBids = allBids;
	}
public int getTaskIdeaId() {
		return taskIdeaId;
	}
	public void setTaskIdeaId(int taskIdeaId) {
		this.taskIdeaId = taskIdeaId;
	}
public String iPost(){
	int id=IdeaDao.ipost(ideaOwner, ideaTitle, ideaDescrption, ideaDomain, ideaPostedOn, ideaBudget, ideaDuration, ideaFlag,ideaDays);
	System.out.println(id);
	this.idea=ideaService.anIdea(id);
	this.allBids=ideaService.ideaAllBids(id);
	Date dNow =new Date();
	   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
	setCurrentDate(ft.format(dNow));
		return "success";
	}

ideaDao ideaService=new ideaDao();


public int getIdeaDays() {
	return ideaDays;
}



public void setIdeaDays(int ideaDays) {
	this.ideaDays = ideaDays;
}



public String myIdeas(){	
this.ideas=ideaService.myIdeas(getIdeaOwner()); 
Date dNow =new Date();
SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
setCurrentDate(ft.format(dNow));
return "success";
}

public String ideaCompleted(){	
	ideaService.ideaCompleted(getIdeaId(), getIdeaFlag());
	this.idea=ideaService.anIdea(getIdeaId());
	this.user=userdao.taskOwner(this.idea.getIdeaPM());
	this.rating=userdao.userRanking(this.idea.getIdeaPM());

return "success";
}
	
}
