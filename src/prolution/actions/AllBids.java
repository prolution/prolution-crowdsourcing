package prolution.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;

import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.dao.taskDao;
import prolution.entity.TaskAllBids;
import prolution.entity.availableIdea;
import prolution.entity.ideaallbids;
import prolution.entity.tb_idea;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;

public class AllBids {
	
	private ArrayList<ideaallbids> allBids = new ArrayList<ideaallbids>();
	private int ideaId;
	private int ideaFlag;
	private double rating;

	private ArrayList<TaskAllBids> taskBids = new ArrayList<TaskAllBids>();
	private int taskID;
	tb_tasks task=new tb_tasks();
	tb_idea idea=new tb_idea();
	tb_users user=new tb_users();
	
	UserDao userdao=new UserDao();
	ideaDao daoIdea=new ideaDao();

	taskDao daoTask=new taskDao();
	
	public String allBids(){
		this.idea=daoIdea.anIdea(getIdeaId());
		if(this.idea.getIdeaPM() ==0 ){
		this.allBids=daoIdea.ideaAllBids((getIdeaId()));
		return "success";
		}
		else {
			if(this.idea.getIdeaFlag() ==4 ){
			this.user=userdao.taskOwner(this.idea.getIdeaPM());
			this.rating=userdao.userRanking(this.idea.getIdeaPM());
			return "completed";
			}
			else {
				this.user=userdao.taskOwner(this.idea.getIdeaPM());
				return "input";
			}
		}
		}
	
	public String myCompletedIdea(){
		this.idea=daoIdea.anIdea(getIdeaId());
	
			this.user=userdao.taskOwner(this.idea.getIdeaPM());
			return "success";
		
		}
	
	
	
	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public tb_users getUser() {
		return user;
	}



	public void setUser(tb_users user) {
		this.user = user;
	}



	public String taskAllBids(){
		this.taskBids=daoTask.taskAllBids((getTaskID()));
		this.task=daoTask.projectTask(getTaskID());
		return "success";
		}
	
	
	
	public ArrayList<TaskAllBids> getTaskBids() {
		return taskBids;
	}



	public tb_tasks getTask() {
		return task;
	}

	public void setTask(tb_tasks task) {
		this.task = task;
	}

	public tb_idea getIdea() {
		return idea;
	}

	public void setIdea(tb_idea idea) {
		this.idea = idea;
	}

	public void setTaskBids(ArrayList<TaskAllBids> taskBids) {
		this.taskBids = taskBids;
	}



	public int getTaskID() {
		return taskID;
	}



	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}



	public ArrayList<ideaallbids> getAllBids() {
		return allBids;
	}
	public void setAllBids(ArrayList<ideaallbids> allBids) {
		this.allBids = allBids;
	}
	public int getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}
	
	
	public int getIdeaFlag() {
		return ideaFlag;
	}


	public void setIdeaFlag(int ideaFlag) {
		this.ideaFlag = ideaFlag;
	}
	

	/*new changes */
/*	public void acceptBid()
	{
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		ideaDao.acceptBids(request.getParameter("ideaId"),request.getParameter("ideaFlag"));
		//ideaDao.acceptBids(int.parseLong(request.getParameter("id")),request.getParameter("ideaFlag"));
		//return SUCCESS;
	}
*/	

}
