package prolution.actions;

import java.util.ArrayList;

import prolution.dao.projectDao;
import prolution.entity.tb_project;

public class DeleteProj {
	
	private int proID;
	private tb_project proj=new tb_project();
	private ArrayList<tb_project> project = new ArrayList<tb_project>();
    private Integer proOwner;

	

    
	public ArrayList<tb_project> getProject() {
		return project;
	}

	public void setProject(ArrayList<tb_project> project) {
		this.project = project;
	}

	public Integer getProOwner() {
		return proOwner;
	}

	public void setProOwner(Integer proOwner) {
		this.proOwner = proOwner;
	}

	public tb_project getProj() {
		return proj;
	}

	public void setProj(tb_project proj) {
		this.proj = proj;
	}

	
	public int getProID() {
		return proID;
	}

	public void setProID(int proID) {
		this.proID = proID;
	}


	projectDao projdao=new projectDao();
	
	public String execute() {
		// TODO Auto-generated method stub
	/*	projdao.deleteProj(getProID());*/	
		this.project=projdao.myProjects(getProOwner());
		return "SUCCESS";
	}
	

}
