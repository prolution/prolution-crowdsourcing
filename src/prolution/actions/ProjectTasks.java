package prolution.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.dao.taskDao;
import prolution.entity.tb_idea;
import prolution.entity.tb_project;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;

public class ProjectTasks {
	
	private tb_project proj=new tb_project();

	private tb_idea idea=new tb_idea();
	private int taskID;
	private String crit;	

	private int proID;
	private String ideaCommit;
	private int ideaId;
	private int rated;
	private int rater;
	private int rateTask;
	private int rateIdea;
	private int rating;
	private ArrayList<tb_tasks> tasks = new ArrayList<tb_tasks>();
	private String currentDate;
private int userid;
	tb_users user=new tb_users();
	
	UserDao daouser=new UserDao();
	private tb_tasks task=new tb_tasks();

	private ArrayList alist;
	
	
	public ArrayList getAlist() {
		return alist;
	}

	public void setAlist(ArrayList alist) {
		this.alist = alist;
	}

	public String getCrit() {
		return crit;
	}

	public void setCrit(String crit) {
		this.crit = crit;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getTaskID() {
		return taskID;
	}

	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}

	public tb_tasks getTask() {
		return task;
	}

	public void setTask(tb_tasks task) {
		this.task = task;
	}

	public int getRated() {
		return rated;
	}

	public void setRated(int rated) {
		this.rated = rated;
	}

	public int getRater() {
		return rater;
	}

	public void setRater(int rater) {
		this.rater = rater;
	}

	public int getRateTask() {
		return rateTask;
	}

	public void setRateTask(int rateTask) {
		this.rateTask = rateTask;
	}

	public int getRateIdea() {
		return rateIdea;
	}

	public void setRateIdea(int rateIdea) {
		this.rateIdea = rateIdea;
	}



	


	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public tb_users getUser() {
		return user;
	}

	public void setUser(tb_users user) {
		this.user = user;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	taskDao daoTask=new taskDao();

	

	public String getIdeaCommit() {
		return ideaCommit;
	}

	public void setIdeaCommit(String ideaCommit) {
		this.ideaCommit = ideaCommit;
	}

	public int getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}

	public ArrayList<tb_tasks> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<tb_tasks> tasks) {
		this.tasks = tasks;
	}

	public int getProID() {
		return proID;
	}

	public void setProID(int proID) {
		this.proID = proID;
	}

	public tb_project getProj() {
		return proj;
	}

	public void setProj(tb_project proj) {
		this.proj = proj;
	}

	projectDao projDao=new projectDao();
	
	/*public String execute(){
		this.proj=projDao.aproj(id);
		return "SUCCESS";
	}*/
	
	public String ideaUpload()  throws Exception{

		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "success";
	}
	
	public String projectTasks()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.proj = projDao.aproj(getProID());
		this.tasks=daoTask.projectTasks(getProID());
		//this.crit=daoTask.taskCriticalPath(getProID());	
		this.alist=daoTask.taskCriticalPath(getProID());	

		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "SUCCESS";
	}	
	public String ideaTasks()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.idea = daoIdea.anIdea(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		this.tasks=daoTask.ideaTasks(getIdeaId());
    	this.rating=(int) daouser.userRanking(getUserid());
		//this.crit=daoTask.ideaTaskCriticalPath(getIdeaId());	
		this.alist=daoTask.ideaTaskCriticalPath(getIdeaId());	

		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
if(this.idea.getIdeaFlag()==4){
	return "hallo";}
else {		return "SUCCESS";}	
}
	
	public tb_idea getIdea() {
		return idea;
	}

	public void setIdea(tb_idea idea) {
		this.idea = idea;
	}

	ideaDao daoIdea=new ideaDao();
	

	
	public String myEnProfile()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.user=daouser.projectOwner(getUserid());
    	this.rating=(int) daouser.userRanking(getUserid());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));

		return "SUCCESS";
	}
	
	public String myCnProfile()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.user=daouser.projectOwner(getUserid());
    	this.rating=(int) daouser.userRanking(getUserid());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));

		return "SUCCESS";
	}
	
	public String myPmProfile()  throws Exception{
		//System.out.println(student.getRegno());
		/*if((this.proj != null) && (this.proj.getProID() != 0)){
			this.proj = projDao.aproj();
		}*/
		this.user=daouser.projectOwner(getUserid());
    	this.rating=(int) daouser.userRanking(getUserid());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));

		return "SUCCESS";
	}
	
	public String rateEn(){		
		this.idea = daoIdea.anIdea(getIdeaId());
		daouser.rateUser(getRating(), getIdeaId(), getRateTask(), this.idea.getIdeaOwner(), getRater());
		this.tasks=daoTask.ideaTasks(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}
	
	public String rateCn(){	
		this.task = daoTask.projectTask(getTaskID());
		daouser.rateUser(getRating(), getRateIdea(), getTaskID(), this.task.getTaskOwner(), getRater());
		this.user=daouser.taskOwner(this.task.getTaskOwner());

		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}
	
	public String ratePm(){		
		this.idea = daoIdea.anIdea(getIdeaId());
		daouser.rateUser(getRating(), getIdeaId(), getRateTask(), this.idea.getIdeaPM(), getRater());
		this.user=daouser.taskOwner(this.idea.getIdeaPM());

		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}
	
	public String CnratePm(){		
		this.task = daoTask.projectTask(getTaskID());
		if(this.task.getTaskProId() !=0){
			daouser.rateUser(getRating(), getRateIdea(), getTaskID(), projDao.taskProOwnerId(this.task.getTaskProId()), getRater());
			this.user=daouser.projectOwner(projDao.taskProOwnerId(this.task.getTaskProId()));
			}
			else
			{
				daouser.rateUser(getRating(), getRateIdea(), getTaskID(), daoIdea.proOwnerId(this.task.getTaskIdeaId()), getRater());
				this.user=daouser.projectOwner(daoIdea.proOwnerId(this.task.getTaskIdeaId()));
			}
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}
	
	public String p2eMsg(){		
		daoIdea.e2pMsg(getIdeaId(), getIdeaCommit());
		this.idea = daoIdea.anIdea(getIdeaId());
		this.tasks=daoTask.ideaTasks(getIdeaId());
		this.user=daouser.projectOwner(this.idea.getIdeaOwner());
		Date dNow =new Date();
		   SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
		setCurrentDate(ft.format(dNow));
		return "input";
}

}
