package prolution.actions;

import prolution.dao.UserDao;
import prolution.dao.ideaDao;
import prolution.dao.projectDao;
import prolution.dao.taskDao;
import prolution.entity.tb_idea;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;

public class Assign {
	
	private String proTitle;
	private String proBudget;
	private String proDuration;
	private String proDomain;
	private String proPostedOn;
	private String proDescrption;
	private int proOwner;
	private int proIdeaId;
	private int proFlag;
	private int ideaFlag;
	private int taskFlag;
	private int taskOwner;
	private int taskID;
	private int bidsAmount;
	private String ideaCommit;
	private int ideaId;
	private int userid;
	private double rating;

	
	
	
	public int getBidsAmount() {
		return bidsAmount;
	}

	public void setBidsAmount(int bidsAmount) {
		this.bidsAmount = bidsAmount;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	private int ideaPM;
tb_tasks task=new tb_tasks();
	UserDao userdao=new UserDao();
	taskDao DaoTask=new taskDao();
tb_users user=new tb_users();
	tb_idea idea=new tb_idea();


	
	
	public String getIdeaCommit() {
		return ideaCommit;
	}

	public void setIdeaCommit(String ideaCommit) {
		this.ideaCommit = ideaCommit;
	}

	public tb_idea getIdea() {
		return idea;
	}

	public void setIdea(tb_idea idea) {
		this.idea = idea;
	}

	public tb_tasks getTask() {
	return task;
}

public void setTask(tb_tasks task) {
	this.task = task;
}

public tb_users getUser() {
	return user;
}

public void setUser(tb_users user) {
	this.user = user;
}

	public int getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}

	public int getTaskID() {
		return taskID;
	}

	public void setTaskID(int taskID) {
		this.taskID = taskID;
	}

	public int getTaskFlag() {
		return taskFlag;
	}

	public void setTaskFlag(int taskFlag) {
		this.taskFlag = taskFlag;
	}

	public int getTaskOwner() {
		return taskOwner;
	}

	public void setTaskOwner(int taskOwner) {
		this.taskOwner = taskOwner;
	}

	public int getProFlag() {
		return proFlag;
	}

	public void setProFlag(int proFlag) {
		this.proFlag = proFlag;
	}

	public int getIdeaFlag() {
		return ideaFlag;
	}

	public void setIdeaFlag(int ideaFlag) {
		this.ideaFlag = ideaFlag;
	}

	ideaDao daoIdea=new ideaDao();
	projectDao projdao=new projectDao();



	public int getProOwner() {
		return proOwner;
	}

	public void setProOwner(int proOwner) {
		this.proOwner = proOwner;
	}

	public int getProIdeaId() {
		return proIdeaId;
	}

	public void setProIdeaId(int proIdeaId) {
		this.proIdeaId = proIdeaId;
	}



	public String getProTitle() {
		return proTitle;
	}




	public void setProTitle(String proTitle) {
		this.proTitle = proTitle;
	}




	public String getProBudget() {
		return proBudget;
	}




	public void setProBudget(String proBudget) {
		this.proBudget = proBudget;
	}




	public String getProDuration() {
		return proDuration;
	}




	public void setProDuration(String proDuration) {
		this.proDuration = proDuration;
	}




	public String getProDomain() {
		return proDomain;
	}




	public void setProDomain(String proDomain) {
		this.proDomain = proDomain;
	}




	public String getProPostedOn() {
		return proPostedOn;
	}




	public void setProPostedOn(String proPostedOn) {
		this.proPostedOn = proPostedOn;
	}




	public String getProDescrption() {
		return proDescrption;
	}




	public int getIdeaPM() {
		return ideaPM;
	}

	public void setIdeaPM(int ideaPM) {
		this.ideaPM = ideaPM;
	}

	public void setProDescrption(String proDescrption) {
		this.proDescrption = proDescrption;
	}
	
	
	public String assignTask(){
		DaoTask.assignTask(taskID, taskFlag, taskOwner);
		this.task = DaoTask.projectTask(getTaskID());
		this.user=userdao.taskOwner(this.task.getTaskOwner());
		return "success";
	}

	public String assignIdea(){
		daoIdea.acceptBids(ideaId,ideaFlag,ideaPM);
		this.idea=daoIdea.anIdea(getIdeaId());
		this.user=userdao.projectOwner(this.idea.getIdeaPM());
		return "success";
	}
	
	public String ideaBidder(){
		this.idea=daoIdea.anIdea(getIdeaId());
		this.user=userdao.projectOwner(getUserid());
    	this.rating=userdao.userRanking(getUserid());
    	this.bidsAmount=this.getBidsAmount();
		return "success";
	}
	
	public String e2pMsg(){		
		daoIdea.e2pMsg(getIdeaId(), getIdeaCommit());
		this.idea=daoIdea.anIdea(getIdeaId());
		this.user=userdao.projectOwner(this.idea.getIdeaPM());
		return "input";
}
	
}
