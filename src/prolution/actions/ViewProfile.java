package prolution.actions;

import java.util.ArrayList;








import prolution.dao.UserDao;
import prolution.entity.viewCndetail;
import prolution.entity.viewEndetail;
import prolution.entity.viewPmdetail;

public class ViewProfile {
	
	private ArrayList<viewPmdetail> viewprofile = new ArrayList<viewPmdetail>();
	
	private int userid;
	private String usertype;
	private String username;
	UserDao userdao=new UserDao();

	public void viewprofile(){
		this.viewprofile = userdao.viewprofile((getUserid()));
		}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public ArrayList<viewPmdetail> getViewprofile() {
		return viewprofile;
	}

	public void setViewprofile(ArrayList<viewPmdetail> viewprofile) {
		this.viewprofile = viewprofile;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private ArrayList<viewCndetail> viewcnprofile = new ArrayList<viewCndetail>();
	
	
	
	private ArrayList<viewEndetail> viewenprofile = new ArrayList<viewEndetail>();
	
	
	public ArrayList<viewEndetail> getViewenprofile() {
		return viewenprofile;
	}

	public void setViewenprofile(ArrayList<viewEndetail> viewenprofile) {
		this.viewenprofile = viewenprofile;
	}

	public ArrayList<viewCndetail> getViewcnprofile() {
		return viewcnprofile;
	}

	public void setViewcnprofile(ArrayList<viewCndetail> viewcnprofile) {
		this.viewcnprofile = viewcnprofile;
	}

	
	
	public void viewenprofile(){
		this.viewenprofile = userdao.viewenprofile((getUserid()));
		}
	
	
	public void viewcnprofile(){
		this.viewcnprofile = userdao.viewcnprofile((getUserid()));
		}
	
	public String checkprofile(){

		String str = userdao.rtype(getUsername());
			
			if(str.equals("pm")){
				viewprofile();
				return "p";
			}
        	
        	else if(str.equals("cn")){
        		viewcnprofile();
        		return "c";
        	}
        	else if(str.equals("en"))
        		viewenprofile();
        	return "e";
        	
		
	}  
	
}
