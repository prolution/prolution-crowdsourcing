package prolution.actions;
import java.util.ArrayList;

import prolution.dao.taskDao;
import prolution.entity.availabletasks;


public class GetAvailTasks {
	
	private ArrayList<availabletasks> tasks = new ArrayList<availabletasks>();

	taskDao taskdao=new taskDao();
	private int bidsUser;
	
	
	
	public String allTasks(){
		this.tasks=taskdao.allTasks(getBidsUser());
		return "success";
		}
	
	public String cnBids(){
		this.tasks=taskdao.cnBids(getBidsUser());
		return "success";
		}



	public ArrayList<availabletasks> getTasks() {
		return tasks;
	}



	public void setTasks(ArrayList<availabletasks> tasks) {
		this.tasks = tasks;
	}



	public int getBidsUser() {
		return bidsUser;
	}



	public void setBidsUser(int bidsUser) {
		this.bidsUser = bidsUser;
	}
	
	
	

}


