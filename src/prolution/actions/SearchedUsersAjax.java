package prolution.actions;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.opensymphony.xwork2.ActionSupport;





import prolution.dao.UserDao;
import prolution.entity.view_users;


@ParentPackage("json-default")
public class SearchedUsersAjax extends ActionSupport{
	private String jsonRequestdata;
	
	private ArrayList<view_users> users  = new ArrayList<view_users>();
	UserDao daoUser=new UserDao();
	private int userid;
	private static final long serialVersionUID = 1L;

	private BigDecimal rating;

	private String username;
	
	private String fullName;
	
	private String country;
	
	private String email;
	
	private String usertype;

	private String userstatus;
	
	
	public String getJsonRequestdata() {
		return jsonRequestdata;
	}


	public void setJsonRequestdata(String jsonRequestdata) {
		this.jsonRequestdata = jsonRequestdata;
	}


	public ArrayList<view_users> getUsers() {
		return users;
	}


	public void setUsers(ArrayList<view_users> users) {
		this.users = users;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public BigDecimal getRating() {
		return rating;
	}


	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}


	public String getUserstatus() {
		return userstatus;
	}


	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
    @Action(value="searchedUsersAjax", 
        results = { @Result(name = "success", type = "json") })
	public String execute() throws ParseException {
		JSONObject json = (JSONObject)new JSONParser()
        .parse(jsonRequestdata);
		
		setUsers(daoUser.viewAllSearchingUsers((String)json.get("str")));
		return ActionSupport.SUCCESS;	}

}
