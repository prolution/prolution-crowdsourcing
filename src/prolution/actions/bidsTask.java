package prolution.actions;

import java.util.ArrayList;

import prolution.dao.taskDao;
import prolution.entity.availabletasks;

public class bidsTask {
	
	private  int bidsUser;
	private int bidsTask;
	private int bidsFlag;
	private int bidsAmount;

	private ArrayList<availabletasks> tasks = new ArrayList<availabletasks>();
	
	taskDao daoTask=new taskDao();
	
	public String execute(){
		daoTask.bidsTask(  getBidsUser(), getBidsTask(),  getBidsFlag(),getBidsAmount());
		this.tasks=daoTask.allTasks(getBidsUser());
		return "success";
	}
	
	
	
	public int getBidsAmount() {
		return bidsAmount;
	}



	public void setBidsAmount(int bidsAmount) {
		this.bidsAmount = bidsAmount;
	}



	public int getBidsUser() {
		return bidsUser;
	}
	public void setBidsFlag(int bidsFlag) {
		this.bidsFlag = bidsFlag;
	}
	public void setBidsUser(int bidsUser) {
		this.bidsUser = bidsUser;
	}
	public int getBidsTask() {
		return bidsTask;
	}
	public void setBidsTask(int bidsTask) {
		this.bidsTask = bidsTask;
	}
	
	public int getBidsFlag() {
		return bidsFlag;
	}
	public void setBidFlag(int bidsFlag) {
		this.bidsFlag = bidsFlag;
	}
	public ArrayList<availabletasks> getTasks() {
		return tasks;
	}
	public void setTasks(ArrayList<availabletasks> tasks) {
		this.tasks = tasks;
	}
	
	

}
