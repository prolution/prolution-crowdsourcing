package prolution.actions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

import prolution.dao.UserDao;
import prolution.entity.availableIdea;
import prolution.entity.view_users;

public class ViewUsers {
	
	private ArrayList<view_users> users  = new ArrayList<view_users>();
UserDao daoUser=new UserDao();
	
	public ArrayList<view_users> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<view_users> users) {
		this.users = users;
	}


	private int userid;

	private BigDecimal rating;

	private String username;
	
	private String fullName;
	
	private String country;
	
	private String email;
	
	private String usertype;

	private String userstatus;

	

	



	


	



	

	public BigDecimal getRating() {
		return rating;
	}

	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getUserstatus() {
		return userstatus;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}
	
	
	public String execute() {
		this.users=daoUser.viewAllUsers();
		return "success";
	}

}
