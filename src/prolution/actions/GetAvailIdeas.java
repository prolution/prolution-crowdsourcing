package prolution.actions;

import java.util.ArrayList;

import prolution.dao.ideaDao;
import prolution.entity.availableIdea;

public class GetAvailIdeas {
	private ArrayList<availableIdea> ideas = new ArrayList<availableIdea>();

	ideaDao ideadao=new ideaDao();
	private int bidsUser;

	
	
	
	public int getBidsUser() {
		return bidsUser;
	}


	public void setBidsUser(int bidsUser) {
		this.bidsUser = bidsUser;
	}


	public String allIdeas(){
	this.ideas=ideadao.allIdeas(getBidsUser());
	return "success";
	}
	
	public String myBids(){
		this.ideas=ideadao.myBids(getBidsUser());
		return "success";
		}


	public ArrayList<availableIdea> getIdeas() {
		return ideas;
	}


	public void setIdeas(ArrayList<availableIdea> ideas) {
		this.ideas = ideas;
	}
	
	
}
