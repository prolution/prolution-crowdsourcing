package prolution.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import prolution.entity.tb_idea;
import prolution.entity.tb_project;
import prolution.entity.tb_users;

public class projectDao {
	
	public boolean post(int proIdeaId,int proOwner,String proTitle, String proDescrption, 
			String proDomain, String proPostedOn,
			String proBudget,String proDuration,int proFlag, int proDays) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
       
        tb_project proj=new tb_project();
        proj.setProFlag(proFlag);
       proj.setProBudget(proBudget);
        
        proj.setProDescrption(proDescrption);
        proj.setProDomain(proDomain);
        proj.setProDuration(proDuration);
        proj.setProIdeaId(proIdeaId);
        proj.setProOwner(proOwner);
        proj.setProPostedOn(proPostedOn);
        proj.setProDays(proDays);
        
        proj.setProTitle(proTitle);
        
        
        session.save(proj);
        session.getTransaction().commit();
        session.close();
        return true;
    }
	
	@SuppressWarnings("unchecked")
    public ArrayList<tb_project> allproj() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_project";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_project> objcoll=new ArrayList<>();
        List<tb_project> list = query.list();
        for (int i =0; i<list.size(); i++) {
            tb_project proj=new tb_project();
            
            proj.setProBudget(list.get(i).getProBudget());
            proj.setProDescrption(list.get(i).getProDescrption());
            proj.setProDomain(list.get(i).getProDomain());
            proj.setProDuration(list.get(i).getProDuration());
            proj.setProID(list.get(i).getProID());
            proj.setProIdeaId(list.get(i).getProIdeaId());
            proj.setProOwner(list.get(i).getProOwner());
            proj.setProPostedOn(list.get(i).getProPostedOn());
            proj.setProTitle(list.get(i).getProTitle());
            proj.setProDays(list.get(i).getProDays());
            
        	objcoll.add(proj);
        }
        
        
        /*while(it.hasNext()){
            project proj=new project();
            proj.setProjectManager(it.projectManager);
            proj.setProjectName(projectName);
            proj.setSubmissionDate(submissionDate);
        }*/
        
        session.close();
        return objcoll;

    }
    
    @SuppressWarnings("unchecked")
    public tb_project aproj(int id) {
        tb_project proj=new tb_project(); 
    	try {
    	Session session = HibernateUtil.getSessionFactory().openSession();
       proj=(tb_project)session.get(tb_project.class, id);
       session.close();
    	 } catch (Exception e) {      

    }
         return proj;

}
    
/*    @Transactional
	public void updateProj(int id,int proIdeaId,int proOwner,String proTitle, String proDescrption, 
			String proDomain, String proPostedOn,
			String proBudget,String proDuration,int proFlag) {
    tb_project proj=new tb_project(); 
	
	Session session = HibernateUtil.getSessionFactory().openSession();
	Transaction tx = null;
	try {
	tx = session.beginTransaction();
	session.beginTransaction();
	proj=(tb_project)session.get(tb_project.class, id);
	proj.setProFlag(proFlag);
	proj.setProBudget(proBudget);
	
	proj.setProDescrption(proDescrption);
	proj.setProDomain(proDomain);
	proj.setProDuration(proDuration);
	proj.setProIdeaId(proIdeaId);
	proj.setProOwner(proOwner);
	proj.setProPostedOn(proPostedOn);
	proj.setProSigned(proSigned);
	proj.setProTitle(proTitle);
   
   session.update(proj);
   
   tx.commit();
	 } catch (HibernateException e) {
    if (tx!=null) tx.rollback();
    e.printStackTrace(); 
 }finally {
    session.close(); 
 }

}
    
    
    	@Transactional
    	public void deleteProj(Integer id) {
        tb_project proj=new tb_project(); 
    	
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	Transaction tx = null;
    	try {
    	tx = session.beginTransaction();
    	session.beginTransaction();
    	proj=(tb_project)session.get(tb_project.class, id);
       session.delete(proj);
       tx.commit();
    	 } catch (HibernateException e) {
        if (tx!=null) tx.rollback();
        e.printStackTrace(); 
     }finally {
        session.close(); 
     }
    	}*/
    	
    	
/*    	public void deleteProj(Integer id) {
        tb_project proj=new tb_project(); 
    	
    	Session session = HibernateUtil.getSessionFactory().openSession();
        String SQL_QUERY = "delete from tb_project p where p.proID='" + id + "'";
        Query query = session.createQuery(SQL_QUERY);
        query.executeUpdate();
}*/
    	
    	@SuppressWarnings("unchecked")
        public ArrayList<tb_project> myProjects(int proOwner) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            //session.beginTransaction();
            String SQL_QUERY = "from tb_project p where p.proIdeaId='0' and p.proOwner='" + proOwner + "'";
            System.out.println(SQL_QUERY);
            Query query = session.createQuery(SQL_QUERY);
            //session.get(getClass(), arg1)
            //Iterator<project> it = query.iterate();      
            ArrayList<tb_project> objcoll=new ArrayList<>();
            List<tb_project> list = query.list();
            for (int i =0; i<list.size(); i++) {
                tb_project proj=new tb_project();
                
                proj.setProBudget(list.get(i).getProBudget());
                proj.setProDescrption(list.get(i).getProDescrption());
                proj.setProDomain(list.get(i).getProDomain());
                proj.setProDuration(list.get(i).getProDuration());
                proj.setProID(list.get(i).getProID());
                proj.setProIdeaId(list.get(i).getProIdeaId());
                proj.setProOwner(list.get(i).getProOwner());
                proj.setProPostedOn(list.get(i).getProPostedOn());
                proj.setProTitle(list.get(i).getProTitle());
                proj.setProDays(list.get(i).getProDays());
            	objcoll.add(proj);
            }                         
            session.close();
            return objcoll;

        }
        
        @SuppressWarnings("unchecked")
        public int taskProOwnerId(int id) {
            tb_project proj=new tb_project(); 
        	int ownerid=0;
            try {
        	Session session = HibernateUtil.getSessionFactory().openSession();
           proj=(tb_project)session.get(tb_project.class, id);
           ownerid=proj.getProOwner();
           session.close();
        	 } catch (Exception e) {      

        }
             return ownerid;

    }
    

}
