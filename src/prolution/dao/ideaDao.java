package prolution.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;




/*import com.googlecode.s2hibernate.struts2.plugin.annotations.SessionTarget;
import com.googlecode.s2hibernate.struts2.plugin.annotations.TransactionTarget;*/
import prolution.entity.availableIdea;
import prolution.entity.searchedideas;
import prolution.entity.tb_bids;
import prolution.entity.tb_idea;
import prolution.entity.ideaallbids;
import prolution.entity.tb_project;
import prolution.entity.tb_tasks;
import prolution.entity.view_users;

public class ideaDao {
	
	public boolean post(int ideaOwner,String ideaTitle, String ideaDescrption, 
			String ideaDomain, String ideaPostedOn,
			String ideaBudget,String ideaDuration,int ideaFlag,int ideaDays ) 
	{
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
       
        tb_idea ide=new tb_idea();
        
        ide.setIdeaBudget(ideaBudget);
        
        ide.setIdeaDescrption(ideaDescrption);
        ide.setIdeaDomain(ideaDomain);
        ide.setIdeaDuration(ideaDuration);
        ide.setIdeaOwner(ideaOwner);
        ide.setIdeaPostedOn(ideaPostedOn);
        ide.setIdeaFlag(ideaFlag);
        ide.setIdeaTitle(ideaTitle);
        ide.setIdeaDays(ideaDays);
        
        session.save(ide);
        session.getTransaction().commit();
        session.close();
        return true;
    }
	
	public int ipost(int ideaOwner,String ideaTitle, String ideaDescrption, 
			String ideaDomain, String ideaPostedOn,
			String ideaBudget,String ideaDuration,int ideaFlag, int ideaDays ) 
	{
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
       
        tb_idea ide=new tb_idea();
        
        ide.setIdeaBudget(ideaBudget);
        
        ide.setIdeaDescrption(ideaDescrption);
        ide.setIdeaDomain(ideaDomain);
        ide.setIdeaDuration(ideaDuration);
        ide.setIdeaOwner(ideaOwner);
        ide.setIdeaPostedOn(ideaPostedOn);
        ide.setIdeaFlag(ideaFlag);
        ide.setIdeaTitle(ideaTitle);
       ide.setIdeaDays(ideaDays);
        Integer id =(Integer) session.save(ide);
        //int id = (Integer) ide.getIdeaId(); 

//       int id = (Integer) session.save(ide); 
        session.getTransaction().commit();
        session.close();
        return id;
    }
	
	
	@SuppressWarnings("unchecked")
    public ArrayList<tb_idea> myIdeas(int ideaOwner) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_idea i where i.ideaOwner='" + ideaOwner + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_idea> objcoll=new ArrayList<>();
        List<tb_idea> list = query.list();
        for (int i =0; i<list.size(); i++) {
            tb_idea idea=new tb_idea();
            
            idea.setIdeaBudget(list.get(i).getIdeaBudget());
            idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
            idea.setIdeaDomain(list.get(i).getIdeaDomain());
            idea.setIdeaDuration(list.get(i).getIdeaDuration());
            idea.setIdeaId(list.get(i).getIdeaId());
            idea.setIdeaOwner(list.get(i).getIdeaOwner());
            idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
            idea.setIdeaTitle(list.get(i).getIdeaTitle());
            idea.setIdeaFlag(list.get(i).getIdeaFlag());
            idea.setIdeaDays(list.get(i).getIdeaDays());
            
        	objcoll.add(idea);
        }                         
        session.close();
        return objcoll;
    }
    
	@SuppressWarnings("unchecked")
    public ArrayList<tb_idea> completedIdeas(int ideaOwner) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_idea i where i.ideaOwner='" + ideaOwner + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_idea> objcoll=new ArrayList<>();
        List<tb_idea> list = query.list();
        for (int i =0; i<list.size(); i++) {
            tb_idea idea=new tb_idea();
            
            idea.setIdeaBudget(list.get(i).getIdeaBudget());
            idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
            idea.setIdeaDomain(list.get(i).getIdeaDomain());
            idea.setIdeaDuration(list.get(i).getIdeaDuration());
            idea.setIdeaId(list.get(i).getIdeaId());
            idea.setIdeaOwner(list.get(i).getIdeaOwner());
            idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
            idea.setIdeaTitle(list.get(i).getIdeaTitle());
            idea.setIdeaDays(list.get(i).getIdeaDays());
            idea.setIdeaFlag(list.get(i).getIdeaFlag());
            
        	objcoll.add(idea);
        }                         
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<availableIdea> allIdeas(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from availableIdea f WHERE f.ideaId NOT IN (SELECT b.bidsIdea FROM tb_bids b WHERE b.bidsUser='" +id+"')";

        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<availableIdea> objcoll=new ArrayList<>();
        List<availableIdea> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	availableIdea idea=new availableIdea();
        	
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;

    } 
    
    @SuppressWarnings("unchecked")
    public ArrayList<availableIdea> myBids(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from availableIdea f WHERE f.ideaId IN (SELECT b.bidsIdea FROM tb_bids b WHERE b.bidsUser='" +id+"')";

        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<availableIdea> objcoll=new ArrayList<>();
        List<availableIdea> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	availableIdea idea=new availableIdea();
        	
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;

    } 
    
    public  boolean bidsIdea(int  bidsUser,int bidsIdea,int  bidsFlag,int  bidsAmount)
    {
    	 Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
          
         tb_bids bid=new tb_bids();
         bid.setBidsIdea(bidsIdea);
         bid.setBidsFlag(bidsFlag);
         bid.setBidsUser(bidsUser);
         bid.setBidsAmount(bidsAmount);
         
         session.save(bid);
         session.getTransaction().commit();
         session.close();
    	
    	return true;
    }
    
    
    @SuppressWarnings("unchecked")
    public ArrayList<ideaallbids> ideaAllBids(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from ideaallbids f WHERE (f.ideaId='" +id+ "')";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<ideaallbids> objcoll=new ArrayList<>();
        List<ideaallbids> list = query.list();
        if (list.size()>0){
        for (int i =0; i<list.size(); i++) {
        	ideaallbids ideaallBids=new ideaallbids();
        	
        	ideaallBids.setCountry(list.get(i).getCountry());
        	ideaallBids.setFirstname(list.get(i).getFirstname());
        	ideaallBids.setLastname(list.get(i).getLastname());
        	ideaallBids.setUsername(list.get(i).getUsername());
        	ideaallBids.setIdeaBudget(list.get(i).getIdeaBudget());
        	ideaallBids.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	ideaallBids.setIdeaDomain(list.get(i).getIdeaDomain());
        	ideaallBids.setIdeaDuration(list.get(i).getIdeaDuration());
        	ideaallBids.setIdeaId(list.get(i).getIdeaId());
        	ideaallBids.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	ideaallBids.setIdeaTitle(list.get(i).getIdeaTitle());
        	ideaallBids.setUserid(list.get(i).getUserid());      	
        	//* new changes made here /
        	ideaallBids.setUsername(list.get(i).getUsername()); 
        	ideaallBids.setBidsAmount(list.get(i).getBidsAmount());
        	
        	objcoll.add(ideaallBids);
        }
        }
        session.close();
        return objcoll;

    }
    
    
    ///new changes to the code for bid acceptance 
    //public  void acceptBids(int  id,int ideaFlag)
    /*public  void acceptBids(int ideaId,int ideaFlag)
    {
    	
    	 tb_idea idea=new tb_idea(); 
    	
    		Session session = HibernateUtil.getSessionFactory().openSession();
    		Transaction tx = null;
    		try {
    		tx = session.beginTransaction();
    		session.beginTransaction();
    		idea=(tb_idea)session.get(tb_idea.class, ideaId);
    		idea.setIdeaFlag(ideaFlag);
    		session.update(idea);
    		   
    		   tx.commit();
    			 } catch (HibernateException e) {
    		    if (tx!=null) tx.rollback();
    		    e.printStackTrace(); 
    		 }finally {
    		    session.close(); 
    		 }

    }*/

    
    
    
/*    @Transactional
    public  void acceptBids(int ideaId,int ideaFlag)
    {
    	 tb_idea idea=new tb_idea(); 
  	
  	Session session = HibernateUtil.getSessionFactory().openSession();
  Transaction tx = null;
  	
  	tx = session.beginTransaction();
  	session.beginTransaction();
  	//String hql = "UPDATE tb_idea set ideaFlag = :ideaFlag" + "WHERE ideaId = :ideaId";
  	String hql = "UPDATE tb_idea set ideaFlag='"+ideaFlag+"' WHERE ideaId='" + ideaId + "'";
  	//String hql = "UPDATE Employee set salary = :salary "  + "WHERE id = :employee_id";
  	Query query = session.createQuery(hql);
  	idea.setIdeaFlag(ideaFlag);

  	query.setParameter("ideaFlag", ideaFlag);
  	query.setParameter("ideaId", ideaId);
  	
  	
  	int result=query.executeUpdate(); 
  		session.update(idea);
  	
  	//int result = query.executeUpdate();
  	//System.out.println("Rows affected: " + result);
    
  	String  = "UPDATE Employee set salary = :salary "  + "WHERE id = :employee_id";
  	query.setParameter("salary", 1000);
	
	query.setParameter("employee_id", 10);
	
	System.out.println("Rows affected: " + result);   
    session.getTransaction().commit();

  	session.close();

  }*/
    
    /*NEW CHANGES 13 MARCH 2015*/

    public  void acceptBids(Integer ideaId,int ideaFlag, int ideaPM)
    {  	
  	Session session = HibernateUtil.getSessionFactory().openSession();
  Transaction tx = null;
 
  try{
     tx = session.beginTransaction();
     tb_idea idea = (tb_idea)session.get(tb_idea.class, ideaId); 
     idea.setIdeaPM(ideaPM);
	idea.setIdeaFlag(ideaFlag); 
  session.update(idea); 
  tx.commit();
}catch (HibernateException e) {
  if (tx!=null) tx.rollback();
  e.printStackTrace(); 
}finally {
  session.close(); 
}}
    
    public  void ideaCompleted(Integer ideaId,int ideaFlag)
    {  	
  	Session session = HibernateUtil.getSessionFactory().openSession();
  Transaction tx = null;
 
  try{
     tx = session.beginTransaction();
     tb_idea idea = (tb_idea)session.get(tb_idea.class, ideaId); 
	idea.setIdeaFlag(ideaFlag); 
  session.update(idea); 
  tx.commit();
}catch (HibernateException e) {
  if (tx!=null) tx.rollback();
  e.printStackTrace(); 
}finally {
  session.close(); 
}}
  
    @SuppressWarnings("unchecked")
    public ArrayList<searchedideas> searchedIdeas( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
       // String SQL_QUERY = "from searchedideas";

        //System.out.println(SQL_QUERY);
        //Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedideas> objcoll=new ArrayList<>();
       // List<searchedideas> list = query.list();
        List<searchedideas> list = session.createCriteria(searchedideas.class).list();

        for (int i =0; i<list.size(); i++) {
        	searchedideas idea=new searchedideas();
        	
        	idea.setNoOfBids(list.get(i).getNoOfBids());
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;
    } 
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedideas> searchedWebIdeas( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedideas f WHERE f.ideaDomain='web'";

        //System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedideas> objcoll=new ArrayList<>();
        List<searchedideas> list = query.list();
        //List<searchedideas> list = session.createCriteria(searchedideas.class).list();

        
        for (int i =0; i<list.size(); i++) {
        	searchedideas idea=new searchedideas();
        	
        	idea.setNoOfBids(list.get(i).getNoOfBids());
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;

    } 
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedideas> searchedIosIdeas( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedideas f WHERE f.ideaDomain='IOS'";

        //System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedideas> objcoll=new ArrayList<>();
        List<searchedideas> list = query.list();
        //List<searchedideas> list = session.createCriteria(searchedideas.class).list();

        
        for (int i =0; i<list.size(); i++) {
        	searchedideas idea=new searchedideas();
        	
        	idea.setNoOfBids(list.get(i).getNoOfBids());
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;

    } 
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedideas> searchedAndroidIdeas( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedideas f WHERE f.ideaDomain='Android'";

        //System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedideas> objcoll=new ArrayList<>();
        List<searchedideas> list = query.list();
        //List<searchedideas> list = session.createCriteria(searchedideas.class).list();

        
        for (int i =0; i<list.size(); i++) {
        	searchedideas idea=new searchedideas();
        	
        	idea.setNoOfBids(list.get(i).getNoOfBids());
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;

    } 
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedideas> searchedDesktopIdeas( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedideas f WHERE f.ideaDomain='Desktop'";

        //System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedideas> objcoll=new ArrayList<>();
        List<searchedideas> list = query.list();
        //List<searchedideas> list = session.createCriteria(searchedideas.class).list();

        
        for (int i =0; i<list.size(); i++) {
        	searchedideas idea=new searchedideas();
        	
        	idea.setNoOfBids(list.get(i).getNoOfBids());
        	idea.setIdeaBudget(list.get(i).getIdeaBudget());
        	idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
        	idea.setIdeaDomain(list.get(i).getIdeaDomain());
        	idea.setIdeaDuration(list.get(i).getIdeaDuration());
        	idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
        	idea.setIdeaTitle(list.get(i).getIdeaTitle());
        	idea.setIdeaId(list.get(i).getIdeaId());
          
        	objcoll.add(idea);
        }       
        session.close();
        return objcoll;

    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_idea> myProjects(int ideaPM) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_idea i where i.ideaPM='" + ideaPM + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_idea> objcoll=new ArrayList<>();
        List<tb_idea> list = query.list();
        for (int i =0; i<list.size(); i++) {
            tb_idea idea=new tb_idea();
            
            idea.setIdeaBudget(list.get(i).getIdeaBudget());
            idea.setIdeaDescrption(list.get(i).getIdeaDescrption());
            idea.setIdeaDomain(list.get(i).getIdeaDomain());
            idea.setIdeaDuration(list.get(i).getIdeaDuration());
            idea.setIdeaId(list.get(i).getIdeaId());
            idea.setIdeaOwner(list.get(i).getIdeaOwner());
            idea.setIdeaPostedOn(list.get(i).getIdeaPostedOn());
            idea.setIdeaTitle(list.get(i).getIdeaTitle());
            idea.setIdeaFlag(list.get(i).getIdeaFlag());
            idea.setIdeaDays(list.get(i).getIdeaDays());
        	objcoll.add(idea);
        }                         
        session.close();
        return objcoll;

    }
    
    public tb_idea anIdea(int id) {
        tb_idea idea=new tb_idea(); 
    	try {
    	Session session = HibernateUtil.getSessionFactory().openSession();
       idea=(tb_idea)session.get(tb_idea.class, id);
       session.close();
    	 } catch (Exception e) {      

    }
         return idea;

}
   
    public  void e2pMsg(Integer ideaId,String ideaCommit)
    {  	
  	Session session = HibernateUtil.getSessionFactory().openSession();
  Transaction tx = null;
 
  try{
     tx = session.beginTransaction();
     tb_idea idea = (tb_idea)session.get(tb_idea.class, ideaId); 
     idea.setIdeaCommit(ideaCommit);
  session.update(idea); 
  tx.commit();
}catch (HibernateException e) {
  if (tx!=null) tx.rollback();
  e.printStackTrace(); 
}finally {
  session.close(); 
}}
    
    @SuppressWarnings("unchecked")
    public int proOwnerId(int id) {
        tb_idea idea=new tb_idea(); 
        int ownerid=0;
        try {
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	idea=(tb_idea)session.get(tb_idea.class, id);

    	 ownerid=idea.getIdeaPM();
       session.close();
    	 } catch (Exception e) {      

    }
        return ownerid;


}
    
}
