package prolution.dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import prolution.entity.listallusers;
import prolution.entity.tb_idea;
import prolution.entity.tb_project;
import prolution.entity.tb_rate;
import prolution.entity.tb_tasks;
import prolution.entity.tb_users;
import prolution.entity.viewCndetail;
import prolution.entity.viewEndetail;
import prolution.entity.viewPmdetail;
import prolution.entity.checkrequests;
import prolution.entity.view_users;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class UserDao {
	
	@SuppressWarnings("unchecked")
    public boolean find(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " from tb_users u where u.username='" + username + "' and u.upassword='" + password + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        List<tb_users> list = query.list();
        if (list.size() > 0) {
            session.close();
            return true;
        }
        session.close();
        return false;
    }
	
    public String rtype(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " select u.usertype from tb_users u where u.username='" + name + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        String result=(String) query.uniqueResult();
        
        session.close();
        return result;
	}
	
    public String checkuserstatus(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " select u.userstatus from tb_users u where u.username='" + name + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        String result=(String) query.uniqueResult();
        
        session.close();
        return result;
	} 
    
    public String getFname(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " select u.firstname from tb_users u where u.username='" + name + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        String result=(String) query.uniqueResult();
        
        session.close();
        return result;
	}     
    
/*	@SuppressWarnings("unchecked")
	@Transactional
	public boolean regis(String username, String upassword, String firstname,String lastname,String email,String usertype,String country ) {
		Session session = null;
		Transaction txn = null;
		boolean var=false;
		try {
			@SuppressWarnings("deprecation")
			SessionFactory sessionFactory = 
			        new Configuration().configure().buildSessionFactory();
			    session = sessionFactory.openSession(); 
			    txn = session.beginTransaction();
        String SQL_QUERY = " from tb_users u where u.username='" + username + "'";
        Query query = session.createQuery(SQL_QUERY);
        List<tb_users> list = query.list();
        if (list.size() == 0){
        	
        tb_users user=new tb_users();
        user.setEmail(email);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setUpassword(upassword);
        user.setUsertype(usertype);
        user.setUsername(username);
        user.setCountry(country);
        user.setUserstatus("A");
        
        session.save(user);
        txn.commit();
        var=true;}
        

        } catch (Exception e) { 
            System.out.println(e.getMessage());
        } finally {
            if (!txn.wasCommitted()) {
                txn.rollback();
            }

            session.flush();  
            session.close(); 
        }
        
    	return var;
        }*/
    
    
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean regis(String username, String upassword, String firstname,String lastname,String email,String usertype,String country ) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String SQL_QUERY = " from tb_users u where u.username='" + username + "'";
        Query query = session.createQuery(SQL_QUERY);
        List<tb_users> list = query.list();
        if (list.size() == 0){
        	
        tb_users user=new tb_users();
        user.setEmail(email);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setUpassword(upassword);
        user.setUsertype(usertype);
        user.setUsername(username);
        user.setCountry(country);
        user.setUserstatus("A");
        
        session.save(user);
        session.getTransaction().commit();
        }
        session.close();
        return true;
        
        }
	
    public int getUserId(String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = " select u.userid from tb_users u where u.username='" + username + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        int result=(int) query.uniqueResult();
        
        session.close();
        return result;
	}
	
	
	
	 @SuppressWarnings("unchecked")
    public ArrayList<tb_users> searchedUsers( ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_users";

        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<tb_users> objcoll=new ArrayList<>();
        List<tb_users> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_users user=new tb_users();
        	
        	user.setCountry(list.get(i).getCountry());
        	user.setEmail(list.get(i).getEmail());
        	user.setFirstname(list.get(i).getFirstname());
        	user.setLastname(list.get(i).getLastname());
        	user.setUserid(list.get(i).getUserid());
        	user.setUsername(list.get(i).getUsername());
        	user.setUsertype(list.get(i).getUsertype());
                  
        	objcoll.add(user);
        }       
        session.close();
        return objcoll;
    } 
    
    
	 @SuppressWarnings("unchecked")
	    public double userRanking(int id) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        String SQL_QUERY = "select avg(r.rating) from tb_rate r WHERE r.rated='" + id + "'";

	        System.out.println(SQL_QUERY);
	        Query query = session.createQuery(SQL_QUERY);  	        
	        
	        Double result=(Double)query.uniqueResult();
	        if (result==null){
	        	result=(double) 0;	        	
	        }
	       // result=(int)query.uniqueResult();	          	           
	        session.close();
	        return result;
	    } 
	 
		@SuppressWarnings("unchecked")
		public void rateUser(
				  int rating,
				  int rateIdea,
				  int rateTask,
				  int rated,
				  int rater ) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        tb_rate rate=new tb_rate();
rate.setRated(rated);
rate.setRateIdea(rateIdea);
rate.setRater(rater);
rate.setRateTask(rateTask);
rate.setRating(rating);
	   
		 session.save(rate);
	        session.getTransaction().commit();
	        session.close();
		}
		
	    public tb_users taskOwner(int id) {
	    	tb_users user=new tb_users(); 
	    	try {
	    	Session session = HibernateUtil.getSessionFactory().openSession();
	    	user=(tb_users)session.get(tb_users.class, id);
	       session.close();
	    	 } catch (Exception e) {      

	    }
	         return user;

	}
	    
	    public tb_users projectOwner(int id) {
	    	tb_users user=new tb_users(); 
	    	try {
	    	Session session = HibernateUtil.getSessionFactory().openSession();
	    	user=(tb_users)session.get(tb_users.class, id);
	       session.close();
	    	 } catch (Exception e) {      

	    }
	         return user;

	}
	    
	    @SuppressWarnings("unchecked")
	    public ArrayList<view_users> viewAllUsers() {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        //String SQL_QUERY = "from view_users";
	        //System.out.println(SQL_QUERY);
	        //Query query = session.createQuery(SQL_QUERY);
	        ArrayList<view_users> objcoll=new ArrayList<>();
	        List<view_users> list = session.createCriteria(view_users.class).list();
	        //List<view_users> list = query.list();
	        for (int i =0; i<list.size(); i++) {
	        	view_users users=new view_users();
	        	
	        	
	        	users.setUserid(list.get(i).getUserid());
	        	users.setUsername(list.get(i).getUsername());
	        	users.setCountry(list.get(i).getCountry());
	        	
	        	users.setEmail(list.get(i).getEmail());
	        	users.setFullName(list.get(i).getFullName());
	        	
	        	users.setRating(list.get(i).getRating());
	        	users.setUsertype(list.get(i).getUsertype());
	        	users.setUserstatus(list.get(i).getUserstatus());
	       
	        	objcoll.add(users);
	        }       
	        session.close();
	        return objcoll;

	    }
	    
	    @SuppressWarnings("unchecked")
	    public ArrayList<view_users> viewAllSearchingUsers(String str) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        String SQL_QUERY = "FROM view_users v WHERE v.fullName LIKE '%"+str+"%'";
	        //System.out.println(SQL_QUERY);
	        Query query = session.createQuery(SQL_QUERY);
	        ArrayList<view_users> objcoll=new ArrayList<>();
	        List<view_users> list = query.list();
	        for (int i =0; i<list.size(); i++) {
	        	view_users users=new view_users();
	        	
	        	
	        	users.setUserid(list.get(i).getUserid());
	        	users.setUsername(list.get(i).getUsername());
	        	users.setCountry(list.get(i).getCountry());
	        	
	        	users.setEmail(list.get(i).getEmail());
	        	users.setFullName(list.get(i).getFullName());
	        	
	        	users.setRating(list.get(i).getRating());
	        	users.setUsertype(list.get(i).getUsertype());
	        	users.setUserstatus(list.get(i).getUserstatus());
	       
	        	objcoll.add(users);
	        }       
	        session.close();
	        return objcoll;

	    }
		
	    @SuppressWarnings("unchecked")
	    public ArrayList<viewEndetail> viewenprofile(int id) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        String SQL_QUERY = "from viewEndetail p WHERE (p.userid='" +id+ "')";
	        System.out.println(SQL_QUERY);
	        Query query = session.createQuery(SQL_QUERY);
	        ArrayList<viewEndetail> objcoll=new ArrayList<>();
	        List<viewEndetail> list = query.list();
	        for (int i =0; i<list.size(); i++) {
	        	viewEndetail viewEndetail=new viewEndetail();
	        	
	        	
	        	viewEndetail.setUserid(list.get(i).getUserid());
	        	viewEndetail.setUsername(list.get(i).getUsername());
	        	viewEndetail.setCountry(list.get(i).getCountry());
	        	
	        	viewEndetail.setEmail(list.get(i).getEmail());
	        	viewEndetail.setFullName(list.get(i).getFullName());
	        	/*user.setFirstname(list.get(i).getFirstname());
	        	user.setLastname(list.get(i).getLastname());*/
	        	
	        	
	        	viewEndetail.setUsertype(list.get(i).getUsertype());
	        	viewEndetail.setIdeacount(list.get(i).getIdeacount());        	        	
	        	
	       
	        	objcoll.add(viewEndetail);
	        }       
	        session.close();
	        return objcoll;

	    }
	    
	    @SuppressWarnings("unchecked")
	    public ArrayList<viewPmdetail> viewprofile(int id) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        String SQL_QUERY = "from viewPmdetail p WHERE (p.userid='" +id+ "')";
	        System.out.println(SQL_QUERY);
	        Query query = session.createQuery(SQL_QUERY);
	        ArrayList<viewPmdetail> objcoll=new ArrayList<>();
	        List<viewPmdetail> list = query.list();
	        for (int i =0; i<list.size(); i++) {
	        	viewPmdetail viewPmdetail=new viewPmdetail();
	        	
	        	
	        	viewPmdetail.setUserid(list.get(i).getUserid());
	        	viewPmdetail.setUsername(list.get(i).getUsername());
	        	viewPmdetail.setCountry(list.get(i).getCountry());
	        	
	        	viewPmdetail.setEmail(list.get(i).getEmail());
	        	viewPmdetail.setFullName(list.get(i).getFullName());
	        	/*user.setFirstname(list.get(i).getFirstname());
	        	user.setLastname(list.get(i).getLastname());*/
	        	
	        	
	        	viewPmdetail.setUsertype(list.get(i).getUsertype());
	        	        	
	        	viewPmdetail.setProjectcount(list.get(i).getProjectcount());
	       
	        	objcoll.add(viewPmdetail);
	        }       
	        session.close();
	        return objcoll;

	    }
	    
	    @SuppressWarnings("unchecked")
	    public ArrayList<viewCndetail> viewcnprofile(int id) {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        //session.beginTransaction();
	        String SQL_QUERY = "from viewCndetail p WHERE (p.userid='" +id+ "')";
	        System.out.println(SQL_QUERY);
	        Query query = session.createQuery(SQL_QUERY);
	        ArrayList<viewCndetail> objcoll=new ArrayList<>();
	        List<viewCndetail> list = query.list();
	        for (int i =0; i<list.size(); i++) {
	        	viewCndetail viewCndetail=new viewCndetail();
	        	
	        	
	        	viewCndetail.setUserid(list.get(i).getUserid());
	        	viewCndetail.setUsername(list.get(i).getUsername());
	        	viewCndetail.setCountry(list.get(i).getCountry());
	        	
	        	viewCndetail.setEmail(list.get(i).getEmail());
	        	viewCndetail.setFullName(list.get(i).getFullName());
	        	/*user.setFirstname(list.get(i).getFirstname());
	        	user.setLastname(list.get(i).getLastname());*/
	        	
	        	
	        	viewCndetail.setUsertype(list.get(i).getUsertype());
	        	viewCndetail.setTaskcount(list.get(i).getTaskcount());        	        	
	        	
	       
	        	objcoll.add(viewCndetail);
	        }       
	        session.close();
	        return objcoll;

	    }
	    
   	 @SuppressWarnings("unchecked")
   public ArrayList<listallusers> enSearch() {
       Session session = HibernateUtil.getSessionFactory().openSession();
       //session.beginTransaction();
       /*String SQL_QUERY = "from tb_users";*/
       String SQL_QUERY = "from listallusers l WHERE l.usertype='en') ";

       System.out.println(SQL_QUERY);
       Query query = session.createQuery(SQL_QUERY);
       ArrayList<listallusers> objcoll=new ArrayList<>();
       List<listallusers> list = query.list();
       for (int i =0; i<list.size(); i++) {
       	listallusers user=new listallusers();
       	
       	user.setUserid(list.get(i).getUserid());
       	
       	user.setCountry(list.get(i).getCountry());
       	user.setEmail(list.get(i).getEmail());
       	user.setFullName(list.get(i).getFullName());
       	/*user.setFirstname(list.get(i).getFirstname());
       	user.setLastname(list.get(i).getLastname());*/
       	
       	user.setUsername(list.get(i).getUsername());
       	user.setUsertype(list.get(i).getUsertype());
       
         
       	objcoll.add(user);
       }       
       session.close();
       return objcoll;
   } 
   
   
	 @SuppressWarnings("unchecked")
   public ArrayList<listallusers> PmSearch() {
       Session session = HibernateUtil.getSessionFactory().openSession();
       //session.beginTransaction();
       /*String SQL_QUERY = "from tb_users";*/
       String SQL_QUERY = "from listallusers l WHERE l.usertype='pm') ";

       System.out.println(SQL_QUERY);
       Query query = session.createQuery(SQL_QUERY);
       ArrayList<listallusers> objcoll=new ArrayList<>();
       List<listallusers> list = query.list();
       for (int i =0; i<list.size(); i++) {
       	listallusers user=new listallusers();
       	
       	user.setUserid(list.get(i).getUserid());
       	
       	user.setCountry(list.get(i).getCountry());
       	user.setEmail(list.get(i).getEmail());
       	user.setFullName(list.get(i).getFullName());
       	/*user.setFirstname(list.get(i).getFirstname());
       	user.setLastname(list.get(i).getLastname());*/
       	
       	user.setUsername(list.get(i).getUsername());
       	user.setUsertype(list.get(i).getUsertype());
       
         
       	objcoll.add(user);
       }       
       session.close();
       return objcoll;
   }
   
   @SuppressWarnings("unchecked")
   public ArrayList<listallusers> CnSearch() {
       Session session = HibernateUtil.getSessionFactory().openSession();
       //session.beginTransaction();
       /*String SQL_QUERY = "from tb_users";*/
       String SQL_QUERY = "from listallusers l WHERE l.usertype='cn') ";

       System.out.println(SQL_QUERY);
       Query query = session.createQuery(SQL_QUERY);
       ArrayList<listallusers> objcoll=new ArrayList<>();
       List<listallusers> list = query.list();
       for (int i =0; i<list.size(); i++) {
       	listallusers user=new listallusers();
       	
       	user.setUserid(list.get(i).getUserid());
       	
       	user.setCountry(list.get(i).getCountry());
       	user.setEmail(list.get(i).getEmail());
       	user.setFullName(list.get(i).getFullName());
       	/*user.setFirstname(list.get(i).getFirstname());
       	user.setLastname(list.get(i).getLastname());*/
       	
       	user.setUsername(list.get(i).getUsername());
       	user.setUsertype(list.get(i).getUsertype());
       
         
       	objcoll.add(user);
       }       
       session.close();
       return objcoll;
   }

   
   public  void blockuser(Integer userid,String userstatus)
   {  	
	   	Session session = HibernateUtil.getSessionFactory().openSession();
	   	Transaction tx = null;

	   	try{
	   			tx = session.beginTransaction();
	   			tb_users user = (tb_users)session.get(tb_users.class, userid); 
	   			user.setUserstatus("I");
	   			/*idea.setIdeaPM(ideaPM);
	   			idea.setIdeaFlag(ideaFlag);*/ 
	   			session.update(user); 
	   			tx.commit();
	   		}
	   		catch (HibernateException e) {
	   			if (tx!=null) tx.rollback();
	   			e.printStackTrace(); 
	   		}finally {
	   			session.close(); 
	   		}
   }
   
   public ArrayList<checkrequests> checkrequests(int userid) {
       Session session = HibernateUtil.getSessionFactory().openSession();
       //session.beginTransaction();
       /*String SQL_QUERY = "from tb_users";*/
       String SQL_QUERY = " from checkrequests u where u.userid='" + userid + "'";

       System.out.println(SQL_QUERY);
       Query query = session.createQuery(SQL_QUERY);
       ArrayList<checkrequests> objcoll=new ArrayList<>();
       @SuppressWarnings("unchecked")
	List<checkrequests> list = query.list();
       for (int i =0; i<list.size(); i++) {
    	   checkrequests user=new checkrequests();
       	
       	user.setUserid(list.get(i).getUserid());
       	user.setUsername(list.get(i).getUsername());
       	user.setBids(list.get(i).getBids());
       	
         
       	objcoll.add(user);
       }       
       session.close();
       return objcoll;
   }
	
}


