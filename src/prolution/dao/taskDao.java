package prolution.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import prolution.entity.searchedideas;
import prolution.entity.searchedtasks;
import prolution.entity.TaskAllBids;
import prolution.entity.availabletasks;
import prolution.entity.tb_bids;
import prolution.entity.tb_tasks;

public class taskDao {
	
	public void post(int taskProId,int taskOwner,String taskTitle, String taskDescrption, String taskDomain, String taskPostedOn,
			String taskBudget,String taskDuration,int taskFlag,int precedor,int taskDays ) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
       
        tb_tasks tsk=new tb_tasks();
        tsk.setTaskFlag(taskFlag);
        tsk.setTaskBudget(taskBudget);
        
        tsk.setTaskDescrption(taskDescrption);
        tsk.setTaskDomain(taskDomain);
        tsk.setTaskDuration(taskDuration);
        tsk.setTaskOwner(taskOwner);
        tsk.setTaskPostedOn(taskPostedOn);
        tsk.setTaskProId(taskProId);
        tsk.setPrecedor(precedor);
        tsk.setTaskTitle(taskTitle);
        tsk.setTaskDays(taskDays);
        session.save(tsk);
        session.getTransaction().commit();
        session.close();
    }
	
    @SuppressWarnings("unchecked")
    public ArrayList<availabletasks> allTasks(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from availabletasks t WHERE t.taskID NOT IN (SELECT b.bidsTask from tb_bids b where b.bidsUser='" +id+"')";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<availabletasks> objcoll=new ArrayList<>();
        List<availabletasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	availabletasks task=new availabletasks();
        	
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;

    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<availabletasks> cnBids(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from availabletasks t WHERE t.taskID IN (SELECT b.bidsTask from tb_bids b where b.bidsUser='" +id+"')";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<availabletasks> objcoll=new ArrayList<>();
        List<availabletasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	availabletasks task=new availabletasks();
        	
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;

    }
    
    public  boolean bidsTask(int  bidsUser,int bidsTask,int  bidsFlag,int bidsAmount)
    {
    	 Session session = HibernateUtil.getSessionFactory().openSession();
         session.beginTransaction();
          
         tb_bids bid=new tb_bids();
         bid.setBidsTask(bidsTask);
         bid.setBidsFlag(bidsFlag);
         bid.setBidsUser(bidsUser);
         bid.setBidsAmount(bidsAmount);
         
         session.save(bid);
         session.getTransaction().commit();
         session.close();
         return true;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_tasks> myTasks(int taskOwner) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskOwner='" + taskOwner + "'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_tasks> objcoll=new ArrayList<tb_tasks>();
        List<tb_tasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_tasks task=new tb_tasks();
            
            task.setTaskBudget(list.get(i).getTaskBudget());
            task.setTaskDescrption(list.get(i).getTaskDescrption());
            task.setTaskDomain(list.get(i).getTaskDomain());
            task.setTaskDuration(list.get(i).getTaskDuration());
            task.setTaskID(list.get(i).getTaskID());
            task.setTaskProId(list.get(i).getTaskProId());
            task.setTaskOwner(list.get(i).getTaskOwner());
            task.setTaskPostedOn(list.get(i).getTaskPostedOn());
            task.setTaskTitle(list.get(i).getTaskTitle());
            
        	objcoll.add(task);
        }                         
        session.close();
        return objcoll;

    }
    

    @Transactional
    public  void commitTask(int  taskId,String taskCommit)
    {
    	 
    	tb_tasks task=new tb_tasks();
    	
    	Session session = HibernateUtil.getSessionFactory().openSession();
    		Transaction tx = null;
    		try {
    		tx = session.beginTransaction();
    		session.beginTransaction();
    		task=(tb_tasks)session.get(tb_tasks.class, taskId);
    	task.setTaskCommit(taskCommit);
    	
    	   session.update(task);
    	   
    	   tx.commit();
    		 } catch (HibernateException e) {
    	    if (tx!=null) tx.rollback();
    	    e.printStackTrace(); 
    	 }finally {
    	    session.close(); 
    	 }

    	}
    
    public tb_tasks projectTask(int id) {
    	tb_tasks task=new tb_tasks(); 
    	try {
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	task=(tb_tasks)session.get(tb_tasks.class, id);
       session.close();
    	 } catch (Exception e) {      

    }
         return task;

}
    
    public tb_tasks preTask(int id) {
    	tb_tasks task=new tb_tasks(); 
   
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	task=(tb_tasks)session.get(tb_tasks.class, id);
    	session.close();
    
         return task;
}
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_tasks> ideaTasks(int ideaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskIdeaId='" + ideaId + "' ORDER BY t.precedor ASC";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_tasks> objcoll=new ArrayList<tb_tasks>();
        List<tb_tasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_tasks task=new tb_tasks();
            
            task.setTaskBudget(list.get(i).getTaskBudget());
            task.setTaskDescrption(list.get(i).getTaskDescrption());
            task.setTaskDomain(list.get(i).getTaskDomain());
            task.setTaskDuration(list.get(i).getTaskDuration());
            task.setTaskID(list.get(i).getTaskID());
            task.setTaskOwner(list.get(i).getTaskOwner());
            task.setTaskPostedOn(list.get(i).getTaskPostedOn());
            task.setTaskTitle(list.get(i).getTaskTitle());
            task.setTaskIdeaId(list.get(i).getTaskIdeaId());
            
        	objcoll.add(task);
        }                         
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<tb_tasks> projectTasks(int proId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskProId='" + proId + "' ORDER BY t.precedor ASC";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        ArrayList<tb_tasks> objcoll=new ArrayList<tb_tasks>();
        List<tb_tasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	tb_tasks task=new tb_tasks();
            
            task.setTaskBudget(list.get(i).getTaskBudget());
            task.setTaskDescrption(list.get(i).getTaskDescrption());
            task.setTaskDomain(list.get(i).getTaskDomain());
            task.setTaskDuration(list.get(i).getTaskDuration());
            task.setTaskID(list.get(i).getTaskID());
            task.setTaskProId(list.get(i).getTaskProId());
            task.setTaskOwner(list.get(i).getTaskOwner());
            task.setTaskPostedOn(list.get(i).getTaskPostedOn());
            task.setTaskTitle(list.get(i).getTaskTitle());
            
        	objcoll.add(task);
        }                         
        session.close();
        return objcoll;

    }
    
    @SuppressWarnings("unchecked")
    public ArrayList ideaTaskCriticalPath(int ideaId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskIdeaId='" + ideaId + "' ORDER BY t.precedor ASC";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        //session.get(getClass(), arg1)
        //Iterator<project> it = query.iterate();      
        List<tb_tasks> list = query.list();
        HashSet<Task> critTasks = new HashSet<Task>();
        for (int i =0; i<list.size(); i++) {
    /*    	int pehla=list.get(i).getPrecedor();
        	Task pretsk=new Task(preTask(pehla).getTaskTitle(), preTask(pehla).getTaskDays());
    	    critTasks.add(new Task(list.get(i).getTaskTitle(), list.get(i).getTaskDays(),pretsk));                 
        }                         
        session.close();
        return Arrays.toString(criticalPath(critTasks));
    }*/
    
 	critTasks.add(new Task(list.get(i).getTaskTitle(), list.get(i).getTaskDays()));

        }                         
        session.close();

        ArrayList alist=new ArrayList<>();
        Iterator<Task> it= Arrays.asList(criticalPath(critTasks)).iterator();
  while(it.hasNext()){
      Task obj=new Task();
	  obj.name= it.next().name;
//	  obj.cost=it.next().cost;
	//  obj.criticalCost=it.next().criticalCost;
	  alist.add(obj);
  }
  return alist;
  
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList taskCriticalPath(int proId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from tb_tasks t where t.taskProId='" + proId + "' ORDER BY t.precedor ASC";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
     
        List<tb_tasks> list = query.list();
	  
        HashSet<Task> critTasks = new HashSet<Task>();
        for (int i =0; i<list.size(); i++) {
   //     	int pehla=list.get(i).getPrecedor();
 //if(pehla !=0){
     //   	Task pretsk=new Task(preTask(pehla).getTaskTitle(), preTask(pehla).getTaskDays());
   //     	critTasks.add(new Task(list.get(i).getTaskTitle(), list.get(i).getTaskDays(),pretsk));
 //}
// else{
 	critTasks.add(new Task(list.get(i).getTaskTitle(), list.get(i).getTaskDays()));

 //}
    	    //critTasks.add(new Task(String.valueOf(list.get(i).getTaskID()), list.get(i).getTaskDays()));                 
        }                         
        session.close();
       // return Arrays.toString(criticalPath(critTasks));

        ArrayList alist=new ArrayList<>();
        Iterator<Task> it= Arrays.asList(criticalPath(critTasks)).iterator();
  while(it.hasNext()){
      Task obj=new Task();
	  // obj.criticalCost= it.next().criticalCost;
 //  obj.cost= it.next().cost;
	  obj.name= it.next().name;
	  //obj.cost=it.next().cost;
	  //obj.criticalCost=it.next().criticalCost;
	  alist.add(obj);
  }
  return alist;
  
    }
    public static Task[] criticalPath(Set<Task> tasks){
	    //tasks whose critical cost has been calculated
	    HashSet<Task> completed = new HashSet<Task>();
	    //tasks whose ciritcal cost needs to be calculated
	    HashSet<Task> remaining = new HashSet<Task>(tasks);

	    //Backflow algorithm
	    //while there are tasks whose critical cost isn't calculated.
	    while(!remaining.isEmpty()){
	      boolean progress = false;

	      //find a new task to calculate
	      for(Iterator<Task> it = remaining.iterator();it.hasNext();){
	        Task task = it.next();
	        if(completed.containsAll(task.dependencies)){
	          //all dependencies calculated, critical cost is max dependency
	          //critical cost, plus our cost
	          int critical = 0;
	          for(Task t : task.dependencies){
	            if(t.criticalCost > critical){
	              critical = t.criticalCost;
	            }
	          }
	          task.criticalCost = critical+task.cost;
	          //set task as calculated an remove
	          completed.add(task);
	          it.remove();
	          //note we are making progress
	          progress = true;
	        }
	      }
	      //If we haven't made any progress then a cycle must exist in
	      //the graph and we wont be able to calculate the critical path
	      if(!progress) throw new RuntimeException("Cyclic dependency, algorithm stopped!");
	    }

	    //get the tasks
	    Task[] ret = completed.toArray(new Task[0]);
	    //create a priority list
	    Arrays.sort(ret, new Comparator<Task>() {

	      public int compare(Task o1, Task o2) {
	        //sort by cost
	        int i= o2.criticalCost-o1.criticalCost;
	        if(i != 0)return i;

	        //using dependency as a tie breaker
	        //note if a is dependent on b then
	        //critical cost a must be >= critical cost of b
	        if(o1.isDependent(o2))return -1;
	        if(o2.isDependent(o1))return 1;
	        return 0;
	      }
	    });

	    return ret;
	  }
    
 
    
    
    

    
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedtasks> searchedTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        //String SQL_QUERY = "from searchedtasks";
       // System.out.println(SQL_QUERY);
        //Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedtasks> objcoll=new ArrayList<>();
        //List<searchedtasks> list = query.list();
        List<searchedtasks> list = session.createCriteria(searchedtasks.class).list();

        for (int i =0; i<list.size(); i++) {
        	searchedtasks task=new searchedtasks();
        	
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());////check the commented values
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
//        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedtasks> searchedWebTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedtasks f WHERE f.taskDomain='web'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedtasks> objcoll=new ArrayList<>();
        List<searchedtasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	searchedtasks task=new searchedtasks();
        	//task.setTaskFlag(list.get(i).getTaskFlag());
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());  ///check thecommented values
        	task.setTaskCommit(list.get(i).getTaskCommit());
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedtasks> searchedIosTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedtasks f WHERE f.taskDomain='IOS'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedtasks> objcoll=new ArrayList<>();
        List<searchedtasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	searchedtasks task=new searchedtasks();
        	//task.setTaskFlag(list.get(i).getTaskFlag());
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());  ///check thecommented values
        	task.setTaskCommit(list.get(i).getTaskCommit());
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedtasks> searchedAndroidTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedtasks f WHERE f.taskDomain='Android'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedtasks> objcoll=new ArrayList<>();
        List<searchedtasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	searchedtasks task=new searchedtasks();
        	//task.setTaskFlag(list.get(i).getTaskFlag());
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());  ///check thecommented values
        	task.setTaskCommit(list.get(i).getTaskCommit());
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    
    @SuppressWarnings("unchecked")
    public ArrayList<searchedtasks> searchedDesktopTasks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        String SQL_QUERY = "from searchedtasks f WHERE f.taskDomain='Desktop'";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        ArrayList<searchedtasks> objcoll=new ArrayList<>();
        List<searchedtasks> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	searchedtasks task=new searchedtasks();
        	//task.setTaskFlag(list.get(i).getTaskFlag());
        	task.setNoOfBids(list.get(i).getNoOfBids());
        	//task.setTaskAproved(list.get(i).isTaskAproved());  ///check thecommented values
        	task.setTaskCommit(list.get(i).getTaskCommit());
        	//task.setTaskCompletion(list.get(i).isTaskCompletion());
        	task.setTaskOwner(list.get(i).getTaskOwner());
        	//task.setTaskSigned(list.get(i).isTaskSigned());
        	task.setTaskBudget(list.get(i).getTaskBudget());
        	task.setTaskDescrption(list.get(i).getTaskDescrption());
        	task.setTaskDomain(list.get(i).getTaskDomain());
        	task.setTaskDuration(list.get(i).getTaskDuration());
        	task.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	task.setTaskTitle(list.get(i).getTaskTitle());
        	task.setTaskID(list.get(i).getTaskID());
        
        	objcoll.add(task);
        }       
        session.close();
        return objcoll;
    }
    

    
    @SuppressWarnings({ "unchecked", "finally" })
    public ArrayList<TaskAllBids> taskAllBids(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        ArrayList<TaskAllBids> objcoll=new ArrayList<>();
        String SQL_QUERY = "from TaskAllBids f WHERE (f.taskID='" +id+ "')";
        System.out.println(SQL_QUERY);
        Query query = session.createQuery(SQL_QUERY);
        List<TaskAllBids> list = query.list();
        for (int i =0; i<list.size(); i++) {
        	TaskAllBids taskAllBids=new TaskAllBids();
        	
        	taskAllBids.setCountry(list.get(i).getCountry());
        	taskAllBids.setFirstname(list.get(i).getFirstname());
        	taskAllBids.setLastname(list.get(i).getLastname());
        	taskAllBids.setUsername(list.get(i).getUsername());
        	taskAllBids.setTaskBudget(list.get(i).getTaskBudget());
        	taskAllBids.setTaskDescrption(list.get(i).getTaskDescrption());
        	taskAllBids.setTaskDomain(list.get(i).getTaskDomain());
        	taskAllBids.setTaskDuration(list.get(i).getTaskDuration());
        	taskAllBids.setTaskID(list.get(i).getTaskID());
        	taskAllBids.setTaskPostedOn(list.get(i).getTaskPostedOn());
        	taskAllBids.setTaskTitle(list.get(i).getTaskTitle());
        	taskAllBids.setUserid(list.get(i).getUserid());
        	taskAllBids.setBidsAmount(list.get(i).getBidsAmount());
        	objcoll.add(taskAllBids);
        }
             

        	  session.close(); 
        	  return objcoll; 
        	}        
 
    
    
		  
			public void assignTask(Integer taskID,int taskFlag,int taskOwner) {
			  
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = null;
			try {
			tx = session.beginTransaction();
			tb_tasks task=(tb_tasks)session.get(tb_tasks.class, taskID);
			task.setTaskFlag(taskFlag);
			task.setTaskOwner(taskOwner);					
		   
		   session.update(task);
		   
		   tx.commit();
			 } catch (HibernateException e) {
		    if (tx!=null) tx.rollback();
		    e.printStackTrace(); 
		 }finally {
		    session.close(); 
		 }

		}
		  
			public void ideaTaskPost(int taskIdeaId,int taskOwner,String taskTitle, String taskDescrption, String taskDomain, String taskPostedOn,
					String taskBudget,String taskDuration,int taskFlag,int precedor,int taskDays  ) {
		        Session session = HibernateUtil.getSessionFactory().openSession();
		        session.beginTransaction();
		       
		        tb_tasks tsk=new tb_tasks();
		        tsk.setTaskFlag(taskFlag);
		        tsk.setTaskBudget(taskBudget);
		        
		        tsk.setTaskDescrption(taskDescrption);
		        tsk.setTaskDomain(taskDomain);
		        tsk.setTaskDuration(taskDuration);
		        tsk.setTaskOwner(taskOwner);
		        tsk.setTaskPostedOn(taskPostedOn);
		        tsk.setTaskIdeaId(taskIdeaId);
		        tsk.setPrecedor(precedor);
		        tsk.setTaskTitle(taskTitle);
		        tsk.setTaskDays(taskDays);		        
		        session.save(tsk);
		        session.getTransaction().commit();
		        session.close();
		    }

		    public  void p2cMsg(Integer taskID,String taskCommit)
		    {  	
		  	Session session = HibernateUtil.getSessionFactory().openSession();
		  Transaction tx = null;
		 
		  try{
		     tx = session.beginTransaction();
		     tb_tasks task = (tb_tasks)session.get(tb_tasks.class, taskID); 
		     task.setTaskCommit(taskCommit);
		  session.update(task); 
		  tx.commit();
		}catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
		  e.printStackTrace(); 
		}finally {
		  session.close(); 
		}}
		    
		    public  void taskCompleted(Integer taskID,int taskFlag)
		    {  	
		  	Session session = HibernateUtil.getSessionFactory().openSession();
		  Transaction tx = null;
		 
		  try{
		     tx = session.beginTransaction();
		     tb_tasks task = (tb_tasks)session.get(tb_tasks.class, taskID); 
		     task.setTaskFlag(taskFlag); 
		  session.update(task); 
		  tx.commit();
		}catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
		  e.printStackTrace(); 
		}finally {
		  session.close(); 
		}}
		    
		    public  void sendPrecedor(Integer taskID,int precedor) throws ParseException
		    {  	
		  	Session session = HibernateUtil.getSessionFactory().openSession();
		  Transaction tx = null;
		 
		  try{
		     tx = session.beginTransaction();
		     tb_tasks ptask = (tb_tasks)session.get(tb_tasks.class, precedor); 

		     tb_tasks task = (tb_tasks)session.get(tb_tasks.class, taskID); 
		     task.setPrecedor(precedor);
			  
		     SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		     SimpleDateFormat ftt = new SimpleDateFormat ("yyyy.MM.dd");
	     
		     Calendar c = Calendar.getInstance(); 
		     Calendar d = Calendar.getInstance(); 
		     
		     c.setTime(ft.parse(ptask.getTaskDuration())); 
		     d.setTime(ft.parse(ptask.getTaskDuration())); 
		     
				c.add(Calendar.DATE,task.getTaskDays());
				d.add(Calendar.DATE,1); 

				String startDate=ftt.format(d.getTime());	
				task.setTaskPostedOn(startDate);
				
				String convertedDate=ft.format(c.getTime());	
				task.setTaskDuration(convertedDate);
		  session.update(task); 
		  tx.commit();
		}catch (HibernateException e) {
		  if (tx!=null) tx.rollback();
		  e.printStackTrace(); 
		}finally {
		  session.close(); 
		}}
		    
		    
}