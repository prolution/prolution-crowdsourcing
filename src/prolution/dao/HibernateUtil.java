package prolution.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static final SessionFactory sessionFactory=buildSessionFactory();
	 
	   public static SessionFactory buildSessionFactory(){
	        try {
	            
	        	Configuration configuration = new Configuration();
	            configuration.configure();
	            ServiceRegistry  serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
	                    configuration.getProperties()).build();
	        	return configuration.buildSessionFactory(serviceRegistry);
	        } catch (Throwable ex) {
	            System.err.println("Initial SessionFactory creation failed." + ex);
	            throw new ExceptionInInitializerError(ex);
	        }
	    }
	 
	    public static SessionFactory getSessionFactory() {
	        return sessionFactory;
	    }

}
